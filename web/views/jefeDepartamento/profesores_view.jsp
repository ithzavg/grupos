<%-- 
    Document   : profesores
    Created on : 15/02/2015, 09:59:19 PM
    Author     : Edgar
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">        
        <link href="css/lib/bootstrap.min.css?${current}" rel="stylesheet">        
        <link href="css/lib/dataTables.bootstrap.css?${current}" rel="stylesheet">
        <link href="css/lib/font-awesome.min.css?${current}" rel="stylesheet">        
        <link href="css/grupos.css?${current}" rel="stylesheet">
        <link href="css/lib/jquery-ui.css?${current}" rel="stylesheet">
        <link rel="icon" href="img/logo-final.png">
        <title>Grupos</title>
    </head>
    <body>
        <%@include file="../modal/reportes_mapeogrupos_modal.jsp"%>
        <%@include file="../modal/profesores_catalogoprofesores_modal.jsp" %>             
        <%@include file="../modal/profesores_horarioprofesor_modal.jsp" %>        
        <%@include file="../general/header.jsp" %> 

        <div class="container">
            <%@include file="../general/navbarGrupos.jsp" %>
            <%@include file="../general/panelGeneral.jsp" %>


            <div class="panel panel-default">
                <div class="panel-body">
                    <div role="tabpanel">

                        <!-- Nav tabs -->
                        <ul id="ulTabJefeDepto" class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active"><a href="#divTabBienvenido" aria-controls="divTabBienvenido" role="tab" data-toggle="tab">Bienvenido</a></li>
                            <li role="presentation"><a href="#divTabAsigProf" aria-controls="divTabAsigProf" role="tab" >Asignar Profesor</a></li>
                            <li role="presentation"><a href="#divTabAsigHorario" aria-controls="divTabAsigHorario" role="tab" >Asignar Horario</a></li>
                            <li role="presentation"><a href="#divTabReportes" aria-controls="divTabReportes" role="tab">Reportes</a></li>                            
                        </ul>

                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="divTabBienvenido">
                                <br/>
                                <div class="jumbotron" style='text-align: justify;'>
                                    <h1>Bienvenido</h1>
                                    <p><strong>Asignar Profesor: </strong></p>
                                    <p>En este apartado se deben asignar profesores a los grupos creados por el coordinador</p>
                                    <p><strong>Asignar Horario: </strong></p>
                                    <p>En este apartado se tiene que asignar los horarios a los profesores que fueron proporcionados por Recursos Humanos</p>
                                    <br/>
                                    <p><center><a class="btn btn-primary btn-lg" href="#" role="button">Ayuda</a></center></p>
                                </div>
                            </div>                            
                            <div role="tabpanel" class="tab-pane" id="divTabAsigProf">

                                <br/>
                                <table id="tblGrpJefDpto" class="table table-striped table-bordered">
                                    <thead>
                                        <tr>                                            
                                            <th>Cve</th>
                                            <th>Materia</th>                                                                                        
                                            <th>Lun</th>
                                            <th>Mar</th>
                                            <th>Mie</th>
                                            <th>Jue</th>
                                            <th>Vie</th>
                                            <th>Sab</th>
                                            <th>Acad</th>
                                            <th>Profesor</th>                                            
                                            <th></th>                                            
                                        </tr>
                                    </thead>
                                    <tbody id="tbodyGruposPJ">

                                    </tbody>
                                </table>                                                 
                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="divTabAsigHorario">
                                <br/>                                
                                <table id="tblHorarioProf" class="table table-striped table-bordered table-responsive">
                                    <thead>
                                        <tr>
                                            <th>Prof</th>
                                            <th>Nombre</th>                                            
                                            <th>H_Asig</th>
                                            <th>H_Ocup</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody id="tbodyProfesoresA">                                        
                                    </tbody>
                                </table>
                            </div>   
                            <div role="tabpanel" class="tab-pane fade" id="divTabReportes">
                                <br/>                                

                                <div class="col-md-6">
                                    <ul>
                                        <li>
                                            <p>
                                                <a data-toggle="collapse" href="#collapseHorarios" aria-expanded="false" aria-controls="collapseHorarios">
                                                    Generación de Mapeo de Profesores
                                                </a>
                                                <button id="btnRepHor" class=" col-md-offset-5 btn btn-warning btn-sm"><i class="fa fa-file-pdf-o"></i></button>
                                            </p>    
                                            <div class="collapse" id="collapseHorarios">
                                                <div class="well">
                                                    Generacion de reporte donde se ven reflejados los grupos que tiene asignado cada profesor
                                                    a lo largo de la semana
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <p>
                                                <a data-toggle="collapse" href="#collapseGrupos" aria-expanded="false" aria-controls="collapseHorarios">
                                                    Generación de Grupos por Carrera &nbsp;&nbsp; 
                                                </a>
                                                <button id="btnRepCarr" class=" col-md-offset-5 btn btn-warning btn-sm"><i class="fa fa-file-pdf-o"></i></button>
                                            </p>    
                                            <div class="collapse" id="collapseGrupos">
                                                <div class="well">
                                                    Generación de reporte donde el Jefe de Departamento puede ver los grupos de su carrera
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <p>
                                                <a data-toggle="collapse" href="#collapseAtender" aria-expanded="false" aria-controls="collapseHorarios">
                                                    Generación de Grupos por Atender &nbsp;&nbsp; 
                                                </a>
                                                <button id="btnRepJef" class=" col-md-offset-5 btn btn-warning btn-sm"><i class="fa fa-file-pdf-o"></i></button>
                                            </p>    
                                            <div class="collapse" id="collapseAtender">
                                                <div class="well">
                                                    Generación de reporte donde el Jefe de Departamento puede ver los grupos por atender
                                                </div>
                                            </div>
                                        </li>
                                        
                                    </ul>
                                    
                                </div>    

                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

        <%@include file="../general/footer.jsp" %>
        <script src="js/lib/jquery-1.11.1.min.js?${current}"></script>  
        <script src="js/lib/jquery-ui.js?${current}"></script> 
        <script src="js/lib/bootstrap.min.js?${current}"></script>        
        <script src="js/lib/jquery.dataTables.js?${current}"></script>               
        <script src="js/lib/dataTables.bootstrap.js?${current}"></script>      
        <script src="js/lib/bootbox.min.js?${current}"></script>   
        <script src="js/profesores_handler.js?${current}"></script>   
        <script src="js/gruposGeneral_handler.js?${current}"></script> 
        <!--<script src="js/panelGeneral.js"></script>-->               

    </body>
</html>
