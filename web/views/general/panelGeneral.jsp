<%-- 
    Document   : panelGeneral
    Created on : 15/02/2015, 09:24:08 PM
    Author     : Edgar
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!--EMPIEZA PANEL GENERAL-->
<div class="panel panel-primary">
    <div class="panel-heading">
        <div class="form-inline">
            <h3 class="panel-title">                
                <c:choose>

                    <c:when test="${usuario == 1 && vista == 0}">
                        Academia: ${objUsuario.getNomAcademia()}
                    </c:when>
                    <c:otherwise>

                        Academia: ${objUsuario.getNomAcademia()}
                        &nbsp;&nbsp;&nbsp;<a href="#" id="aRefresh"
                                             data-toggle="tooltip" data-placement="top" 
                                             title="Ver en tiempo real cuantos profesores han sido asignados"><i class="fa fa-refresh" id="iRefresh"></i></a>

                    </c:otherwise>    
                </c:choose>                
            </h3>
        </div>                    
    </div>
    <div class="panel-body">
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="form-group">
                        <label>PLANES</label>                                                                                    
                        <div class="input-group">
                            <div class="input-group-addon"><i class="fa fa-graduation-cap"></i></div>                            
                                <c:choose>
                                    <c:when test="${usuario == 1 && vista == 1}">                                    
                                    <select class="form-control input-sm" id="selCarreras" style="font-family: 'FontAwesome',Helvetica Neue,Helvetica,Arial">
                                        <option value='#'>No ha seleccionado periodo</option>                                
                                    </select>                                    
                                </c:when>
                                <c:otherwise>
                                    <input class="form-control input-sm" value="${objUsuario.getNombreCarrera()}" readonly="true">
                                </c:otherwise>
                            </c:choose>
                        </div>
                        <br/>
                        <div class="input-group">

                            <div class="input-group-addon"><i class="fa fa-key"></i></div>

                            <select  id="selPlanes" class="form-control input-sm" style="font-family: 'FontAwesome',Helvetica Neue,Helvetica,Arial">
                                <option value="#">
                                    Seleccione Plan
                                </option>
                                <c:if test="${usuario == 0 || (usuario ==1 && vista == 0 )}">
                                    <c:choose>
                                        <c:when test="${empty error}">
                                            <c:forEach items="${planes}" var="plan">
                                                <option value="${plan.getPlan_id()}">
                                                    ${plan.getClave()}
                                                </option>
                                            </c:forEach>                                
                                        </c:when>
                                        <c:otherwise>
                                            <option>${error}</option>
                                        </c:otherwise>
                                    </c:choose>
                                </c:if>
                            </select>                                    
                        </div>
                    </div>
                </div>
            </div>                        
        </div>
        <div class="col-md-6">

            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="form-group">
                        <label>ESPECIALIDAD</label>                                                                      
                        <h5>Clave&nbsp;&nbsp;&nbsp;&nbsp;Nombre</h5>
                        <div class="input-group">                                        
                            <div id="divEspecId" class="input-group-addon">#</div>                                        
                            <select  id="selEspec" class="form-control" style="font-family: 'FontAwesome',Helvetica Neue,Helvetica,Arial">                               
                                <option value="#">No se ha seleccionado plan</option>                               
                            </select>                                    
                        </div>                                    
                    </div>                                
                </div>
            </div>
        </div>
    </div>
</div>
<!--TERMINA PANEL GENERAL-->