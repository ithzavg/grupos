<%-- 
    Document   : checkout
    Created on : 8/11/2015, 09:36:50 AM
    Author     : Edgar
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="<c:url value="css/lib/bootstrap.min.css" />" rel="stylesheet">                                        
        <link href="css/grupos.css" rel="stylesheet">          
        <link rel="icon" href="img/logo-final.png">
        <title>Expiró Sesión</title>
    </head>    
    <body>
        <div class="container">
            <%@include file="header.jsp" %>
            <div role="tabpanel" class="tab-pane active" id="divTabBienvenido">
                <br/>
                <div class="panel panel-default">
                    <div class="panel-body">
                
                        <div class="jumbotron" style='text-align: justify;'>
                            <center>
                                <h3><strong>${causa}</strong></h3>                                
                                <p>Si persisten los problemas comuniquese con su administrador</p>                
                                <br/>
                                <p><a class="btn btn-primary btn-lg" href="/ITTLogin/" role="button">Iniciar Sesión</a></p>
                            </center>
                        </div>
                    </div>
                </div>

            </div>  
        </div>
        <%@include file="footer.jsp" %>

        <script src="js/lib/jquery-1.11.1.min.js"></script>          
        <script src="js/lib/bootstrap.min.js"></script>                                       
    </body>
</html>
