<%-- 
    Document   : navbarGrupos
    Created on : 15/02/2015, 09:23:54 PM
    Author     : Edgar
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!-- EMPIEZA NAVBAR-->
<nav class="navbar navbar-default" role="navigation">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Grupos                 
                <b id="bPeriodoNavbar"  data-catalogo="${opCat}" data-periodo="${paPeriodo}" data-año = "${paAño}" data-academia="${objUsuario.getAcademiaUs()}" data-modalidad="${objUsuario.getModalidad()}">                   
                    <%--<c:choose>
                        <c:when test="${opPeriodo==1}">
                            Actuales ${paPeriodoDes}
                        </c:when>
                        <c:when test="${opPeriodo==2}">
                            Preparación ${paPeriodoDes}
                        </c:when>
                        <c:when test="${opPeriodo==3}">
                            Verano ${paPeriodoDes}
                        </c:when>    
                    </c:choose>--%>
                    ${paPeriodoDes}
                </b>
            </a>
        </div>

        <div class="collapse navbar-collapse">            
            <ul class="nav navbar-nav navbar-right">                                            
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-cog"></i> Opciones<span class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
                        <!--                        <li><a href="#">Lista Asistencia</a></li>
                                                <li><a href="#">Lista Asistencia Comp</a></li>
                                                <li><a href="#">Lista Calif. Parciales</a></li>
                                                <li><a href="#">Lista Calif. Parciales Comp</a></li>
                                                <li><a href="#">Grupos Actuales</a></li>
                                                <li><a href="#">Grupos por profesor</a></li>
                                                <li><a href="#">Seguimiento Materias</a></li>                        -->
                        <c:if test="${usuario == 1 && vista==0}">
                            <li><a href="prestamoaula.do"><i class="fa fa-university"></i> Prestamo de aulas</a></li> 
                        </c:if>
                          
                            <li><a href="salirRegresa.do?opc=0"><i class="fa fa-sitemap"></i> Regresar</a></li> 
                            <li><a href="salirRegresa.do?opc=1"><i class="fa fa-sign-out"></i> Salir</a></li> 
                        </ul>
                    </li>
                <c:if test="${usuario == 1 && vista== 1 }">
                    <form class="navbar-form navbar-left" >
                        <select id="selPeriodo" class="form-control input-sm">                        
                            <option value="_">Sel.Periodo</option>
                            <option value="1">Actuales</option>
                            <option value="2">Preparacion</option>                        
                            <option value="3">Intersemestral</option>                        
                        </select>
                    </form>
                </c:if>
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>
<!--TERMINA NAVBAR-->
