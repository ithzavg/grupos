<%-- 
    Document   : menuPrestamoAulas
    Created on : 16/02/2016, 12:25:47 PM
    Author     : IthzaVG
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

    <div class="list-group">
        <a class="list-group-item" href="prestamoaula.do"><i class="fa fa-university fa-fw"></i>&nbsp; Prestar Aula</a>
        <a class="list-group-item" href="aulasprestadas.do"><i class="fa fa-users fa-fw"></i>&nbsp; Aulas en prestamo</a>
        <a class="list-group-item" href="otrasaulas.do"><i class="fa fa-archive fa-fw"></i>&nbsp; Aulas prestadas</a>
       
    </div>
