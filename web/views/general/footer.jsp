<%-- 
    Document   : footer
    Created on : 8/06/2015, 01:11:45 PM
    Author     : Edgar
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<div class="container">

    <footer class="login-footer">                
        <div >                    
            <br><br>
            <label >
                Instituto Tecnológico de Toluca | <a href="//www.ittoluca.edu.mx">www.ittoluca.edu.mx</a>
                <br>
                Instituto Tecnológico de Toluca - Algunos derechos reservados © 2013
                <br>
            </label>

            <center> <img   class="img-responsive " alt="Responsive image" src="img/footer.png" title="footer"></center>           
            <br>
            <label>
                Av. Tecnológico s/n. Fraccionamiento La Virgen
                <br>
                Metepec, Edo. De México, México C.P. 52149
                <br>
                Tel. (52) (722) 2 08 72 00
            </label>
        </div>
    </footer>

</div>