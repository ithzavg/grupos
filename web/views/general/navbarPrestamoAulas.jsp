<%-- 
    Document   : navbarPrestamoAulas
    Created on : 9/03/2016, 10:36:29 AM
    Author     : IthzaVG
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<nav class="navbar navbar-default">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" >
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Prestamo de aulas / Grupos 
                <b>
                   ${paPeriodoDes}
                </b>
            </a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">

                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"><i class="fa fa-cog"></i> Opciones<span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="modulogrupos.do?p=${objUsuario.getTipoPeriodoUs()}"><i class="fa fa-sitemap"></i> Regresar</a></li>
                        <li><a href="salirRegresa.do?opc=1"><i class="fa fa-sign-out"></i> Salir</a></li>                        
                    </ul>
                </li>
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>

