<%-- 
    Document   : profesores_catalogoprofesores_modal
    Created on : 15/02/2015, 10:47:00 PM
    Author     : Edgar
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<div class="modal fade " id="divModalCatProf" data-backdrop="static">
    <div class="modal-dialog modal-lg" id="dragModCatProfs">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Busqueda Profesores</h4>
            </div>
            <div class="modal-body ">          
                <br/>
                <b id="bGrupoId"></b>&nbsp;<b id="bNomGrupo"></b>
                <select id="selAcademia" class="form-control input-sm">                                      
                </select>
                <br/>   
                <table id="tblCatProfesores" class="table table-striped table-bordered table-responsive">
                    <thead>
                        <tr>                            
                            <th>Id</th>
                            <th>Nombre</th>
                            <th>Acd</th>                            
                            <th>H_Ocup</th>                            
                            <th>Mensaje</th>                            
                        </tr>
                    </thead>
                    <tbody id="tbodyProfesores">
                                      
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>                
                <button id="btnAceptarCatProfesores" type="button" class="btn btn-primary" data-dismiss="modal">Aceptar</button>                
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->