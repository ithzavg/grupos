<%-- 
    Document   : reportes_listainscritos_modal
    Created on : 12/04/2016, 09:30:43 PM
    Author     : Ed
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<div class="modal fade " id="modalGrupoEspecifico" data-backdrop="static">
    <div class="modal-dialog modal-sm" id="dragGrupoEspecifico">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Ingrese el n&uacute;mero de grupo</h4>
            </div>
            <div class="modal-body ">                                         
                <div class="input-group">
                    <span class="input-group-addon" id="basic-addon1"><i class="fa fa-keyboard-o" aria-hidden="true"></i></span>
                    <input id="inpIdGrupo" type="text" class="form-control" placeholder="0000000000" maxlength="10" aria-describedby="basic-addon1" required>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>                                                                                                      
                <button id="btnGenRepInscritos" type="button" class="btn btn-primary" >Generar Reporte</button>                                                                                                                      
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->