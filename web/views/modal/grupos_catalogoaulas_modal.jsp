<%-- 
    Document   : aulasGrupos
    Created on : 14/02/2015, 09:10:06 PM
    Author     : Edgar
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
 <!--MODAL INICIO-->
        <div class="modal fade" id="divModalCatAulas" data-backdrop="static">
            <div class="modal-dialog" id="dragModCatAulas">
                <div class="modal-content">
                    <div class="modal-header">
                        <!--<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>-->
                        <h4 class="modal-title">Seleccione Aula</h4>
                    </div>
                    <div class="modal-body ">
                                                
                        <table id="tblCatAulas" class="table table-bordered table-responsive">
                            <thead>
                                <tr>
                                    <th>Aula</th>
                                    <th>Edif</th>
                                    <th>Cap</th>
                                    <th>Observación </th>
                                </tr>
                            </thead>
                            <tbody id="tbodyAulas">                             
                            </tbody>                               
                        </table>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <button id="btnAceptarCatAulas" type="button" class="btn btn-primary">Aceptar</button>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->

        <!--MODAL FINAL-->
        