<%-- 
    Document   : reportes_mapeogrupos_modal
    Created on : 2/02/2016, 03:34:11 PM
    Author     : Edgar
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<div class="modal fade " id="divRepCatAulas" data-backdrop="static">
    <div class="modal-dialog" id="dragRepCatAulas">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">${objUsuario.getNomAcademia()}</h4>
            </div>
            <div class="modal-body ">                          
                
                <table id="tblRepCatAulas" class="table table-hover table-striped table-bordered table-responsive">
                    <thead>
                        <tr>                            
                            <th id="thID"></th>
                            <th id="thDes"></th>                            
                        </tr>
                    </thead>
                    <tbody id="tbodyRepAulas">                        
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>                                                                                                      
                <button id="btnTodosRep" type="button" class="btn btn-primary" >Todos</button>                                                                                                                      
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->