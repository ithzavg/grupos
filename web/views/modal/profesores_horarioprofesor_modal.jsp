<%-- 
    Document   : horariosProfesor
    Created on : 11/06/2015, 10:49:16 AM
    Author     : Edgar
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<div class="modal fade" id="divModalHorarioProf" data-backdrop="static">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">               
                <h4 class="modal-title">Horario Profesor <b id="bNomProf"> </b></h4>
            </div>
            <div class="modal-body">
                <br/>                
                <label class="horAnt">Horario Anterior </label><input type="checkbox" id="HorAnterior" class="horAnt">
                <table  id="tblAsigHorProf" class="table table-striped table-bordered">
                    <thead>
                   <th>Hora/Dia </th>
                    <th>Lunes</th>
                    <th>Martes</th>
                    <th>Miercoles</th>
                    <th>Jueves</th>
                    <th>Viernes</th>                        
                    <th>Sabado</th>                        
                    </thead>
                    <tbody>
                        <tr data-hora="7-8">
                            <td>7:00-8:00</td>
                            <td class="sel" data-day="0"></td>
                            <td class="sel" data-day="1"></td>
                            <td class="sel" data-day="2"></td>
                            <td class="sel" data-day="3"></td>
                            <td class="sel" data-day="4"></td>
                            <td class="sel" data-day="5"></td> 
                        </tr>
                        <tr data-hora="8-9">
                            <td>8:00-9:00</td>
                            <td class="sel" data-day="0"></td>
                            <td class="sel" data-day="1"></td>
                            <td class="sel" data-day="2"></td>
                            <td class="sel" data-day="3"></td>
                            <td class="sel" data-day="4"></td>  
                            <td class="sel" data-day="5"></td> 
                        </tr>
                        <tr data-hora="9-10">
                            <td>9:00-10:00</td>
                            <td class="sel" data-day="0"></td>
                            <td class="sel" data-day="1"></td>
                            <td class="sel" data-day="2"></td>
                            <td class="sel" data-day="3"></td>
                            <td class="sel" data-day="4"></td> 
                            <td class="sel" data-day="5"></td> 
                        </tr>
                        <tr data-hora="10-11">
                            <td>10:00-11:00</td>
                           <td class="sel" data-day="0"></td>
                            <td class="sel" data-day="1"></td>
                            <td class="sel" data-day="2"></td>
                            <td class="sel" data-day="3"></td>
                            <td class="sel" data-day="4"></td>
                            <td class="sel" data-day="5"></td> 
                        </tr>
                        <tr data-hora="11-12">
                            <td>11:00-12:00</td>
                            <td class="sel" data-day="0"></td>
                            <td class="sel" data-day="1"></td>
                            <td class="sel" data-day="2"></td>
                            <td class="sel" data-day="3"></td>
                            <td class="sel" data-day="4"></td>
                            <td class="sel" data-day="5"></td> 
                        </tr>
                        <tr data-hora="12-13">
                            <td>12:00-13:00</td>
                            <td class="sel" data-day="0"></td>
                            <td class="sel" data-day="1"></td>
                            <td class="sel" data-day="2"></td>
                            <td class="sel" data-day="3"></td>
                            <td class="sel" data-day="4"></td>
                            <td class="sel" data-day="5"></td> 
                        </tr>
                        <tr data-hora="13-14">
                            <td>13:00-14:00</td>
                            <td class="sel" data-day="0"></td>
                            <td class="sel" data-day="1"></td>
                            <td class="sel" data-day="2"></td>
                            <td class="sel" data-day="3"></td>
                            <td class="sel" data-day="4"></td>
                            <td class="sel" data-day="5"></td> 
                        </tr>
                        <tr data-hora="14-15">
                            <td>14:00-15:00</td>
                            <td class="sel" data-day="0"></td>
                            <td class="sel" data-day="1"></td>
                            <td class="sel" data-day="2"></td>
                            <td class="sel" data-day="3"></td>
                            <td class="sel" data-day="4"></td>
                            <td class="sel" data-day="5"></td> 
                        </tr>
                        <tr data-hora="15-16">
                            <td>15:00-16:00</td>
                            <td class="sel" data-day="0"></td>
                            <td class="sel" data-day="1"></td>
                            <td class="sel" data-day="2"></td>
                            <td class="sel" data-day="3"></td>
                            <td class="sel" data-day="4"></td>
                            <td class="sel" data-day="5"></td> 
                        </tr>
                        <tr data-hora="16-17">
                            <td>16:00-17:00</td>
                            <td class="sel" data-day="0"></td>
                            <td class="sel" data-day="1"></td>
                            <td class="sel" data-day="2"></td>
                            <td class="sel" data-day="3"></td>
                            <td class="sel" data-day="4"></td>
                            <td class="sel" data-day="5"></td> 
                        </tr>
                        <tr data-hora="17-18">
                            <td>17:00-18:00</td>
                            <td class="sel" data-day="0"></td>
                            <td class="sel" data-day="1"></td>
                            <td class="sel" data-day="2"></td>
                            <td class="sel" data-day="3"></td>
                            <td class="sel" data-day="4"></td>
                            <td class="sel" data-day="5"></td> 
                        </tr>
                        <tr data-hora="18-19">
                            <td>18:00-19:00</td>
                            <td class="sel" data-day="0"></td>
                            <td class="sel" data-day="1"></td>
                            <td class="sel" data-day="2"></td>
                            <td class="sel" data-day="3"></td>
                            <td class="sel" data-day="4"></td>
                            <td class="sel" data-day="5"></td> 
                        </tr>
                        <tr data-hora="19-20"> 
                            <td>19:00-20:00</td>
                            <td class="sel" data-day="0"></td>
                            <td class="sel" data-day="1"></td>
                            <td class="sel" data-day="2"></td>
                            <td class="sel" data-day="3"></td>
                            <td class="sel" data-day="4"></td>
                            <td class="sel" data-day="5"></td> 
                        </tr>
                        <tr data-hora="20-21">
                            <td>20:00-21:00</td>
                            <td class="sel" data-day="0"></td>
                            <td class="sel" data-day="1"></td>
                            <td class="sel" data-day="2"></td>
                            <td class="sel" data-day="3"></td>
                            <td class="sel" data-day="4"></td>
                            <td class="sel" data-day="5"></td> 
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button id="btnCancelarHorariosProf" type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <button id="btnAceptarHorariosProf" type="button" class="btn btn-primary">Aceptar</button>
            </div>
        </div>
    </div>
</div>
