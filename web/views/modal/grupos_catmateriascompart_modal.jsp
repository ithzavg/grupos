<%-- 
    Document   : catalogoMateriasComp
    Created on : 15/02/2015, 11:52:48 PM
    Author     : Edgar
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!--MODAL INICIO-->
<div class="modal fade " id="divModalCatMatComp" data-backdrop="static">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Busqueda Grupos</h4>
            </div>
            <div class="modal-body ">
                <div class="row">                    
                    <div class="col-md-4">
                        <label for="selCarrera">Plan</label>
                        <select id="selCarrera" class="form-control"  >                            
                        </select>                            
                    </div>
                </div>
                <br/>
                <table id="tblCatGrupos" class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th>Grupo</th>
                            <th>Materia</th>                            
                            <th>Cap</th>
                            <th>Insc</th>                            
                            <th>Plan</th>                                                                                  
                            <th>Espec</th>                            
                            <th>Profesor</th>                            
                            <th></th>                            
                        </tr>
                    </thead>
                    <tbody id="tbodyGruposCat">
                                          
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button id="btnAceptarCatGrup" type="button" class="btn btn-primary" data-dismiss="modal">Aceptar</button>                
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>                
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!--MODAL FINAL-->