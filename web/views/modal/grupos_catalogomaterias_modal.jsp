<%-- 
    Document   : catalogoMaterias
    Created on : 14/02/2015, 09:19:28 PM
    Author     : Edgar
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!--MODAL INICIO-->
<div class="modal fade " id="divModalCatMaterias" data-backdrop="static">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Busqueda Materias</h4>
            </div>
            <div class="modal-body ">                

                <table id="tblCatMaterias" class="table table-striped table-bordered" >
                    <thead>
                        <tr>
                            <th>Clave</th>
                            <th>Nombre</th>
                            <th>Creditos</th>                            
                            <th>Academia</th>                            
                        </tr>
                    </thead>
                    <tbody id="tbodyMaterias">

                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal" style="float: right;">Cerrar</button>                
                <button id="btnAceptarEq" type="button" class="btn btn-primary" style="display: none; float: right;">Aceptar</button>
                <button id="btnAceptarCatMat" type="button" class="btn btn-primary" style="float: right;">Aceptar</button> 
                
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!--MODAL FINAL-->
