<%-- 
    Document   : gruposGeneral_view
    Created on : 8/02/2015, 09:52:37 PM
    Author     : Edgar
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">        
        <link href="css/lib/bootstrap.min.css?${current}" rel="stylesheet">                
        <link href="css/lib/dataTables.bootstrap.css?${current}" rel="stylesheet">    
        <link href="css/lib/font-awesome.min.css?${current}" rel="stylesheet">        
        <link href="css/grupos.css?${current}" rel="stylesheet">  
        <link href="css/lib/jquery-ui.css?${current}" rel="stylesheet"> 
        <link rel="icon" href="img/logo-final.png">
        <title>Grupos</title>
    </head>
    <body>
        <%@include file="../modal/reportes_listainscritos_modal.jsp"%>
        <%@include file="../modal/reportes_mapeogrupos_modal.jsp"%>
        <%@include file="../modal/grupos_catalogoaulas_modal.jsp"%>
        <%@include file="../modal/grupos_catalogomaterias_modal.jsp"%>         
        <%@include file="../modal/grupos_catmateriascompart_modal.jsp" %>
        <%@include file="../general/header.jsp" %>

        <div class="container">
            <%@include file="../general/navbarGrupos.jsp" %>
            <%@include file="../general/panelGeneral.jsp" %>

            <div class="panel panel-default">
                <div class="panel-body">
                    <div role="tabpanel">

                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist" id="ulTabGrupos">
                            <li role="presentation" class="active" data-des="1"><a href="#divTabBienvenido" aria-controls="divTabBienvenido" role="tab" data-toggle="tab">Bienvenido</a></li>
                            <li role="presentation" data-des="2"><a href="#divTabPreparacion" aria-controls="divTabPreparacion" role="tab" >                                    
                                    <c:choose>
                                        <c:when test="${opPeriodo == 1}">
                                            Actuales
                                        </c:when>
                                        <c:when test="${opPeriodo == 2}">
                                            Preparación
                                        </c:when>
                                        <c:when test="${opPeriodo == 3}">  
                                            Intersemestral
                                        </c:when>
                                        <c:otherwise>
                                            Periodo Invalido
                                        </c:otherwise>   
                                    </c:choose>
                                </a></li>
                            <li role="presentation" data-des="3"><a href="#divTabCompPrep" aria-controls="divTabCompPrep" role="tab">Compartidos</a></li>                            
                                <c:if test="${objUsuario.getModalidad() == 0}">
                                <li role="presentation"><a href="#divTabReportes" aria-controls="divTabReportes" role="tab" data-toggle="tab">Reportes</a></li>                            
                                </c:if>
                        </ul>
                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="divTabBienvenido">
                                <br/>
                                <div class="jumbotron" style='text-align: justify;'>
                                    <h1>Bienvenido</h1>
                                    <p><strong>Reales: </strong></p>
                                    <p>En este apartado se puede agregar, modificar y eliminar los grupos reales que se requieran</p>
                                    <p><strong>Compartidos: </strong></p>
                                    <p>En este apartado se pueden agregar grupos compartidos en base a los grupos reales ya existentes</p>
                                    <br/>
                                    <!--<p><center><a class="btn btn-primary btn-lg" href="#" role="button">Ayuda</a></center></p>-->
                                </div>
                            </div>                            
                            <div role="tabpanel" class="tab-pane fade" id="divTabPreparacion">
                                <br/>
                                <div align="right">
                                    <button  style="float: right;" class="btn btn-primary btn-sm" 
                                             id="btnUpdate" data-toggle="tooltip" data-placement="top" 
                                             title="Ver en tiempo real la cantidad de alumnos inscritos">Actualizar&nbsp;<i class="fa fa-refresh"></i></button>
                                </div>
                                <br/>                                
                                <table id="tblGrpPreparacion" class="table table-striped table-bordered" >
                                    <thead>
                                        <tr>
                                            <th><input id="colSearch1" type="text" placeholder="Buscar:" style="width:80%;"/></th>
                                            <th><input id="colSearch2" type="text" placeholder="Buscar:" style="width:80%;"/></th>
                                            <th><input id="colSearch3" type="text" placeholder="Buscar:" style="width:80%;"/></th>
                                            <th><input id="colSearch4" type="text" placeholder="Buscar:" style="width:80%;"/></th>
                                            <th><input id="colSearch5" type="text" placeholder="Buscar:" style="width:80%;"/></th>
                                            <th><input id="colSearch6" type="text" placeholder="Buscar:" style="width:80%;"/></th>
                                            <th><input id="colSearch7" type="text" placeholder="Buscar:" style="width:80%;"/></th>                                            
                                            <th><input id="colSearch8" type="text" placeholder="Buscar:" style="width:80%;"/></th>
                                            <th><input id="colSearch9" type="text" placeholder="Buscar:" style="width:80%;"/></th>
                                            <th><input id="colSearch0" type="text" placeholder="Buscar:" style="width:80%;"/></th>
                                            <!-- sis01 2 carreras, qui01 1 carrera, ind01 3 carreras -->
                                                <c:if test="${opPeriodo == 1 || opPeriodo == 3}">
                                                <th></th>
                                                <th></th>
                                                <th></th>
                                                </c:if>
                                            <th></th>
                                        </tr>
                                        <tr>
                                            <th>Blq</th>
                                            <th>Grp</th>
                                            <th>Materia</th>
                                            <th>Cap</th>
                                            <th>Lun</th>
                                            <th>Mar</th>
                                            <th>Mie</th>
                                            <th>Jue</th>                                            
                                            <th>Vie</th>
                                            <th>Sab</th>
                                                <c:if test="${opPeriodo == 1 || opPeriodo == 3}">
                                                <th>Ins</th>
                                                <th>InsT</th> 
                                                <th></th> 
                                                </c:if>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody id="tbodyGruposP">
                                    </tbody>
                                </table>

                                <div align="right">
                                    <button class="btn btn-primary  btn-sm" id="btnGuardarP"><i class="fa fa-floppy-o"></i></button>
                                    <button type="button" class="btn btn-success btn-sm" id="btnAgregarGrupoP"><i class="glyphicon glyphicon-plus"></i></button>
                                    <button type="button" class="btn btn-danger btn-sm" id="btnElimarP"><i class="glyphicon glyphicon-minus"></i></button>
                                </div>    


                            </div>                            
                            <div role="tabpanel" class="tab-pane" id="divTabCompPrep">
                                <br/>
                                <div align="rigth">
                                    <button  style="float: right;" class="btn btn-primary btn-sm" 
                                             id="btnUpdateC" data-toggle="tooltip" data-placement="top" 
                                             title="Ver en tiempo real la cantidad de alumnos inscritos">Actualizar&nbsp;<i class="fa fa-refresh"></i></button>
                                </div>
                                <br/>
                                <br/>
                                <table id="tblGrpPrepCompar" class="table table-bordered table-responsive tablesorter ">
                                    <thead>
                                        <tr>                                            
                                            <th>Blq</th>                                                                                        
                                            <th>GrpRl</th>                                                                                        
                                            <th>Mat</th>                                                                                        
                                            <th>GrpCp</th>                                            
                                            <th>Cap</th>
                                            <th>Lun</th>
                                            <th>Mar</th>
                                            <th>Mie</th>
                                            <th>Jue</th>
                                            <th>Vie</th>
                                            <th>Sab</th>
                                            <th>Compartido</th> 
                                                <c:if test="${opPeriodo == 1 || opPeriodo == 3}">
                                                <th>Ins</th>
                                                <th>InsT</th>
                                                </c:if>
                                            <th></th>                                            
                                        </tr>
                                    </thead>
                                    <tbody id="tbodyGruposPC">                                        
                                    </tbody>
                                </table>
                                <div align="right">
                                    <button id="btnGuardarC" class="btn btn-primary  btn-sm"><i class="fa fa-floppy-o"></i></button>
                                    <button id="btnAgregarGrupoPC" type="button" class="btn btn-success btn-sm"><i class="glyphicon glyphicon-plus"></i></button>
                                    <button id="btnElimarC" type="button" class="btn btn-danger btn-sm"><i class="glyphicon glyphicon-minus"></i></button>
                                </div>
                            </div>                            
                            <div role="tabpanel" class="tab-pane fade" id="divTabReportes">
                                <br/>                                

                                <div class="col-md-6">
                                    <ul>
                                        <li>
                                            <p>
                                                <a data-toggle="collapse" href="#collapseHorarios" aria-expanded="false" aria-controls="collapseHorarios">
                                                    Generacion de Mapeo de Grupos
                                                </a>
                                                <button id="btnRepHor" class=" col-md-offset-5 btn btn-warning btn-sm"><i class="fa fa-file-pdf-o"></i></button>
                                            </p>    
                                            <div class="collapse" id="collapseHorarios">
                                                <div class="well">
                                                    Generacion de reporte donde se ven reflejados los grupos que tiene asignada cada aula
                                                    a lo largo de la semana
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <p>
                                                <a data-toggle="collapse" href="#collapseInscritos" aria-expanded="false" aria-controls="collapseInscritos">
                                                    Generacion de Lista de Inscritos&nbsp;&nbsp;
                                                </a>
                                                <button id="btnLiIns" class=" col-md-offset-5 btn btn-warning btn-sm"><i class="fa fa-file-pdf-o"></i></button>
                                            </p>    
                                            <div class="collapse" id="collapseInscritos">
                                                <div class="well">
                                                    Generación de la lista de inscritos ordenados por fecha 
                                                    de un grupo en específico
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>    

                            </div>
                        </div>
                    </div>
                </div>                
            </div>
        </div>
        <%@include file="../general/footer.jsp" %>
        <script src="js/lib/jquery-1.11.1.min.js?${current}"></script>  
        <script src="js/lib/jquery-ui.js?${current}"></script> 
        <script src="js/lib/bootstrap.min.js?${current}"></script>        
        <script src="js/lib/jquery.dataTables.js?${current}"></script>      
        <script src="js/lib/dataTables.bootstrap.js?${current}"></script>      
        <script src="js/lib/bootbox.min.js?${current}"></script>      
        <script src="js/gruposGeneral_handler.js?${current}"></script>      
        <script src="js/grupos_handler.js?${current}"></script>      
        <script src="js/gruposCompartidos_handler.js?${current}"></script>      
        <script src="js/panelGeneral.js?${current}"></script>              
    </body>
</html>
