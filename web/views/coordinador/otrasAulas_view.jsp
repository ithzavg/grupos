<%-- 
    Document   : aulasOtrasCarreras_view
    Created on : 9/02/2016, 12:54:19 PM
    Author     : IthzaVG
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">        
        <link href="css/lib/bootstrap.min.css?${current}" rel="stylesheet">                
        <link href="css/lib/dataTables.bootstrap.css?${current}" rel="stylesheet">    
        <link href="css/lib/font-awesome.min.css?${current}" rel="stylesheet">        
        <link href="css/grupos.css?${current}" rel="stylesheet">  
        <link href="css/lib/jquery-ui.css?${current}" rel="stylesheet"> 
        <link rel="icon" href="img/logo-final.png">
        <title>Otras aulas</title>
    </head>
    <body>

        <div class="container">
            <!-- Header!-->
            <div class="row"> 
                <div class="col-md-12">
                    <%@include file="../general/header.jsp" %>
                </div>
            </div><br>
            <!-- Fin de header!-->
            <%@include file="../general/navbarPrestamoAulas.jsp" %>
            <div class="row">
                <div class="col-md-3">
                    <%@include file="../general/menuPrestamoAulas.jsp" %>
                </div>

                <div class="col-md-9">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h1 class="panel-title">Aulas prestadas</h1>
                        </div>
                        <div class="panel-body">

                            <div class="alert alert-warning" role="alert">
                                <p> Bienvenido en este apartado podr&aacute; ver las aulas que
                                    le prestan otras carreras</p>
                            </div>

                            <table class="table table-striped table-bordered dt-responsive nowrap" id="oAulasPrestadas">
                                <thead>
                                    <tr>
                                        <th><input id="colSearch1" type="text" placeholder="Buscar:" style="width:80%;" class="form-control"/></th>
                                        <th><input id="colSearch2" type="text" placeholder="Buscar:" style="width:80%;" class="form-control"/></th>
                                        <th><input id="colSearch3" type="text" placeholder="Buscar:" style="width:80%;" class="form-control"/></th>
                                        <th><input id="colSearch4" type="text" placeholder="Buscar:" style="width:80%;" class="form-control"/></th>
                                        <th><input id="colSearch5" type="text" placeholder="Buscar:" style="width:80%;" class="form-control"/></th>
                                        <th><input id="colSearch6" type="text" placeholder="Buscar:" style="width:80%;" class="form-control"/></th>
                                    </tr>
                                    <tr>
                                        <td>Edificio</td>
                                        <td>Aula</td>
                                        <td>Carrera propietario</td>
                                        <td>Carrera prestamo</td>
                                        <td>Horario</td>
                                        <td>D&iacute;a</td>

                                    </tr>
                                </thead>

                                <tbody>
                                    <c:forEach items="${otrasAulas}" var="op">
                                        <tr>
                                            <td>${op.getEdificio()}</td>
                                            <td>${op.getAula()}</td>
                                            <td>${op.getCarrera_propietario()}</td>
                                            <td>${op.getCarrera_prestamo()}</td>
                                            <td>${op.getHorario()}</td>
                                            <td>${op.getDia()}</td>
                                        </tr>
                                    </c:forEach>
                                </tbody>
                            </table>
                        </div>
                    </div>

                </div>

            </div>

        </div>


        <%@include file="../general/footer.jsp" %>
        <script src="js/lib/jquery-1.11.1.min.js?${current}"></script>  
        <script src="js/lib/jquery-ui.js?${current}"></script> 
        <script src="js/lib/bootstrap.min.js?${current}"></script>        
        <script src="js/lib/jquery.dataTables.js?${current}"></script>      
        <script src="js/lib/dataTables.bootstrap.js?${current}"></script>      
        <script src="js/lib/bootbox.min.js?${current}"></script>
        <script src="js/prestamoAulas_handler.js?${current}"></script>
        
    </body>
</html>
