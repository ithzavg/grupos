<%-- 
    Document   : prestamo_aulas
    Created on : 8/02/2016, 01:02:21 PM
    Author     : IthzaVG
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">        
        <link href="css/lib/bootstrap.min.css?${current}" rel="stylesheet">                
        <link href="css/lib/dataTables.bootstrap.css?${current}" rel="stylesheet">    
        <link href="css/lib/font-awesome.min.css?${current}" rel="stylesheet">        
        <link href="css/grupos.css?${current}" rel="stylesheet">  
        <link href="css/lib/jquery-ui.css?${current}" rel="stylesheet"> 
        <link rel="icon" href="img/logo-final.png">
        <title>Prestamo de aulas</title>
    </head>
    <body>
        <div class="container">
            <!-- Header!-->
            <div class="row"> 
                <div class="col-md-12">
                    <%@include file="../general/header.jsp" %>
                </div>
            </div><br>
            <!-- Fin de header!-->
            <%@include file="../general/navbarPrestamoAulas.jsp" %>

            <div class="row">
                <div class="col-md-3">
                    <%@include file="../general/menuPrestamoAulas.jsp" %>
                </div>

                <div class="col-md-9">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title">Prestamo de Aulas</h3>
                        </div>
                        <div class="panel-body">
                            <div class="alert alert-warning" role="alert">

                                <p> Bienvenido en este apartado podrá asignar aulas disponibles a otras carreras. 
                                    Complete el formulario correctamente</p>
                            </div>
                            <form class="col-md-6 col-md-offset-3" id="formPA">
                                <div class="form-group">
                                    <label>Edificio</label><br>

                                    <select class="form-control input-sm" id="selEdificio" name="selEdificio">
                                        <option value="#">Selecciona el edifcio</option>

                                        <c:forEach items="${edificios}" var="edificio">

                                            <option value="${edificio.getEdificioId()}">
                                                ${edificio.getNombre()}
                                            </option>
                                        </c:forEach>                                

                                    </select>
                                </div>



                                <div class="form-group">
                                    <label for="aula">Aula</label><br>
                                    <select class="form-control input-sm" id="selAula" name="selAula">
                                        <option value="#">No se ha seleccionado Edificio</option>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label>Asignar a</label><br>
                                    <select class="form-control input-sm" id="selCar" name="selCar">
                                        <option value="#">Selecciona la carrera</option>
                                        <c:forEach items="${carreras}" var="carrera">
                                            <option value="${carrera.getCarreraId()}">
                                                ${carrera.getNombre()}
                                            </option>
                                        </c:forEach>

                                    </select>
                                </div>

                                <div class="row">

                                    <div class="col-xs-4">
                                        <label>Hora Inicio</label>
                                        <input type="text" class="form-control" placeholder="7-21hrs" id="hora_inicio" name="hora_inicio" required="">
                                    </div>
                                    <div class="col-xs-4">
                                        <label>Hora Fin</label>
                                        <input type="text" class="form-control" placeholder="7-21hrs" id="hora_fin" name="hora_fin" required="">
                                    </div>
                                    <div class="col-xs-4">
                                        <label>D&iacute;a</label>
                                        <select class="form-control input-sm" id="dia" name="dia"> 
                                            <option value="#">D&iacute;a</option>
                                            <option value="1">Lunes</option>
                                            <option value="2">Martes</option>
                                            <option value="3">Mi&eacute;rcoles</option>
                                            <option value="4">Jueves</option>
                                            <option value="5">Viernes</option>
                                            <option value="6">S&aacute;bado</option>
                                        </select>
                                    </div>
                                </div>
                                <br>

                                <button type="submit" class="btn btn-primary" id="btnGuardaPA">Aceptar</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <%@include file="../general/footer.jsp" %>
        <script src="js/lib/jquery-1.11.1.min.js?${current}"></script>  
        <script src="js/lib/jquery-ui.js?${current}"></script> 
        <script src="js/lib/bootstrap.min.js?${current}"></script>        
        <script src="js/lib/jquery.dataTables.js?${current}"></script>      
        <script src="js/lib/dataTables.bootstrap.js?${current}"></script>      
        <script src="js/lib/bootbox.min.js?${current}"></script>
        <script src="js/prestamoAulas_handler.js?${current}"></script>

    </body>
</html>
