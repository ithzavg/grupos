/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

window.onload = function () {
    nobackbutton();
};

function nobackbutton() {
    window.location.hash = "no-back-button";
    window.location.hash = "Again-No-back-button"; //chrome
    window.onhashchange = function () {
        window.location.hash = "no-back-button";
    };
}


/**
 * 
 * Tablas con busqueda por columnas
 */

$(document).ready(function () {

    aPrestadas = $("#aPrestadas").DataTable({
       
        "language": {
            "paginate": {
                "next": "Siguiente",
                "previous": "Anterior"
            },
            "search": "Buscar: ",
            "zeroRecords": "No hay aulas prestadas",
            "lengthMenu": "Mostrar _MENU_ resultados"

        },
        "responsive": true,
        "info": false,
        "sDom": "lrtip",
//        columnDefs: [
//            {targets: [0, 1, 2, 3, 4, 5], visible: true},
//            {targets: '_all', visible: false}
//        ]

        "columnDefs": [
            {
                "targets": [6],
                "visible": false,
                "searchable": false
            },
            {
                "targets": [7],
                "visible": false
            }
        ]
    });
    var i = 0;
    aPrestadas.columns().every(function () {
        var that = this;

        $('#colSearch' + (++i)).on('keyup change', function () {
            that
                    .search(this.value)
                    .draw();
        });
    });

    oAulasPrestadas = $("#oAulasPrestadas").DataTable({
        "language": {
            "paginate": {
                "next": "Siguiente",
                "previous": "Anterior"
            },
            "search": "Buscar: ",
            "zeroRecords": "No hay aulas de otras carreras",
            "lengthMenu": "Mostrar _MENU_ resultados"
        },
        "responsive": true,
        "info": false,
        "sDom": "lrtip"
    });

    var i = 0;
    oAulasPrestadas.columns().every(function () {
        var that = this;

        $('#colSearch' + (++i)).on('keyup change', function () {
            that
                    .search(this.value)
                    .draw();
        });
    });





    /*
     * Funcion que obtiene una cadena JSON de las aulas por una petición POST
     * a prestamo aula segun el edificio que se haya seleccionado
     */

    $("#selEdificio").change(function () {
        var option = $('option:selected', this).attr('value');
        var $select = $('#selAula');
        if (option === '#') {
            $select.find('option').remove();
            $('<option>').val('#').text('No se ha seleccionado edificio').appendTo($select);
        } else {
            $.ajax({
                url: "prestamoaula.do",
                type: 'POST',
                async: false,
                dataType: 'json',
                data: {paEdificioId: option, opcion: 1},
                success: function (aulas) {
                    var paCodError = aulas.paCodError;
                    if (Number(paCodError) === 0) {
                        $select.find('option').remove();
                        $.each(aulas.retorno, function (key, value) {
                            $('<option>').val(value.aula).text(value.aula).appendTo($select);
                        });

                        $('#selAula').change();
                    } else {
                        bootbox.alert(aulas.paDesError);
                    }

                }
            });
        }
    });

    /**
     * Funcion que verifica los campos en blanco y guarda un prestamo de aula
     */

    $('#btnGuardaPA').click(function ()
    {

        var verificaCampos = true;

        if (document.getElementById("selAula").value === '#')
        {
            verificaCampos = false;
        }
        if (document.getElementById("selEdificio").value === '#')
        {
            verificaCampos = false;

        }
        if (document.getElementById("selCar").value === '#')
        {
            verificaCampos = false;

        }
        if (isNaN(document.getElementById("hora_inicio").value))
        {
            verificaCampos = false;

        }
        if (isNaN(document.getElementById("hora_fin").value))
        {
            verificaCampos = false;

        }

        if (document.getElementById("dia").value === '#')
        {
            verificaCampos = false;

        }

        if (!verificaCampos)
        {
            bootbox.alert("Algunos campos están incorrectos vuelva a revisarlos");

        } else
        {
            var datosPrestamo = {
                edificio: $("#selEdificio").val(),
                aula: $("#selAula").val(),
                carrera_prestamo: $("#selCar").val(),
                hora_inicio: $("#hora_inicio").val(),
                hora_fin: $("#hora_fin").val(),
                dia: $("#dia").val()};

            $.ajax({
                type: "POST",
                async: false,
                url: "guardapa.do",
                data: {
                    prestamo: JSON.stringify(datosPrestamo)
                },
                success: function (retorno) {

                    if (Number(retorno.paCodError) === 0)
                    {
                        bootbox.alert("Su prestamo se ha guardado correctamente");


                    } else {
                        bootbox.alert(retorno.paDesError);
                    }

                }
            });
        }

    });




    function eliminarPrestamo(paEdificio, paAula, paCarreraProp, paHorario, paDia, paCarreraPrestamo, tblPA) {
        $.ajax({
            url: "eliminaprestamo.do",
            async: false,
            type: 'POST',
            data: {paAula: paAula
                , paEdificio: paEdificio
                , paHorarioAula: paHorario
                , paCarreraProp: paCarreraProp
                , paDia: paDia
                , paCarreraPrestamo: paCarreraPrestamo


            },
            success: function (data) {
                if (Number(data) === 0) {
                    tblPA.row('.selected').remove().draw(false);
                } else {
                    bootbox.alert(data);
                }
            }
        });
    }

    /**
     * Función que elimina un prestamo de Aulas prestadas 
     */
    var d;
    var btn = document.getElementById('btnEliminaPA');
    btn.disabled = true;
    $('#aPrestadas tbody').on('click', 'tr', function () {

        if ($(this).hasClass('selected'))
        {
            $(this).removeClass('selected');
            btn.disabled = true;


        } else {
            aPrestadas.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
            d = aPrestadas.row(this).data();

            btn.disabled = false;

        }


    });



    $('#btnEliminaPA').click(function () {

        var tr = $("#tBody tr.selected");
        var index = tr.attr("data-key");
        if (index !== undefined) {
            bootbox.confirm("¿Está seguro de eliminar este prestamo?", function (result) {
                if (result) {
                    var paEdificio = d[0];
                    var paAula = d[1];
                    var paCarreraProp = d[7];
                    var paHorario = d[4];
                    var paDia = d[6];
                    var paCarreraPrestamo = d[8];

                    eliminarPrestamo(paEdificio, paAula, paCarreraProp, paHorario, paDia, paCarreraPrestamo, aPrestadas);



                    btn.disabled = true;

                } else {
                    btn.disabled = true;
                }
            });
        } else {
            bootbox.alert('index undefined');
        }

    });
});



