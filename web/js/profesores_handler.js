/* 
 * Titulo: Handler para el modulo de Jefe de Departamento
 * 
 * Nombre: profesores_handler
 * 
 * Autor: Edgar Cirilo González
 * 
 * 
 */

function onChangeParamJefDpto() {
    $('#ulTabJefeDepto a:first').tab('show');
    if ($.fn.DataTable.isDataTable('#tblGrpJefDpto')) {
        tblGrpJefDpto.destroy();
    }
    if ($.fn.DataTable.isDataTable('#tblHorarioProf')) {
        tblHorarioProf.destroy();
    }
    $('tbodyGruposPJ').empty();
    $('tbodyProfesoresA').empty();
}
;

/*
 * 
 * @param {type} paPeriodo[1|2] 
 * @returns {String}
 */
function getStringPeriodo(paPeriodo) {
    if (paPeriodo === 2) {
        return "Enero-Junio ";
    } else {
        return "Agosto-Diciembre ";
    }
}
/*
 * Descripción: llena la tabla de catalogo profesores con los datos obtenidos
 * con la petición al servidor.
 * 
 * @param {type} paAcademiaId:
 * @returns {undefined}
 */
function requestGetProfesores(paAcademiaId, paGrupoId, paPeriodo, paAño) {
    if ($.fn.DataTable.isDataTable('#tblCatProfesores')) {
        tblCatProfesores.destroy();
    }
    $.ajax({
        url: "catalogoprofesores.do",
        type: 'POST',
        async: false,
        dataType: 'json',
        data: {
            paAcademiaId: paAcademiaId,
            paGrupoId: paGrupoId,
            paPeriodo: paPeriodo,
            paAño: paAño,
            opcion: 1
        },
        success: function (profGen) {

            var paCodError = profGen.paCodError;

            if (Number(paCodError) === 0) {
                lProfes = profGen.retorno;
                var tbody = $("#tbodyProfesores");

                tbody.find("tr").remove();
                $.each(profGen.retorno, function (key, object) {
                    var tr = $("<tr></tr>");
                    tbody.append(tr);
                    tr.attr("data-key", key);
                    tr.addClass(object.trClass);

                    var td = $("<td>" + object.pro_Id + "</td>");
                    tr.append(td);
                    td.addClass(object.idClass);

                    var td = $("<td>" + object.nombre_Prof + "</td>");
                    tr.append(td);

                    var td = $("<td>" + object.academia_Id + "</td>");
                    tr.append(td);

                    var td = $("<td>" + object.horas_ocup + "</td>");
                    tr.append(td);

                    var td = $("<td>" + object.OBS + "</td>");
                    tr.append(td);
                });
            } else {
                bootbox.alert(profGen.paDesError);
            }


        },
        complete: function () {
            tblCatProfesores = $('#tblCatProfesores').DataTable({
                "language": {
                    "paginate": {
                        "next": "Siguiente",
                        "previous": "Anterior"
                    },
                    "search": "Buscar: ",
                    "zeroRecords": "No se encontraron coincidencias"
                },
                "info": false,
                "destroy": true
                , "lengthChange": false
            });
        }
    });
}

$(document).ready(function () {


    // ********************* DECLARACION DE VARIABLES **************************

    var paPeriodo;        // Almacena el periodo que se encuentra seleccionado
    var paAño;            // Almacena el año correspondiente al periodo seleccionado
    var paLeyenda;        // Almacena la descripcion del periodo
    var paOpcionCatalogo; // Almacena la opción de los catalogos segun el periodo
    lProfes = [];         // Inicializa un arreglo que almacena los datos del catalogo de profesores
    dataProf = [];        // Inicializa un arreglo que almacena los datos de los grupos a los que se les asigna profesor


    // ****************** EVENTOS PARA ELEMENTOS <select> **********************


    /*
     * Objetivo: Filtrar el catalogo de profesores según academia
     * 
     * Descripción:
     * 
     * Escucha un evento change en el elemento selAcademia, cuando se
     * selecciona una opcion se obtiene el ID de la academia seleccionada, se eliminan
     * los datos de la tabla y se llena con los nuevos datos segun la academia seleccionada.
     * 
     * Tag:  <select>
     * Id:   #selAcademia
     * View: profesores_catalogoprofesores_modal.jsp
     *  
     */
    $("#selAcademia").change(function () {
        var paAcademiaId = $('option:selected', this).attr('value');
        var paGrupoId = $(this).attr("data-gpoId");

        if ($.fn.DataTable.isDataTable('#tblCatProfesores')) {
            tblCatProfesores.destroy();
        }

        requestGetProfesores(paAcademiaId, paGrupoId, paPeriodo, paAño);
    });

    /*
     * Objetivo: Cambiar de periodo, actuales o preparación.
     * 
     * Descripción: 
     * 
     * Escucha un evento change en el elemento selPeriodo, cuando se selecciona
     * actuales o preparación obtiene la opción a la que pertenece y se hace una
     * consulta a la base de datos de cual es año correspondiente al periodo 
     * seleccionado.
     * 
     * Tag:  <select>
     * Id:   #selPeriodo
     * View: navbarGrupos.jsp
     * 
     * 
     */
    $("#selPeriodo").change(function () {

        var paOpcion = $('option:selected', this).attr('value');
        if (!isNaN(paOpcion)) {
            $.ajax({
                url: "modulogrupos.do",
                type: 'POST',
                async: false,
                dataType: 'json',
                data: {
                    paOpcion: paOpcion,
                    opcion: 2
                },
                success: function (data) {
                    if (Number(data.paCodError) === 0) {
                        paPeriodo = data.extraParams.paPeriodo;
                        paAño = data.extraParams.paAño;
                        paLeyenda = data.extraParams.paLeyenda;
//                        if (paOpcion === '1') {
//                            paOpcionCatalogo = 1;
//                            $("#bPeriodoNavbar").text("Actuales " + getStringPeriodo(paPeriodo) + paAño);
//                        } else if (paOpcion === '2') {
//                            paOpcionCatalogo = 2;
//                            $("#bPeriodoNavbar").text("Preparación " + getStringPeriodo(paPeriodo) + paAño);
//                        }
                        paOpcionCatalogo = paOpcion;
                        $("#bPeriodoNavbar").text(paLeyenda);


                        $.ajax({
                            url: "catalogosGenericos.do",
                            type: 'POST',
                            async: false,
                            dataType: 'json',
                            data: {
                                paPeriodo: paPeriodo,
                                paAnio: paAño,
                                opcion: 2
                            },
                            success: function (carrGen) {
                                var paCodError = carrGen.paCodError;
                                if (Number(paCodError) === 0) {
                                    var $select = $("#selCarreras");

                                    $select.find('option').remove();

                                    if (carrGen.retorno.length > 0) {
                                        $.each(carrGen.retorno, function (key, value) {
                                            $('<option>').val(value.carrera_Id).html(value.checkClass + " " + value.nombre).appendTo($select);
                                        });
                                    } else {
                                        $('<option>').val("#").html("&#xf00c;" + " No hay grupos por asignar").appendTo($select);
                                    }


                                    $("#selCarreras").change();
                                } else {
                                    bootbox.alert(carrGen.paDesError);
                                }
                            }
                        });

                        onChangeParamJefDpto();
                        dataProf = [];



                    } else {

                        bootbox.alert(data.paDesError);
                    }
                }
            });
        }



    });

    /*
     * Objetivo: Obtener los planes de la carrera seleccionada
     * 
     * Descripción: 
     * 
     * Escucha un evento change en el elemento selCarreras, cuando una carrera 
     * es seleccionada se toma el ID de la carrera y se hace una consulta a la 
     * base de datos para obtener los planes de esa carrera, con base en esto
     * se llena el elemento selPlanes.
     * 
     * Tag:  <select>
     * Id:   #selCarreras
     * View: panelGeneral.jsp
     * 
     * 
     */
    $("#selCarreras").change(function () {

        var option = $('option:selected', this).attr('value');
        if (option === '#') {
            this.find('option').remove();
            $('<option>').val('#').text('No se ha seleccionado plan').appendTo($select);
        } else {
            $.ajax({
                url: "catalogosGenericos.do",
                type: 'POST',
                async: false,
                dataType: 'json',
                data: {
                    paCarreraId: option,
                    paPeriodo: paPeriodo,
                    paAnio: paAño,
                    opcion: 3
                },
                success: function (planGen) {
                    var paCodError = planGen.paCodError;
                    if (Number(paCodError) === 0) {
                        var $select = $('#selPlanes');

                        $select.find('option').remove();

                        $.each(planGen.retorno, function (key, value) {
                            $('<option>').val(value.plan_id).html(value.checkClass + " " + value.clave).appendTo($select);
                        });

                        $('#selPlanes').change();
                    } else {
                        bootbox.alert(planGen.paDesError);
                    }

                }
            });
        }

    });

    $("#selPlanes").change(function () {
        var option = $('option:selected', this).attr('value');
        var $select = $('#selEspec');
        if (option === '#') {
            $select.find('option').remove();
            $('<option>').val('#').text('No se ha seleccionado plan').appendTo($select);
        } else {
            $.ajax({
                url: "catalogosGenericos.do",
                type: 'POST',
                async: false,
                dataType: 'json',
                data: {paPlanId: option
                    , paPeriodo: paPeriodo
                    , paAnio: paAño
                    , opcion: 4},
                success: function (espGen) {
                    if (Number(espGen.paCodError) === 0) {

                        $select.find('option').remove();
                        $.each(espGen.retorno, function (key, value) {
                            $('<option>').val(value.esp_id).html(value.checkClass + " " + value.nombre_espec).appendTo($select);
                        });

                        $('#selEspec').change();

                    } else {

                        bootbox.alert(espGen.paDesError);
                    }

                }
            });
        }
    });


    $("#selEspec").change(function () {
        var option = $('option:selected', this).attr('value');
        $("#divEspecId").html(option);
        onChangeParamJefDpto();
    });


    // ************************ CONTROL DE PESTAÑAS ****************************


    /*
     * Objetivo: Abrir pestaña Asignar Profesor
     * 
     * Descripción: 
     * 
     * Espera un evento click en el elemento ulTabJefeDepto, si el elemento presionado
     * fue la opción divTabAsigProf, este hace una petición al servidor para obtener los
     * grupos que pertenecen a la carrera del Jefe de Departamento en la sesión y con estos
     * datos llenar la tabla e inicializar el elemento DataTable.
     *
     * Tag:  <ul>
     * Id:   #ulTabJefeDepto
     * View: profesores_view.jsp
     */
    $('#ulTabJefeDepto a[href="#divTabAsigProf"]').click(function (e) {
        e.preventDefault();

        if ($.fn.DataTable.isDataTable('#tblGrpJefDpto')) {
            tblGrpJefDpto.destroy();
        }
        var paPlanId = $('option:selected', "#selPlanes").attr('value');
        var paEspecId = $('option:selected', "#selEspec").attr('value');

        if (paPlanId === undefined || paPlanId === '#') {
            bootbox.alert("Debe seleccionar un plan");
        } else if (paEspecId === undefined || paEspecId === '#') {
            bootbox.alert("Debe seleccionar una especialidad");
        } else if (paOpcionCatalogo === undefined) {
            bootbox.alert("Debe seleccionar un periodo");
        } else {
            $.ajax({
                url: "gruposprofesores.do",
                type: 'POST',
                async: false,
                dataType: 'json',
                data: {
                    paPlanId: paPlanId,
                    paEspecId: paEspecId,
                    paOpcionCatalogo: paOpcionCatalogo

                },
                success: function (gruposGen) {
                    var paCodError = gruposGen.paCodError;
                    if (Number(paCodError) === 0) {
                        dataProf = gruposGen.retorno;
                        var tbody = $("#tbodyGruposPJ");

                        tbody.find("tr").remove();

                        $.each(gruposGen.retorno, function (key, object) {
                            var tr = $("<tr></tr>");
                            tbody.append(tr);
                            tr.attr("data-key", key);

                            var td = $("<td>" + object.grupo_Id + "</td>");
                            tr.append(td);


                            var td = $("<td>" + object.nom_Materia_Corto + "</td>");
                            tr.append(td);

                            if (object.H_LUN == undefined) {
                                object.H_LUN = "";
                            }
                            var td = $("<td data-day='1'>" + object.H_LUN + "</td>");
                            tr.append(td);

                            if (object.H_MAR == undefined) {
                                object.H_MAR = "";
                            }
                            var td = $("<td data-day='2'>" + object.H_MAR + "</td>");
                            tr.append(td);

                            if (object.H_MIE == undefined) {
                                object.H_MIE = "";
                            }
                            var td = $("<td data-day='3'>" + object.H_MIE + "</td>");
                            tr.append(td);

                            if (object.H_JUE == undefined) {
                                object.H_JUE = "";
                            }
                            var td = $("<td data-day='4'>" + object.H_JUE + "</td>");
                            tr.append(td);
                            if (object.H_VIE == undefined) {
                                object.H_VIE = "";
                            }

                            var td = $("<td data-day='5'>" + object.H_VIE + "</td>");
                            tr.append(td);


                            if (object.H_SAB == undefined) {
                                object.H_SAB = "";
                            }

                            var td = $("<td data-day='6'>" + object.H_SAB + "</td>");  //SABADO
                            tr.append(td);


                            var td = $("<td>" + object.acad_Imparte_Mat + "</td>");
                            tr.append(td);

                            var td = $("<td>" + ((object.nombre_Prof === undefined) ? "" : object.nombre_Prof) + "</td>");
                            tr.append(td);

                            var td = $("<td class='searchP'>" +
                                    "<a href='#'><i class='fa fa-user-plus'></i></a>"//:
                                    + "</td>");
                            tr.append(td);

                        });
                    } else {
                        bootbox.alert(gruposGen.paDesError);
                    }


                },
                complete: function () {

                    tblGrpJefDpto = $('#tblGrpJefDpto').DataTable({
                        "language": {
                            "paginate": {
                                "next": "Siguiente",
                                "previous": "Anterior"
                            },
                            "search": "Buscar: ",
                            "zeroRecords": "No se encontraron coincidencias"
                        },
                        "info": false,
                        "destroy": true
                        , "lengthChange": false
                    });

                }
            });
            $(this).tab("show");
        }
    });

    /*
     * Objetivo: Abrir la pestaña Asignar Horario
     * 
     * Descripción:
     * 
     * Espera un evento click en el elemento ulTabJefeDepto, si el elemento presionado
     * fue la opción divTabAsigHorario, realiza una petición al servidor para obtener
     * los maestros adscritos al departamento del usuario en la sesión, se llena la tabla y
     * se inicializa el elemento DataTable.
     * 
     * Tag:  <ul>
     * Id:   #ulTabJefeDepto
     * View: profesores_view.jsp
     * 
     */
    $('#ulTabJefeDepto a[href="#divTabAsigHorario"]').click(function (e) {
        e.preventDefault();

        if ($.fn.DataTable.isDataTable('#tblHorarioProf')) {
            tblHorarioProf.destroy();
        }

        if (paOpcionCatalogo === undefined) {
            bootbox.alert("Debe seleccionar un periodo");
        } else {
            $.ajax({
                url: "catalogoprofesores.do",
                type: 'POST',
                async: false,
                dataType: 'json',
                data: {
                    opcion: 2,
                    paPeriodo: paPeriodo,
                    paAño: paAño
                },
                success: function (profGen) {
                    var paCodError = profGen.paCodError;
                    if (Number(paCodError) === 0) {
                        lProfes = profGen.retorno;
                        var tbody = $("#tbodyProfesoresA");

                        tbody.find("tr").remove();

                        $.each(profGen.retorno, function (key, object) {
                            var tr = $("<tr></tr>");
                            tbody.append(tr);
                            tr.attr("data-key", key);

                            var td = $("<td>" + object.pro_Id + "</td>");
                            tr.append(td);

                            var td = $("<td>" + object.nombre_Prof + "</td>");
                            tr.append(td);

                            var td = $("<td>" + object.horas_asig + "</td>");
                            tr.append(td);
                            td.addClass("tdHoAsiClass");

                            var td = $("<td>" + object.horas_ocup + "</td>");
                            tr.append(td);

                            var td = $("<td class='horarioP'>" +
                                    "<a href='#'><i class='fa fa-calendar'></i></a>"//:
                                    + "</td>");
                            tr.append(td);

                        });
                    } else {

                        bootbox.alert(profGen.paDesError);
                    }

                }
            });

            tblHorarioProf = $("#tblHorarioProf").DataTable({
                "language": {
                    "paginate": {
                        "next": "Siguiente",
                        "previous": "Anterior"
                    },
                    "search": "Buscar: ",
                    "zeroRecords": "No se encontraron coincidencias"
                },
                "info": false,
                "destroy": true
                , "lengthChange": false
            });

            $(this).tab("show");
        }
    });

    $('#ulTabJefeDepto a[href="#divTabReportes"]').click(function (e) {

        if (paOpcionCatalogo === undefined) {
            bootbox.alert("Debe seleccionar un periodo");
        } else {
            $(this).tab("show");    
        }
    });

     // ********************** EVENTOS DE BOTONES **************************


    /*
     * Objetivo: Asignar profesor seleccionado
     * 
     * Descripción:
     * 
     * Escucha un evento click en el elemento btnAceptarCatProfesores, cuando 
     * se da click se obtiene el profesor seleccionado y se asigna al grupo que
     * llamo al modal, se hace una petición para guardar los cambios del grupo
     * en la base de datos y finalmente se pintan los cambios en la tabla.
     * 
     * Tag: <button>
     * Id: #btnAceptarCatProfesores
    * View: profesores_catalogoprofesores_modal.jsp
     * 
     */
        $("#btnAceptarCatProfesores").click(function () {

        var trn = $(this).attr('data-trCall');
        var tr = $("#tbodyGruposPJ tr[data-key=" + trn + "]");
        var tr2 = $('#tblCatProfesores tbody tr.selected');
        var key = tr2.attr("data-key");

            dataProf[trn].nombre_Profesor = lProfes[key].nombre_Prof;

        $.ajax({
            type: 'POST',
            url: "guardarProf.do",
            async: false,
                data: {
                paGrupoId: dataProf[trn].grupo_Id,
                paProfId: lProfes[key].pro_Id,
                paPeriodo: paPeriodo,
            paAño: paAño
            },
                success: function (data) {                 var paCodError = data.paCodError;                 var paDesError = data.paDesError;
                    if (Number(paCodError) === 0) {
                    tr.children().eq(9).text(dataProf[trn].nombre_Profesor);
                tr.children().eq(9).addClass("highligth");
                    } else {
                    tr.children().eq(9).addClass("error");
            bootbox.alert(paDesError);
        }

        }
        });

     lProfes = null;


    });
    /*
     * Objetivo: Guardar y Actualizar horario de profesor
     * 
     * Descripción:
     * 
     * Escucha un evento click en el elemento btnAceptarHorariosProf, cuando el 
     * boton se presiona se hace una petición al servidor para guardar el horario
     * del profesor correspondiente a la fila donde se llamo el modal.
     * 
     * Tag:  <button>
     * Id:   #btnAceptarHorariosProf
    * View: profesores_horarioprofesor_modal.jsp
     * 
     */
        $("#btnAceptarHorariosProf").click(function () { 
        var trCall = $(this).attr('data-trCall');
        var tr = $("#tbodyProfesoresA tr[data-key=" + trCall + "]");
        var paProfId = tr.children().eq(0).text();
        var paHorasAsign = tr.children().eq(2).text();         var horario = [[], [], [], [], [], []];
            $('#tblAsigHorProf td.update').each(function (index) {
            var dia = $(this).attr("data-day");
            var tr = $(this).closest("tr");
            var hora = tr.attr("data-hora");

        horario[dia].push(hora);
            });

        $.ajax({
            url: "horarios.do",
            type: 'POST',
            async: false,
                data: {
                paProfId: paProfId,
                paPeriodo: paPeriodo,
                paAño: paAño,
                lHorario: JSON.stringify(horario),
                paHorasAsign: paHorasAsign,
            opcion: 0
            },
                success: function (gen) {                 var paCodError = gen.paCodError;
                    if (Number(paCodError) !== 0) {
                bootbox.alert(gen.paDesError);
                    } else {
            tr.children().eq(0).addClass("update");
        }
        }
        });

    $("#divModalHorarioProf").modal("hide");
    });


     // ************************* EVENTOS CLICK ****************************


    /*
     * Objetivo: Desplegar Catalogo Profesores
     * 
     * Descripción:
     * 
     * Escucha un evento click en el icono que representa un elemento <a>,
     * cuando se produce el evento, se hace una petición al servidor para obtener
     * el catalogo de profesores de todas las academias (-100) y se hace otra 
     * petición para obtener la lista de las academias disponibles en la base 
     * de datos, con esta lista se llena el elemento selAcademia, una vez hecho esto
     * se despliega el modal divModalCatProf.
     * 
     * Tag:  <table>
     * Id:   #tblGrpJefDpto
     * View: profesores_view.jsp
    * 
     * 
     */
        $("#tblGrpJefDpto tbody").on("click", "td.searchP a", function (e) {
        e.preventDefault();

        var tr = $(this).closest("tr");
        var paAcademiaId = $("#bPeriodoNavbar").attr("data-academia");
            var paGrupoId = tr.children().eq(0).text();

        $.ajax({
            url: "catalogosGenericos.do",
            type: 'POST',
            async: false,
            dataType: 'json',
            data: {opcion: 1},
                success: function (acadGen) {
                var paCodError = acadGen.paCodError; 
                    if (Number(paCodError) === 0) {

                    var select = $("#selAcademia");
                    select.attr("data-gpoId", paGrupoId);
                    select.find('option').remove();

                        $.each(acadGen.retorno, function (key, value) {
                    $('<option>').val(value.id).text(value.nombre).appendTo(select);
                    });

                    $("#selAcademia").val(paAcademiaId);

                    requestGetProfesores(paAcademiaId, paGrupoId, paPeriodo, paAño);

                    $('#divModalCatProf').modal("show");
                        $('#dragModCatProfs').draggable({
                    handle: ".modal-header"
                    });

                $('#btnAceptarCatProfesores').attr('data-trCall', tr.attr("data-key"));
                    } else {

            bootbox.alert(acadGen.paDesError);
        }
    }

     });



    });
    /*
     * Objetivo: Desplegar tabla de horario de profesor
     * 
     * Descripción: 
     * 
     * Escucha un evento click en el icono que representa un elemento <a>, hace una
     * petición para obtener  el horario del profesor que llama al modal, se llena 
     * la tabla con el horario y posteriormente se muestra el modal divModalHorarioProf.
     * 
     * Tag:  <table>
     * Id:   #tblHorarioProf
    * View: profesores_view.jsp
     * 
     */
        $("#tblHorarioProf tbody").on("click", "td.horarioP a", function (e) {
        e.preventDefault();

        var tr = $(this).closest("tr");
        var paProfId = tr.children().eq(0).text();
        var hrs = tr.children().eq(2).text(); 
            if (hrs != "S.A") {
            $('#btnAceptarHorariosProf').attr('data-trCall', tr.attr("data-key"));
            $("#bNomProf").text("" + tr.children().eq(1).text());

                $.ajax({
                url: "horarios.do",
                type: 'POST',
                async: false,
                    data: {
                    paProfId: paProfId,
                    paPeriodo: paPeriodo,
                    paAño: paAño,
                opcion: 1
                },
                    success: function (data) {


                        $.each(data, function (dia, horario) {
                            $.each(horario, function (key, horas) {
                            var tr = $("#tblAsigHorProf tbody tr[data-hora=" + horas + "]");
                        tr.children().eq(dia + 1).addClass("update");
                    });
            });
            }
            });

            $("#divModalHorarioProf").modal("show");
        } else {
    bootbox.alert("Estimado usuario debe asignar total de horas para poder asignar un horario");
         }
    });

    /*
         * Objetivo: Desplegar el horario de profesor correspondiente al periodo anterior
     * 
         * Descripción: 
     * 
         * Escucha un evento click en el check de horario anterior, hace una
         * petición para obtener  el horario del profesor, se llena 
         * la tabla con el horario y posteriormente se refresca el modal.
     * 
         * Tag:  <table>
         * Id:   #tblHorarioProf
    * View: profesores_view.jsp
     * 
     */
        $('#HorAnterior').change(function () {
        if ($(this).is(":checked")) {

//            var key = $('#btnAceptarHorariosProf').attr('data-trCall');
//            var tr = $("#tbodyProfesoresA tr[data-key=" + key + "]");
//            var paProfId = tr.children().eq(0).text();
//
//            $.ajax({
//                url: "",
//                type: 'POST',
//                async: false,
//                data: {
//                    paProfId: paProfId,
//                    paPeriodo: paPeriodo,
//                    paAño: paAño,
//                    opcion: 1
//                },
//                success: function (data) {
//
//                    $('#tblAsigHorProf tbody tr td.update').removeClass('update');
//                    $.each(data, function (dia, horario) {
//                        $.each(horario, function (key, horas) {
//                            var tr = $("#tblAsigHorProf tbody tr[data-hora=" + horas + "]");
//                            tr.children().eq(dia + 1).addClass("update");
//                        });
//                    });
//                }
        //            });
            

        } else {
    $('#tblAsigHorProf tbody tr td.update').removeClass('update');            
    }

 });







    /*
 * Objetivo: Seleccionar profesor del catalogo de profesores
     * 
 * Descripción: 
     * 
 * Escucha un evento click en una fila de la tabla de catalogo de profesores, 
 * cuando el evento sucede, se agrega una clase selected a la fila, esta clase
 * sombre la fila indicando que ha sido seleccionada, en caso de que se vuelva
 * a dar click en la fila se elimina la clase para que la fila deje de estar
 * sombreada.
     * 
 * Tag:  <table>
 * Id:   #tblCatProfesores
    * View: profesores_catalogoprofesores_modal.jsp
     * 
     */
        $("#tblCatProfesores tbody").on('click', 'tr.noObsClass', function () {
     selectUnSelect('#tblCatProfesores', $(this));
    });
    /*
     * Objetivo: Editar Columna Horas Asignadas
     * 
     * Descripción: 
     * 
     * Escucha un evento click en las celdas de la columna H_Asig de la tabla tblHorarioProf
     * cuando el evento sucede, se agrega un elemento <input> a la celda para que se puedan
     * agregar datos, tambien se valida que estos datos solo puedan ser numeros enteros.
     * 
     * Tag:  <table>
     * Id:   #tblHorarioProf
    * View: profesores_view.jsp
     * 
     */
        $("#tblHorarioProf tbody").on('click', 'td.tdHoAsiClass', function () {
        var tr = $(this).closest("tr");
        var originalContent = $(this).text();

        $(this).removeClass("tdHoAsiClass");
        $(this).html("<center><input type='text' value='" + originalContent + "' style='width:30px;'/></center>");

        $(this).children().first().children().first().focus();
            $(this).children().first().children().first().keypress(function (evt) {
            evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;
                if (!(charCode === 8 || charCode === 46)) {
                    if (charCode < 48 || charCode > 57) {
            evt.preventDefault();
        }
        }
        });

            $(this).children().first().children().first().blur(function () {

            var paHorasAsign = $(this).val();
            var paProfId = tr.children().eq(0).text();
            var child = $(this).parent().parent();
                if (paHorasAsign.length > 0 && paHorasAsign !== originalContent) {
                child.addClass("tdHoAsiClass");
                child.text(paHorasAsign);
                    if (originalContent !== 'S.A') {
                        $.ajax({
                        url: "horarios.do",
                        type: 'POST',
                        async: false,
                            data: {
                            paProfId: paProfId,
                            paPeriodo: paPeriodo,
                            paAño: paAño,
                            paHorasAsign: paHorasAsign,
                        opcion: 2},
                        dataType: 'json',
                            success: function (data) {
                            var paCodError = data.paCodError;
                            var paDesError = data.paDesError;
                                if (Number(paCodError) === 0) {
                            tr.children().eq(2).addClass("update");
                                } else {
                                child.text(originalContent);
                                tr.children().eq(2).addClass("error");
                            bootbox.alert(paDesError);
                        }
                    }
                });
                    } else {
            tr.children().eq(2).addClass("highligth");
                }
                } else {
                child.addClass("tdHoAsiClass");
        child.text(originalContent);
    }

    });
    });



     // ************************ EVENTOS DE MODAL ***************************

    /*
     * Objetivo: Deseleccionar filas
     * 
     * Descripcion: 
     * 
     * Cuando el modal divModalCatProf se cierra se ejecuta este metodo, que
     * quita la clase que marca como seleccionada a una fila en la tabla
     * tblCatProfesores. Esto evita que cuando el modal se vuelva a llamar,
     * no exista ninguna fila seleccionada.
     * 
     * Tag:  <div>
     * Id:   #divModalCatProf
    * View: profesor_catalogoprofesores_modal.jsp
     * 
     */
        $('#divModalCatProf').on("hidden.bs.modal", function () {
     $('#tblCatProfesores tbody tr.selected').removeClass('selected');
    });

    /*
     * Objetivo: Limpiar tabla de horarios
     * 
     * Descripción:
     * 
     * Cuando el modal  divModalHorarioProf se cierra se eliminan las clases que
     * representan el horario del profesor, tambien se limpia el horario para que 
     * este pueda recibir nuevos datos y no se corrompa la información.
     * 
     * Tag:  <div>
     * Id:   #divModalHorarioProf
    * View: profesores_horarioprofesor_modal.jsp
     * 
     */
        $('#divModalHorarioProf').on("hidden.bs.modal", function () {
    $('#tblAsigHorProf tbody td.update').removeClass('update');
    });

        $("#aRefresh").click(function (e) {
        e.preventDefault();
    $("#selPeriodo").change();

    });

    $("#btnRepHor").click(function () {

        $.ajax({
            url: "profesores.do",
            type: 'POST',
            async: false,
            dataType: 'json',
            data:{opC:paOpcionCatalogo}
            ,success: function (bean) {

                if (Number(bean.paCodError) === 0) {
                    if ($.fn.DataTable.isDataTable('#tblRepCatAulas')) {
                        tblRepCatAulas.destroy();
                    }
                    var tbody = $("#tbodyRepAulas");

                    tbody.find("tr").remove();
                    $("#thID").text("Id");
                    $("#thDes").text("Profesor");
                    $.each(bean.retorno, function (key, object) {
                        var tr = $("<tr></tr>");
                        tbody.append(tr);
                        tr.attr("data-key", key);
                        tr.attr("data-id", object.id);                        
                        tr.addClass("genReport");
                        var td = $("<td>" + object.id + "</td>");
                        tr.append(td);

                        var td = $("<td>" + object.nombre + "</td>");
                        tr.append(td);


                    });

                    tblRepCatAulas = $("#tblRepCatAulas").DataTable({
                        "language": {
                            "paginate": {
                                "next": "Siguiente",
                                "previous": "Anterior"
                            },
                            "search": "Buscar: ",
                            "zeroRecords": "No se encontraron coincidencias",
                            "lengthMenu": "Mostrar _MENU_ grupos"
                        },
                        "info": false,
                        "destroy": true
                        , "lengthChange": false

                    });

                    $("#divRepCatAulas").modal("show");

                } else {
                    bootbox.alert(bean.paDesError);
                }

            }

        });




    });
    
     $("#tbodyRepAulas").on("click", "tr.genReport", function (e) {

        var id = $(this).attr("data-id");
        
        window.open('mapeohorario.pdf?op=1&id=' + id+"&opC="+ paOpcionCatalogo,'_blank');
    });  
    
    $("#btnTodosRep").click(function(e){
        window.open('mapeohorario.pdf?op=2&opC='+ paOpcionCatalogo, '_blank');
    });
    
      $("#btnRepJef").click(function (e){
       window.open('gruposatendidos.pdf'); 
    });
    
    $("#btnRepCarr").click(function(e){
        window.open('gruposjefedpto.pdf');
    });
    
    

    var table = $('#tblAsigHorProf'), isMouseDown, th;

        $(document).delegate('td.sel', 'mousedown', function () {
        var th = $(this);         isMouseDown = true;
        th = $(this);
        th.toggleClass("update").addClass('recent');
        $('td.sel').removeClass('marker');
        th.addClass('marker');
            return false;
    })
                .delegate('td.sel', 'mouseover', function () {                 var start_cell, start_y, start_x, end_y, end_x;
                    if (isMouseDown) {
                    th = $(this);
                        if (!th.hasClass('recent')) {
                    th.toggleClass("update").addClass('recent');
                    }
                    else
                        {
                    $('.recent').removeClass('recent').removeClass('update');
                    }
                    start_cell = table.find('td.marker')[0];



                    var start_y = $.inArray($(start_cell).parent()[0], table.find('tr'));
                    var end_y = $.inArray(th.parent()[0], table.find('tr'));


                    var start_x = $.inArray(start_cell, $(start_cell).parent().find('td'));
                    var end_x = $.inArray(this, th.parent().find('td'));


                        if (start_y > end_y) {
                        var temp = end_y;
                        end_y = start_y;
                    start_y = temp;
                    }

                        if (start_x > end_x) {
                        var temp = end_x;
                        end_x = start_x;
                    start_x = temp;
                    }


                        for (y = start_y; y <= end_y; y++) {
                            for (x = start_x; x <= end_x; x++) {
                                table.find('tr:eq(' + y + ')').find('td:eq(' + x + ')').each(function (i, o) {
                                var th = $(this);
                                    if (th.hasClass('sel')) {
                                    if (th.hasClass('recent')) {
                                        } else {
                                    th.toggleClass("update").addClass('recent');
                                }
                            }
                        });
                    }
            }
            }
            })
                .mouseup(function () {
                isMouseDown = false;
$('td.sel').removeClass('recent');
            });
});

 