/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * Inhabilita el boton regresar del buscador
 */
window.onload = function () {
    nobackbutton();
};

function nobackbutton() {
    window.location.hash = "no-back-button";
    window.location.hash = "Again-No-back-button"; //chrome
    window.onhashchange = function () {
        window.location.hash = "no-back-button";
    };
}


/*
 * Módulo: Grupos Preparacion
 * 
 * @param {number} key : representa la posicion de la fila en la tabla.
 * @param {char} type : indica si es una fila nueva o actualizable {0:Nuevo,1:Actualizable}
 * 
 * @returns {String}
 * 
 * Metodo tabla desplegable, retorna una tabla que sera agregada a una file de la tabla principal
 */
function formatPrepa(key, type, opCat) {


    var materia = dataPrep[key].nombre_Materia;
    var creditos = dataPrep[key].creditos;
    var academia = dataPrep[key].acad_Imparte_Mat;
    var tope_Alumno = dataPrep[key].tope_Alumno;
    var profesor = dataPrep[key].nombre_Prof;

    var table = '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">' +
            '<tr>' +
            '<td>Nombre Completo:</td>' +
            '<td>' + ((materia !== undefined) ? materia : '') + '</td>' +
            '</tr>' +
            '<tr>' +
            '<td>Creditos:</td>' +
            '<td>' + ((creditos !== undefined) ? creditos : '') + '</td>' +
            '</tr>' +
            '<tr>' +
            '<td>Academia:</td>' +
            '<td>' + ((academia !== undefined) ? academia : '') + '</td>' +
            '</tr>';

    if (opCat === "2" || opCat === "4") {

        var extraRows = '<tr>' +
                '<tr data-key="' + key + '" data-type="' + type + '">' +
                '<td>TopeAlumno:</td>' +
                '<td class="ediTope" >' + ((tope_Alumno !== undefined) ? tope_Alumno : '') + '</td>' +
                '</tr>' +
                '<tr>' +
                '<td>Profesor:</td>' +
                '<td>' + ((profesor !== undefined) ? profesor : '') + '</td>' +
                '</tr>';

        table = table + extraRows;


    }

    return table + '</table>';
}

/*
 * Módulo: Grupos Preparacion Compartidos
 * 
 * @param {number} key: representa la posicion de la fila en la tabla.
 * @param {char} type: indica si es una fila nueva o actualizable {0:Nuevo,1:Actualizable}
 * @returns {String}
 * 
 * Metodo tabla desplegable, retorna una tabla que sera agregada a una file de la tabla principal
 */
function formatCompart(key, type, opCat) {
    var creditos = dataPrepC[key].creditos;
    var academia = dataPrepC[key].acad_Imparte_Mat;
    var tope_Alumno = dataPrepC[key].tope_Alumno;
    var profesor = dataPrepC[key].nombre_Prof;

    var table = '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">' +
            '<tr>' +
            '<td>Creditos:</td>' +
            '<td>' + ((creditos !== undefined) ? creditos : '') + '</td>' +
            '</tr>' +
            '<tr>' +
            '<td>Academia:</td>' +
            '<td>' + ((academia !== undefined) ? academia : '') + '</td>' +
            '</tr>';


    if (opCat === "2" || opCat === "4") {//Actuales e Intersemestral
        var extraTr = '<tr>' +
                '<td>TopeAlumno:</td>' +
                '<td>' + ((tope_Alumno !== undefined) ? tope_Alumno : '') + '</td>' +
                '</tr>' +
                '<tr>' +
                '<td>Profesor:</td>' +
                '<td>' + ((profesor !== undefined) ? profesor : '') + '</td>' +
                '</tr>';
        table = table + extraTr;
    }

    return  table + '</table>';
}

/*
 * Módulo: Grupos Preparacion Compartidos
 * 
 * @param {number} key: representa la posicion de la fila en la tabla.
 * @returns {String}
 * 
 * Metodo tabla desplegable, retorna una tabla que sera agregada a una file de la tabla principal
 */
function formatCatGrup(key) {
    return '<table class="table table-striped table-bordered">' +
            '<tr>' +
            '<th>Lun</th>' +
            '<th>Mar</th>' +
            '<th>Mie</th>' +
            '<th>Jue</th>' +
            '<th>Vie</th>' +
            '<th>Sab</th>' +
            '</tr>' +
            '<tr>' +
            '<td>' + ((lGrupos[key].H_LUN !== undefined) ? lGrupos[key].H_LUN : '') + '</td>' +
            '<td>' + ((lGrupos[key].H_MAR !== undefined) ? lGrupos[key].H_MAR : '') + '</td>' +
            '<td>' + ((lGrupos[key].H_MIE !== undefined) ? lGrupos[key].H_MIE : '') + '</td>' +
            '<td>' + ((lGrupos[key].H_JUE !== undefined) ? lGrupos[key].H_JUE : '') + '</td>' +
            '<td>' + ((lGrupos[key].H_VIE !== undefined) ? lGrupos[key].H_VIE : '') + '</td>' +
            '<td>' + ((lGrupos[key].H_SAB !== undefined) ? lGrupos[key].H_SAB : '') + '</td>' + //SABADO
            '</tr>' +
            '</table>';
}


/*
 * @param {number} key: representa la posicion de la fila en la tabla.
 * @param {char} type: indica si es una fila nueva o actualizable {0:Nuevo,1:Actualizable}
 * @param {number} op: indica a que tabla corresponde concatenar los detalles
 * 
 * @returns {function}
 * 
 * Devuelve la función segun el numero asignado a op
 */
function tblDetalles(key, type, op, opCat) {
    switch (op) {
        case 1:
            return formatPrepa(key, type, opCat);
            break;
        case 2:
            return formatCompart(key, type, opCat);
            break;
        case 3:
            return formatCatGrup(key);
            break;

    }
}

/* 
 * @param {Object} dataTable:  Objeto dataTables al cual esta vinculado la fila a anexar los detalles
 * @param {number} op: indica a que tabla corresponde concatenar los detalles
 * @param {Object} tr: Elemento tr al cual se concatenar los detalles 
 * 
 * Concatena a la fila donde se lanzo el evento click td.details-control
 * la tabla donde de despliegan los detalles. 
 */
function adjuntarDetalles(dataTable, op, tr, opCat) {
    var key = tr.attr('data-key');
    var type = tr.attr('data-type');
    var row = dataTable.row(tr);
    if (row.child.isShown()) {
        row.child.hide();
        tr.removeClass('shown');
    } else {
        row.child(tblDetalles(
                ((key !== undefined) ? key : -1)
                , ((type !== undefined) ? type : -1)
                , op, opCat)).show();

        tr.addClass('shown');
    }
}

/*
 * @param {number} id: representa la posicion de la fila en la tabla.
 * @param {Object} tr: Elemento tr al cual se agrega la clase
 * 
 * Agrega una clase selected a el elemnto tr recibido, esta clase sombrea
 * la fila indicando que esta seleccionado.
 */
function selectUnSelect(id, tr) {
    if (tr.hasClass('selected')) {
        tr.removeClass('selected');
    } else {
        $('' + id + ' tbody tr.selected').removeClass('selected');
        tr.addClass('selected');
    }
}

function  validaCapacidad(dataGpo, tipo) {
    var resp = 0;
    $.ajax({
        url: "ValidaCapacidad.do",
        type: 'POST',
        async: false,
        data: {Grupo: JSON.stringify(dataGpo),
            tipo: tipo},
        success: function (data) {

            if (data != 0) {
                bootbox.alert(data);
                resp = -1;
            }

        }
    });
    return resp;
}

function eliminarGrupo(paGrupoId, paPeriodo, paAño, tblObj) {
    $.ajax({
        url: "eliminar.do",
        async: false,
        type: 'POST',
        data: {paGrupoId: paGrupoId
            , paPeriodo: paPeriodo
            , paAño: paAño
        },
        success: function (data) {
            if (Number(data) === 0) {
                tblObj.row('.selected').remove().draw(false);
            } else {
                bootbox.alert(data);
            }
        }
    });
}

function completeAjax(op) {

    switch (op) {
        case 1: //Grupos Preparacion
            tblGrpPreparacion = $('#tblGrpPreparacion').DataTable({//se inicializa un objeto DataTable y se almacena en tblGrpPreparacion
                "language": {
                    "paginate": {
                        "next": "Siguiente",
                        "previous": "Anterior"
                    },
                    "search": "Buscar: ",
                    "zeroRecords": "No se encontraron coincidencias",
                    "lengthMenu": "Mostrar _MENU_ grupos"
                    
                },
                "responsive":"true",
                "info": false,
                "destroy": true,
//                "lengthChange": false,
                "sDom": "lrtip"
            });

            var i = 0;//contador
            tblGrpPreparacion.columns().every(function () {//se asigna la busqueda por columna
                var that = this;

                $('#colSearch' + (++i)).on('keyup change', function () {
                    that
                            .search(this.value)
                            .draw();
                });
            });
            break;
        case 2: //Grupos Preparacion Compartidos
            tblGrpPrepCompar = $('#tblGrpPrepCompar').DataTable({//se inicializa un objeto DataTable
                "responsive":true,
                "language": {
                    "paginate": {
                        "next": "Siguiente",
                        "previous": "Anterior"
                    },
                    "search": "Buscar: ",
                    "zeroRecords": "No se encontraron coincidencias",
                    "lengthMenu": "Mostrar _MENU_ grupos"
                },
                "info": false,
                "destroy": true
//                ,"lengthChange": false
            });
            break;
        default:
            bootbox.alert("Ocurrio un error inesperado, comuniquese con su administrador: completeAjax");
            break;
    }

}

function createRows(op, data, tbody) {
    if (op === 1) {
        $.each(data, function (key, grupo) {//se hace un recorrido total del objeto JSON

            var tr = $("<tr></tr>");//Se construye un elemento <tr>
            tbody.append(tr);//se agrega a la tbody de la tabla

            tr.attr("data-key", key); //se agrega un atributo "data-key" contiene la posición en el arreglo dataPrep de los datos asignados al elemento <tr> 
            tr.attr("data-type", 1);//se agrega un atributo "data-type" con el valor de 1: data-type=1 indica que son datos que se pueden actualizar

            //Se llena la tabla con los datos contenidos en el objeto JSON                        
            var td = $("<td>" + grupo.bloque + "</td>");
            tr.append(td);
            td.attr("class", "bloqueGrupo");

            var td = $("<td>" + grupo.grupo_Id + "</td>");
            tr.append(td);
            td.attr("class", "no-editable");

            var td = $("<td>" + grupo.nom_Materia_Corto + "</td>");
            tr.append(td);
            td.attr("class", "no-editable");

            //Movimiento de capacidad a tabla principal
            var td = $("<td>" + grupo.capacidad + "</td>");
            tr.append(td);
            td.attr("class", "cell-edit");



            var td = $("<td data-day='1'>" + ((grupo.H_LUN !== undefined) ? grupo.H_LUN : "") + "</td>");
            tr.append(td);
            td.attr("class", "editable");

            var td = $("<td data-day='2'>" + ((grupo.H_MAR !== undefined) ? grupo.H_MAR : "") + "</td>");
            tr.append(td);
            td.attr("class", "editable");

            var td = $("<td data-day='3'>" + ((grupo.H_MIE !== undefined) ? grupo.H_MIE : "") + "</td>");
            tr.append(td);
            td.attr("class", "editable");

            var td = $("<td data-day='4'>" + ((grupo.H_JUE !== undefined) ? grupo.H_JUE : "") + "</td>");
            tr.append(td);
            td.attr("class", "editable");

            var td = $("<td data-day='5'>" + ((grupo.H_VIE !== undefined) ? grupo.H_VIE : "") + "</td>");
            tr.append(td);
            td.attr("class", "editable");

            var td = $("<td data-day='6'>" + ((grupo.H_SAB !== undefined) ? grupo.H_SAB : "") + "</td>");
            tr.append(td); //SABADO
            td.attr("class", "editable");
            //Movimiento de inscritos  
            checkInscritos(grupo, tr, op);
            var td = $("<td></td>");
            tr.append(td);
            td.attr("class", "details-control");



        });
    } else if (op === 2) {
        $.each(data, function (key, grupo) {//se hace un recorrido total del objeto JSON

            var tr = $("<tr></tr>");//Se construye un elemento <tr>
            tbody.append(tr);//se agrega a la tbody de la tabla

            tr.attr("data-key", key); //se agrega un atributo "data-key" contiene la posición en el arreglo dataPrep de los datos asignados al elemento <tr> 
            tr.attr("data-type", 1);//se agrega un atributo "data-type" con el valor de 1: data-type=1 indica que son datos que se pueden actualizar

            //Se llena la tabla con los datos contenidos en el objeto JSON                        
            var td = $("<td>" + grupo.bloque + "</td>");
            tr.append(td);
            td.attr("class", "bloqueGrupo");

            var td = $("<td>" + grupo.grupoId_Real + "</td>");
            tr.append(td);
            td.attr("class", "no-editable");

            var td = $("<td>" + grupo.nom_Materia_Corto + "</td>");
            tr.append(td);
            td.attr("class", "no-editable");

            var td = $("<td>" + grupo.grupo_Id + "</td>");
            tr.append(td);
            td.attr("class", "no-editable");

            //Movimiento de capacidad a tabla principal
            var td = $("<td>" + grupo.capacidad + "</td>");
            tr.append(td);
            td.attr("class", "cell-edit");


            var td = $("<td data-day='1'>" + ((grupo.H_LUN !== undefined) ? grupo.H_LUN : "") + "</td>");
            tr.append(td);
            td.attr("class", "no-editable");

            var td = $("<td data-day='2'>" + ((grupo.H_MAR !== undefined) ? grupo.H_MAR : "") + "</td>");
            tr.append(td);
            td.attr("class", "no-editable");

            var td = $("<td data-day='3'>" + ((grupo.H_MIE !== undefined) ? grupo.H_MIE : "") + "</td>");
            tr.append(td);
            td.attr("class", "no-editable");

            var td = $("<td data-day='4'>" + ((grupo.H_JUE !== undefined) ? grupo.H_JUE : "") + "</td>");
            tr.append(td);
            td.attr("class", "no-editable");

            var td = $("<td data-day='5'>" + ((grupo.H_VIE !== undefined) ? grupo.H_VIE : "") + "</td>");
            tr.append(td);
            td.attr("class", "no-editable");

            var td = $("<td data-day='6'>" + ((grupo.H_SAB !== undefined) ? grupo.H_SAB : "") + "</td>");
            tr.append(td);         //SABADO
            td.attr("class", "no-editable");


            var td = $("<td>" + grupo.compartido + "</td>");
            tr.append(td);
            td.attr("class", "no-editable");
            //Movimiento de inscritos 
            checkInscritos(grupo, tr, op);

            var td = $("<td></td>");
            tr.append(td);
            td.attr("class", "details-control");

        });
    }


}

function openTab(tbl, tblName, tbodyName, paOpcionCatalogo, op, $this) {

    if ($.fn.DataTable.isDataTable('#' + tblName)) {
        tbl.destroy();
    }
    //Parametros que seran enviados al servidor: paPlanId, paEspecId, paOpcionCatalogo
    //son los parametros que espera recibir el procedimiento que optiene el catalogo de grupos
    var paPlanId = $('option:selected', "#selPlanes").attr('value');
    var paEspecId = $('option:selected', "#selEspec").attr('value');

    if (paPlanId === undefined || paPlanId === '#') {//Se comprueba que se haya seleccionado un plan en #selPlanes
        bootbox.alert("Debe seleccionar un plan");
    } else if (paEspecId === undefined || paEspecId === '#') {
        bootbox.alert("Debe seleccionar una especialidad");
    } else {
        $.ajax({
            url: "catalogogrupos.do",
            type: 'POST',
            async: false,
            dataType: 'json', //el tipo de parametro que se esta recibiendo es una cadena JSON
            data: {
                paPlanId: paPlanId
                , paEspecId: paEspecId
                , paOpcionCatalogo: paOpcionCatalogo
                , opcion: op
            }, //envio de parametros
            success: function (data) {
                if (Number(data.paCodError) === 0) {
                    var tbody = $("#" + tbodyName);

                    tbody.find("tr").remove();//removemos todos los elementos <tr> de #tbodyGruposP

                    switch (op) {
                        case 1:
                            dataPrep = [];//limpiamos e inicializamos un array dataPrep el cual almacenara todo el objeto JSON
                            dataPrep = data.retorno;//asignamos el objeto JSON a el array dataPrep
                            createRows(1, data.retorno, tbody);
                            break;
                        case 2:
                            dataPrepC = [];//limpiamos e inicializamos un array dataPrep el cual almacenara todo el objeto JSON
                            dataPrepC = data.retorno;//asignamos el objeto JSON a el array dataPrep
                            createRows(2, data.retorno, tbody);
                            break;
                        default:
                            bootbox.alert("Ocurrio un error inesperado, comuniquese con su administrador: openTab");
                            break;
                    }

                    completeAjax(op);
                    $this.tab("show");//Mostramos el <div> de la pestaña 

                } else {

                    bootbox.alert(data.paDesError);

                }
            }
        });

    }
}

function unSetEditable(tr) {
    var children = tr.children("td.editando");
    children.addClass("editable");
    children.removeClass("editando");
    children.text("");

}

function actualizarGrupo(value, paOpcion, tbodyName) {
    var flag = -1;
    if (value !== undefined) {
        var grupo = (paOpcion === 1) ? dataPrep[value] : dataPrepC[value];
        $.ajax({
            url: "update.do",
            type: 'POST',
            async: false,
            data: {grupoMB: JSON.stringify(grupo)
                , paOpcion: paOpcion
            }, //Se envia un objeto como cadena JSON
            success: function (data) {

                var tr = $("#" + tbodyName + " tr[data-key=" + value + "]");

                if (Number(data) === 0) {
                    tr.children().eq(1).removeClass("error");
                    tr.children().eq(1).removeClass("highligth");
                    tr.children().eq(1).addClass("update");//Se pinta la segunda columna si fue correcto
                    if (paOpcion === 1) {
                        toUpdate = undefined;
                    } else {
                        toUpdateC = undefined;
                    }
                    flag = 0;
                } else {
                    flag = -1;
                    bootbox.alert(data);
                    tr.children().eq(1).removeClass("update");
                    tr.children().eq(1).removeClass("highligth");
                    tr.children().eq(1).toggleClass("error");//Se pinta de rojo el id si no fue correcto
                }
            }
        });


    }
    return flag;
}

function guardarGrupo(paPlanId, paEspecId, value, paOpcion, tbodyName) {
    var flag = -1;
    if (value !== undefined) {
        var grupo = (paOpcion === 1) ? dataPrep[value] : dataPrepC[value];
        $.ajax({
            url: "guardar.do",
            type: 'POST',
            async: false,
            data: {
                paPlanId: paPlanId
                , paEspId: paEspecId
                , grupoMB: JSON.stringify(grupo)
                , paOpcion: paOpcion
            }, //Se envian parametros
            success: function (data) {
                var tr = $("#" + tbodyName + " tr[data-key=" + value + "]");

                if (!isNaN(data)) {
                    flag = 0;
                    var i = (paOpcion === 1) ? 1 : 3;
                    tr.children().eq(1).removeClass("error");
                    tr.children().eq(i).addClass("highligth");//Se pinta la segunda columna si fue correcto
                    tr.children().eq(i).text("" + data);//Se asigna el nuevo Id del grupo que fue creado
                    tr.attr("data-type", 1);
                    if (paOpcion === 1) {
                        dataPrep[value].grupo_Id = data;
                        unSetEditable(tr);
                    } else {
                        dataPrepC[value].grupo_Id = data;
                    }
                } else {
                    bootbox.alert(data);
                    tr.children().eq(1).removeClass("error");
                    tr.children().eq(1).addClass("error");
                    flag = -1;
                    tr.attr("data-type", 0);
                }
            }
        });
    }
    return flag;
}
/*
 * Descripción: Permite editar los datos contenidos en una celda
 * 
 * @param {Object} tr : Elemento tr que identifica la fila donde se editara una celda
 * @param {String} clase : Clase css que se agregara para indicarle que puede o no ser editada
 * @param {Object} $this : Elemento td el cual será editado
 * @param {number} op : Identifica que valor será modificado
 * 
 */


function editarCelda(tr, clase, $this, op, update, paOpcion, tbody) {
    var type = tr.attr("data-type");
    var key = tr.attr("data-key");
    var originalContent = $this.text();
    var flag = 0;
    //alert("type::"+type+"key::"+key+"originalContent::"+originalContent+"update::"+update);        
    if (key !== update && type === '1' && update !== undefined) {
        flag = actualizarGrupo(update, paOpcion, tbody);
    }
    if (flag === 0) {
        $this.removeClass(clase);
        $this.html("<input type='text' maxlength='4' value='" + originalContent + "' style='width: 30px;' class='inputs' />");

        var child = $this.children();

        child.first().focus();
        child.first().keypress(function (evt) {
            validaNumeros(evt);
        });

        child.first().blur(function () {

            child.parent().addClass(clase);
            var newContent = $(this).val();
            if (newContent !== originalContent) {
                var flag = asignaNewContent(newContent, key, op);
                child.parent().text((flag === 0) ? newContent : originalContent);
                if (op === 2) {
                    dataPrep[key].capacidad = (flag === 0) ? newContent : ((originalContent === "") ? 0 : originalContent);

                } else {
                    if (op === 4) {
                        dataPrepC[key].capacidad = (flag === 0) ? newContent : ((originalContent === "") ? 0 : originalContent);
                    }
                }

                if (op < 3 || op === 6) {
                    var result = validaNoDuplicados(toUpdate, type, key);
                    toUpdate = (result !== undefined) ? result : toUpdate;
                } else if (op !== 6) {
                    var result = validaNoDuplicados(toUpdateC, type, key);//Modificar para compartidos 
                    toUpdateC = (result !== undefined) ? result : toUpdateC;
                }
            } else {
                child.parent().text(originalContent);
            }


        });
    }

}

/*
 * Descripción: Asigna el valor de newContent a un atributo de un objeto en la posicion key
 *  
 * @param {String} newContent : Nuevo contenido que se asignará al registro
 * @param {number} key : Identifica en que posición se debe hacer la modificación
 * @param {number} op : Especifica que valor será modificado
 *
 */
function asignaNewContent(newContent, key, op) {
    var flag = 0;
    switch (op) {
        case 1:
            dataPrep[key].bloque = newContent;
            break;
        case 2://real
            dataPrep[key].capacidad = newContent;
            flag = validaCapacidad(dataPrep[key], 0);
            break;
        case 3:
            dataPrepC[key].bloque = newContent;
            break;
        case 4://compartido
            dataPrepC[key].capacidad = newContent;
            flag = validaCapacidad(dataPrepC[key], 1);
            break;
        case 5:
            dataPrepC[key].grupo_Id = newContent;
            break;
        case 6:
            dataPrep[key].tope_Alumno = newContent;
            break;
        case 7:
            dataPrepC[key].tope_Alumno = newContent;
            break;
    }
    return flag;
}



/*
 * Descripción: Valida que la posicion del elemento que fue actualizado, 
 *              no se encuentre ya dentro del arreglo.
 * 
 * @param {Object} lista : Lista de posiciones donde se registro una modificación(Actualización)
 * @param {char} type : Identifica si es un registro nuevo o actualizable
 *                      (0): Nuevo
 *                      (1): Actualizable
 * @param {number} key : Identificador de la posicion del registro 
 *
 */
function validaNoDuplicados(toUpdate, type, key) {
    if (type === '1') {
        if (toUpdate !== key) {
            return key;
        } else {
            return undefined;
        }
    } else {
        return undefined;
    }
}
/*
 * Descripción: Detecta un evento de teclado y valida que el valor a ser ingresado sea numerico
 * 
 * @param {Object} e
 * 
 */
function validaNumeros(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (!(charCode === 8 || charCode === 46)) {
        if (charCode < 48 || charCode > 57) {
            evt.preventDefault();
        }
    }

}
/*
 * Descripción: Valida el periodo si es actuales o intersemestral agrega 
 * inscritos e inscritos totales
 * 
 * @param {Object} grupo
 * @param {Object} tr
 * 
 */

function checkInscritos(grupo, tr, op) {
    var opCat = $("#bPeriodoNavbar").attr("data-catalogo");

    if (opCat === "2" || opCat === "4") {
        var td = $("<td>" + grupo.inscritos + "</td>");
        tr.append(td);
        td.attr("class", "no-editable");
        var td = $("<td>" + grupo.insTotales + "</td>");
        tr.append(td);
        td.attr("class", "no-editable");
        if (op === 1) {
            var td = $("<td></td>");
            tr.append(td);
            td.attr("class", "gen-report");
        }

    }

}