/* 
 * Titulo: Handler para el modulo de Coordinacion: Grupos Preparación
 * 
 * Nombre: grupos_handler
 * 
 * Autor: Edgar Cirilo González
 * 
 * 
 */
var toSave;
var toUpdate;

function validaRango(inp) {
    if (Number(inp) > 6 && Number(inp) < 22) {
        return true;
    } else {
        return false;
    }
}
/*
 * Descripción: Metodo que crea los elementos cuando se da click en las columnas de horario
 *  
 * @param {type} $this
 * @param {type} clase
 * @returns {undefined}
 */
function createChilds($this, clase) {
    var tr = $this.closest('tr');
    var key = tr.attr("data-key");
    var type = tr.attr("data-type");
    var originalContent = $this.text();
    $this.attr("data-original", originalContent);
    var splitSlash;
    var splitGuion;
    var materia = tr.children().eq(2).text();
    var flag = 0;
    if (key !== toUpdate && type === '1' && toUpdate !== undefined) {
        flag = actualizarGrupo(toUpdate, 1, 'tbodyGruposP');
    }
    if (flag === 0) {
        $this.removeClass(clase);
        $this.addClass("editando");

        if (originalContent.length > 1) {
            splitSlash = originalContent.split("/");
        } else {
            splitSlash = ["", ""];
        }

        if (splitSlash[0].length > 1) {
            splitGuion = splitSlash[0].split("-");
        } else {
            splitGuion = ["", ""];
        }

        var inp = "<input class='horaC inputs'  type='text' maxlength='2' value='" + splitGuion[0] + "' style='width: 30px;'/>";
        var inp2 = "<input class='aulaC' type='text' maxlength='2' value='" + splitGuion[1] + "' style='width: 30px;'/>";

        $this.html(inp);
        $this.append(inp2);
        $this.append("<button class='go'><i class='fa fa-caret-square-o-right'></i></button>");

        if (materia.length > 0) {
            $this.append("<button class='cancel'><i class='fa fa-times'></i></button>");
        }

        var parent = $this;
        var child = $this.children();

        $this.children().first().focus();

        $this.children().first().keypress(function (evt) {

            validaNumeros(evt);

        });

        $this.children().eq(1).keypress(function (evt) {

            validaNumeros(evt);

        });

        $this.children().first().keydown(function (evt) {
            evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if ((charCode === 8 || charCode === 46)) {
                parent.children("input.aulaC").val(" ");
            }

        });

        $this.children("button.cancel").click(function () {
            child.parent().addClass(clase);
            child.parent().removeClass("editando");
            child.parent().text(originalContent);
        });

        $this.children("button.go").click(function () {

            var materia = tr.children().eq(2).text();
            var inp1 = child.eq(0).val().trim();
            var inp2 = child.eq(1).val().trim();
            var day = $this.attr("data-day");
            var td = tr.children("td[data-day =" + day + "]");
            if (materia.length > 0) {

                if (inp1.length > 0 && inp2.length > 0) {
                    if (validaRango(inp1) && validaRango(inp2)) {
                        var modUser = $("#bPeriodoNavbar").attr("data-modalidad");
                        if (Number(inp2) > Number(inp1)) {
                            if (modUser == 1) {//mientras no sea a distancia se llama catalogo 
                                day = Number(day);
                                var newContent = inp1.trim() + "-" + inp2.trim();
                                switch (day) {
                                    case 1:
                                        dataPrep[key].H_LUN = newContent;
                                        break;
                                    case 2:
                                        dataPrep[key].H_MAR = newContent;
                                        break;
                                    case 3:
                                        dataPrep[key].H_MIE = newContent;
                                        break;
                                    case 4:
                                        dataPrep[key].H_JUE = newContent;
                                        break;
                                    case 5:
                                        dataPrep[key].H_VIE = newContent;
                                        break;
                                    case 6:
                                        dataPrep[key].H_SAB = newContent;//SABADO
                                        break;
                                }
                                td.text(newContent);
                                td.removeClass("editando");
                                td.addClass("editable");

                                var result = validaNoDuplicados(toUpdate, type, key);
                                toUpdate = (result !== undefined) ? result : toUpdate;
                            } else {

                                $.ajax({
                                    url: "catalogogrupos.do",
                                    type: 'POST',
                                    async: false,
                                    dataType: 'json',
                                    data: {
                                        paOpcion: 2
                                        , paGrupoId: dataPrep[key].grupo_Id
                                        , paDia: day
                                        , paCapacidad: dataPrep[key].capacidad
                                        , paHor_Ini: inp1
                                        , paHor_Fin: inp2
                                        , opcion: 5},
                                    success: function (data) {

                                        $("#btnAceptarCatAulas").attr('data-trCall', key);
                                        $("#btnAceptarCatAulas").attr('data-dayCall', day);
                                        $("#btnAceptarCatAulas").attr('data-HorIni', inp1);
                                        $("#btnAceptarCatAulas").attr('data-HorFin', inp2);

                                        if (Number(data.paCodError) === 0) {
                                            if ($.fn.DataTable.isDataTable('#tblCatAulas')) {
                                                tblCatAulas.destroy();
                                            }
                                            var tbody = $("#tbodyAulas");

                                            tbody.find("tr").remove();

                                            $.each(data.retorno, function (key, object) {
                                                var tr = $("<tr></tr>");
                                                tbody.append(tr);
                                                tr.attr("data-key", key);
                                                tr.attr("data-aula", object.nombre);
                                                tr.addClass(object.trClass);

                                                var td = $("<td>" + object.nombre + "</td>");
                                                tr.append(td);

                                                var td = $("<td>" + object.edificio + "</td>");
                                                tr.append(td);

                                                var td = $("<td>" + object.capacidad + "</td>");
                                                tr.append(td);

                                                var td = $("<td>" + object.obs + "</td>");
                                                tr.append(td);
                                                td.addClass(object.idClass);

                                            });

                                            tblCatAulas = $('#tblCatAulas').DataTable({
                                                "language": {
                                                    "paginate": {
                                                        "next": "Siguiente",
                                                        "previous": "Anterior"
                                                    },
                                                    "search": "Buscar: ",
                                                    "zeroRecords": "No se encontraron coincidencias"
                                                },
                                                "responsive": true,
                                                "info": false,
                                                "destroy": true
                                                , "lengthChange": false
                                            });

                                            $('#divModalCatAulas').modal("show");
                                            $("#dragModCatAulas").draggable({
                                                handle: ".modal-header"
                                            });

                                        } else {
                                            bootbox.alert(data.paDesError);
                                        }
                                    }
                                });
                            }//dist
                        } else {
                            bootbox.alert("La hora final debe de ser mayor que la hora inicial");
                        }
                    } else {
                        bootbox.alert("Solo puede elegir un horario entre 7 - 21 hrs");
                    }
                } else {
                    //var td = $this.closest('td');
                    var day = $this.attr("data-day");
                    child.parent().addClass(clase);
                    child.parent().removeClass("editando");
                    child.parent().text("");

                    switch (day) {
                        case '1':
                            dataPrep[key].H_LUN = undefined;
                            break;
                        case '2':
                            dataPrep[key].H_MAR = undefined;
                            break;
                        case '3':
                            dataPrep[key].H_MIE = undefined;
                            break;
                        case '4':
                            dataPrep[key].H_JUE = undefined;
                            break;
                        case '5':
                            dataPrep[key].H_VIE = undefined;
                            break;
                        case '6':
                            dataPrep[key].H_SAB = undefined;//SABADO
                            break;
                    }

                    var result = validaNoDuplicados(toUpdate, type, key);
                    toUpdate = (result !== undefined) ? result : toUpdate;
                }
            } else {
                bootbox.alert("Necesita indicar una materia para seleccionar un aula ");
            }
        });
    }
}


/*
 * 
 * @param {type} param
 * 
 * READY metodo principal
 */
$(document).ready(function () {

    $('[data-toggle="tooltip"]').tooltip();

    var nuevo = 0;
    var periodo = $("#bPeriodoNavbar").attr("data-periodo");
    var año = $("#bPeriodoNavbar").attr("data-año");
    var opCat = $("#bPeriodoNavbar").attr("data-catalogo");
//    var modUser = $("#bPeriodoNavbar").attr("data-modalidad");

    /*
     * Objetivo: Abrir pestaña(tab) "Preparacion"
     * 
     * Descripción: 
     * 
     * Metodo que detecta un evento click en la pestaña de grupos preparacion, cuando
     * es detectado se llena la tabla segun el resultado de la peticion que realiza al servidor,
     * asi igual inicializa un objeto DataTables tblGrpPreparacion el cual servira 
     * para manejar la tabla dinamicamente.
     * 
     * Tag:  <ul>
     * Id:   #ulTabGrupos   
     * View: gruposGeneral_view.jsp
     * 
     */
    $('#ulTabGrupos a[href="#divTabPreparacion"]').click(function (e) {
        toSave = undefined;
        toUpdate = undefined;
        nuevo = 0;
        e.preventDefault();//Previene la accion natural de una elemento <a>        

        openTab(tblGrpPreparacion, 'tblGrpPreparacion', 'tbodyGruposP', opCat, 1, $(this));
    });

    $('#btnUpdate').click(function () {
        $('#ulTabGrupos a[href="#divTabPreparacion"]').click();
    });
    /*
     * SECCION DE BOTONES "GUARDAR", "AGREGAR" Y "ELIMINAR" EN ESE ORDEN
     */

    /*
     * Objetivo: Guardar y actualizar los datos de los grupos preparación
     * 
     * Descripción: 
     * Boton guardar grupos preparacion, el metodo escucha un evento click,
     * una vez que se detecta el evento se lanza un cuadro de dialogo de confirmacion
     * en caso de que la opcion elegida sea correcta,se realiza el recorrido al arreglo 
     * listaUpdate y listaNuevos que contienen los id'sde aquellos grupos que tuvieron 
     * modificaciones y grupos nuevos respectivamente.
     * 
     * Tag:  <button>
     * Id:   #btnGuardarP
     * View: gruposGeneral_view.jsp
     * 
     * 
     */
    $("#btnGuardarP").click(function () {
        var paPlanId = $('option:selected', "#selPlanes").attr('value');
        var paEspecId = $('option:selected', "#selEspec").attr('value');

        var td = $("#tbodyGruposP tr[data-type='1'] td.editando");


        if (td.length > 0 && modUser !== 1) { //actualizar para distancia
//            var inp1 = td.children().eq(0).val();
//            var inp2 = td.children().eq(1).val();
//            if (inp1.length > 0 && inp2.length > 0) {
            bootbox.alert("Un horario debe llevar un formato '11-13/Q-1', asegurese de asignar una horario y un aula");
        } else {
            actualizarGrupo(toUpdate, 1, 'tbodyGruposP');
        }






//       actualizarGrupo(toUpdate, 1, 'tbodyGruposP');

        var flag = guardarGrupo(paPlanId, paEspecId, toSave, 1, 'tbodyGruposP');
        if (flag >= 0) {
            toSave = undefined;
            nuevo = 0;
        }
//        if (flag === 0) {
//            if ($.fn.DataTable.isDataTable('#tblGrpPreparacion')) {
//                tblGrpPreparacion.destroy
//                completeAjax(1);
//            }
//        }
    });

    /*
     * Objetivo: Agregar una fila vacia a la tabla y manejar los datos
     * 
     * Descripción: 
     * 
     * Boton agregar grupos preparacion, escucha un evento click en el boton,
     * cuando se genera el evento se agrega un elemento <tr> a la tabla, y se 
     * inicializa el componente de busqueda mediante la clase searchM,el cual
     * desplegara una ventana emergente donde se muestra el catalogo de materias.
     * 
     * Tag:  <button>
     * Id:   #btnAgregarGrupoP
     * View: gruposGeneral_view.jsp
     * 
     * 
     */
    var lMaterias;
    $('#btnAgregarGrupoP').on('click', function () {
        var paPlanId = $('option:selected', "#selPlanes").attr('value');
        var paEspecId = $('option:selected', "#selEspec").attr('value');
        var flag;

        if (nuevo === 1) {
            flag = guardarGrupo(paPlanId, paEspecId, toSave, 1, 'tbodyGruposP');
            if (flag === 0) {
                toSave = undefined;
//                if ($.fn.DataTable.isDataTable('#tblGrpPreparacion')) {
//                    tblGrpPreparacion.destroy();
//                    completeAjax(1);
//                }
            }
        } else if (nuevo === 0) {
            nuevo = 1;
            flag = 1;
        }

        if (flag >= 0) {
            var tr = $("<tr></tr>");
            tr.attr("data-type", '0');
            var td1 = $("<td></td>");
            tr.append(td1);
            td1.addClass("bloqueGrupo");
            var td2 = $("<td><a href='#'><i class='fa fa-search'></i></a></td>");
            tr.append(td2);
            td2.addClass("searchM");
            var td3 = $("<td></td>");
            tr.append(td3);
            td3.addClass("no-editable");
            var td10 = $("<td></td>");
            tr.append(td10);
            td10.addClass("cell-edit");

            var td4 = $("<td></td>");
            tr.append(td4);
            td4.addClass("editable");
            td4.attr("data-day", '1');
            var td5 = $("<td></td>");
            tr.append(td5);
            td5.addClass("editable");
            td5.attr("data-day", '2');
            var td6 = $("<td></td>");
            tr.append(td6);
            td6.addClass("editable");
            td6.attr("data-day", '3');
            var td7 = $("<td></td>");
            tr.append(td7);
            td7.addClass("editable");
            td7.attr("data-day", '4');
            var td8 = $("<td></td>");
            tr.append(td8);
            td8.addClass("editable");
            td8.attr("data-day", '5');
            var td11 = $("<td></td>");//SABADO
            tr.append(td11);
            td11.addClass("editable");
            td11.attr("data-day", '6');

            if (opCat === "2" || opCat === "4") {
                var td12 = $("<td></td>");//inscritos
                tr.append(td12);
                var td13 = $("<td></td>");//inscritos totales
                tr.append(td13);
            }
            if (opCat ==="2" || opCat === "4") {
                var td14 = $("<td></td>");
                tr.append(td14);
                td14.addClass("gen-report");
            }
            var td9 = $("<td></td>");
            tr.append(td9);
            td9.addClass("details-control");

            tr.attr('data-key', (dataPrep.length));//se le asigna un id, este sera la longitud del arreglo dataPrep

            var nuevoGrupo = {
                periodo: periodo,
                año: año,
                bloque: 0,
                grupo_Id: 0,
                nombre_Materia: "",
                nom_Materia_Corto: "",
                creditos: 0,
                capacidad: 0,
                inscritos: 0,
                inscTot: 0,
                tope_Alumno: 0,
                acad_Imparte_Mat: 0,
                nombre_Prof: "",
                H_LUN: "",
                H_MAR: "",
                H_MIE: "",
                H_JUE: "",
                H_VIE: ""
                , H_SAB: ""//SABADO
            };

            dataPrep.push(nuevoGrupo);//se agrega un elemento vacio a dataPrep

            var idNewTr = tr.attr('data-key');

            tblGrpPreparacion.row.add(tr).draw(false);

            td4.click();
            td5.click();
            td6.click();
            td7.click();
            td8.click();
            td11.click(); //SABADO
            tr.on("click", "td.searchM", function (e) {//agregamos un elemento click al boton buscar en la columna grp

                e.preventDefault();

                //Parametros que seran enviados al servidor: paPlanId, paEspecId
                //son los parametros que espera recibir el procedimiento que optiene 
                //el catalogo de grupos
                var paPlanId = $('option:selected', "#selPlanes").attr('value');
                var paEspecId = $('option:selected', "#selEspec").attr('value');

                $.ajax({//Peticion para obtener catalogo de materias
                    url: "catalogomaterias.do",
                    type: 'POST',
                    async: false,
                    dataType: 'json',
                    data: {
                        paPlanId: paPlanId
                        , paEspeciaId: paEspecId
                    },
                    success: function (result) {
                        if ($.fn.DataTable.isDataTable('#tblCatMaterias')) {
                            tblCatMaterias.destroy();
                        }
                        var tbody = $("#tbodyMaterias");

                        tbody.find("tr").remove();

                        if (Number(result.paCodError) === 0) {

                            $('#btnAceptarCatMat').attr('data-trCall', idNewTr);

                            lMaterias = result.retorno;

                            $.each(result.retorno, function (key, object) {

                                var tr = $("<tr></tr>");
                                tbody.append(tr);
                                tr.attr("data-key", key);
                                tr.addClass(object.trClass);

                                var td = $("<td>" + object.materia_Id + "</td>");
                                tr.append(td);

                                var td = $("<td>" + object.nombre_Materia + "</td>");
                                tr.append(td);

                                var td = $("<td>" + object.creditos_Materia + "</td>");
                                tr.append(td);

                                var td = $("<td>" + object.academia_Id + "</td>");
                                tr.append(td);
                            });

                            tblCatMaterias = $('#tblCatMaterias').DataTable({
                                "language": {
                                    "paginate": {
                                        "next": "Siguiente",
                                        "previous": "Anterior"
                                    },
                                    "search": "Buscar: ",
                                    "zeroRecords": "No se encontraron coincidencias"
                                },
                                "responsive": true,
                                "info": false,
                                "destroy": true
                                , "lengthChange": false
                            });
                            $('#btnAceptarCatMat').css("display", "block");
                            $('#btnAceptarEq').css("display", "none");
                            $('#divModalCatMaterias').modal("show");
                        } else {

                            bootbox.alert(result.paDesError);

                        }

                    }
                });

            });
        } else if (flag === -1) {
            nuevo = 1;
        }
    });


    /*
     * Objetivo: Eliminar grupo de la tabla grupos
     * 
     * Descripcion:
     * 
     * Escucha un envento click, cuando lo detecta, lanza un mensaje de confirmacion
     * en caso de que se confirme la eliminacion, hace una peticion al servidor,
     * si la eliminacion fue correcta elimina la fila que se selccionó.
     * 
     * Tag:  <button>
     * Id:   #btnElimarP
     * View: gruposGeneral_view.jsp
     *  
     */
    $('#btnElimarP').click(function () {
        var tr = $("#tbodyGruposP tr.selected");
        var index = tr.attr("data-key");
        var type = tr.attr("data-type");
        if (index !== undefined) {

            bootbox.confirm("¿Esta seguro de eliminar este grupo?", function (result) {
                if (result) {


                    if (type === '0') {
                        //Type 0 es igual a nuevo                    
                        tblGrpPreparacion.row('.selected').remove().draw(false);
                        nuevo = 0;
                        toSave = undefined;
                    } else if (type === '1') {

                        var paGrupoId = dataPrep[index].grupo_Id;
                        var paPeriodo = dataPrep[index].periodo;
                        var paAño = dataPrep[index].año;

                        eliminarGrupo(paGrupoId, paPeriodo, paAño, tblGrpPreparacion);
                    }
                }
            });
        }

    });

    /*
     * Objetivo: Obtener materia seleccionada y asignarla al grupo
     * 
     * Descripcion: Escucha un evento click del boton aceptar del modal catalogo materias
     * el cual asigna la materia seleccionada en la tabla de catalogo materias a la tabla de
     * grupos preparacion.
     * 
     * Tag:  <button>
     * Id:   #btnAceptarCatMat
     * View: grupos_catalogosmaterias_modal.jsp
     * 
     */
    $("#btnAceptarCatMat").click(function () {

        var tr2 = $('#tblCatMaterias tbody tr.selected');
        var key = tr2.attr("data-key");
        if (key !== undefined) {
            var trn = $(this).attr('data-trCall');
            var tr = $("#tbodyGruposP tr[data-key=" + trn + "]");

            dataPrep[trn].grupo_Id = lMaterias[key].materia_Id;
            dataPrep[trn].nombre_Materia = lMaterias[key].nombre_Materia;
            dataPrep[trn].nom_Materia_Corto = lMaterias[key].nombre_Corto;
            dataPrep[trn].creditos = lMaterias[key].creditos_Materia;
            dataPrep[trn].acad_Imparte_Mat = lMaterias[key].academia_Id;

            lMaterias = null;

            toSave = tr.attr('data-key');
            tr.children().eq(1).addClass("no-editable");
            tr.children().eq(1).text(dataPrep[trn].grupo_Id);
            tr.children().eq(2).text(dataPrep[trn].nom_Materia_Corto);
            tr.children("td.searchM").removeClass("searchM");
            $('#divModalCatMaterias').modal("hide");
        } else {
            bootbox.alert("Debe seleccionar una materia");
        }


    });

    /*
     * Sección de eventos JQuery con clases y tags
     *
     */

    /*
     * Objetivo: Quitar todos los elementos seleccionados con la clase selected
     * 
     * Descripción:
     * 
     * Cuando el modal #divModalCatMaterias se cierra, se quita la clase selected
     * de el elemento <tr> que haya sido seleccionado.
     * 
     * Tag:  <div>
     * Id:   #divModalCatMaterias
     * View: grupos_catalogomaterias_modal.jsp
     * 
     * 
     */
    $('#divModalCatMaterias').on("hidden.bs.modal", function () {
        $('#tblCatMaterias tbody tr.selected').removeClass('selected');
    });

    /*
     * Objetivo: Agregar o quitar clase selected cuando un <tr> es seleccionado
     * 
     * Descripción: 
     * 
     * Se agrega la clase selected a un elemento <tr> cuando este es seleccionado,
     * la clase sombrea la fila indicando que esta seleccionada, solo se puede 
     * seleccionar un elemento <tr> a la vez.
     * 
     * Tag:  <table>
     * Id:   #tblCatMaterias
     * View: grupos_catalogomaterias_modal.jsp
     * 
     */
    $("#tblCatMaterias tbody").on('click', 'tr.isSelectable', function () {
        selectUnSelect('#tblCatMaterias', $(this));
    });

    /*
     * Objetivo: Agregar o quitar clase selected cuando un <tr> es seleccionado
     * 
     * Descripción: 
     * 
     * Se agrega la clase selected a un elemento <tr> cuando este es seleccionado,
     * la clase sombrea la fila indicando que esta seleccionada, solo se puede 
     * seleccionar un elemento <tr> a la vez.
     * 
     * Tag:  <table>
     * Id:   #tblCatMaterias
     * View: grupos_catalogoaulas_modal.jsp
     * 
     */
    $('#tblCatAulas tbody').on('click', 'tr.noObsClass', function () {
        selectUnSelect('#tblCatAulas', $(this));
    });

    /*
     * 
     * Objetivo: Editar la capacidad de un grupo
     *
     * Descripción: 
     * Este metodo escucha un evento click
     * y habilita la edicion del campo capacidad
     *  
     * Tag:  <table>
     * Id:   #tblGrpPreparacion
     * View: gruposGeneral_view.jsp
     * 
     */
    $('#tblGrpPreparacion tbody').on('click', 'td.cell-edit', function () {
        var tr = $(this).closest('tr');
        editarCelda(tr, 'cell-edit', $(this), 2, toUpdate, 1, "tbodyGruposP");
    });

    $('#tblGrpPreparacion tbody').on('click', 'td.ediTope', function () {
        var tr = $(this).closest('tr');
        editarCelda(tr, 'ediTope', $(this), 6, toUpdate, 1, "tbodyGruposP");
    });

    /*
     * Objetivo: Editar los horarios de un grupo
     * 
     * Descripcion:     
     * 
     * Este metodo escucha un evento lik en las celdas horarios 
     * marcadas por la clase "editable".
     * 
     * Tag:  <table>
     * Id:   #tblGrpPreparacion
     * View: gruposGeneral_view.jsp
     *
     */
    $('#tblGrpPreparacion tbody').on('click', 'td.editable', function () {
        var buttonCancel = $("#tbodyGruposP tr td.editando button.cancel");
        buttonCancel.click();
        createChilds($(this), "editable");
    });


    /*
     * Objetivo: Edita bloque del grupo
     * 
     * Descripción: 
     * 
     * Espera un evento click en la celdas de la columna bloque de la tabla
     * tblGrpPreparacion, cuando sucede el evento, se agrega un elemento <input>
     * para poder agregar o editar datos, se valida que los datos ingresados sean
     * unicamente numericos.
     * 
     * Tag:  <table>
     * Id:   #tblGrpPreparacion
     * View: gruposGeneral_view.jsp
     * 
     */
    $('#tblGrpPreparacion tbody').on('click', 'td.bloqueGrupo', function () {
        var tr = $(this).closest('tr');
        editarCelda(tr, 'bloqueGrupo', $(this), 1, toUpdate, 1, "tbodyGruposP");
    });

    /*
     * Objetivo: Desplegar y Replegar tabla de detalles
     * 
     * Descripción: 
     * 
     * Escucha un evento click en el icono de detalles de la tabla ubicado en la 
     * ultima columna, cuando sucede el click concate una tabla a la fila donde 
     * se muestran los datos restantes del grupo. Si ya esta deplegada y sucede
     * el click se repliega la tabla de detalles.
     * 
     * Tag:  <table>
     * Id:   #tblGrpPreparacion
     * View: gruposGeneral_view.jsp
     * 
     */
    $('#tblGrpPreparacion tbody').on('click', 'td.details-control', function () {
        var tr = $(this).closest('tr');
        adjuntarDetalles(tblGrpPreparacion, 1, tr, opCat);
    });

    /*
     * Objetivo: Agregar o quitar clase selected cuando un <tr> es seleccionado
     * 
     * Descripción: 
     * 
     * Se agrega la clase selected a un elemento <tr> cuando este es seleccionado,
     * la clase sombrea la fila indicando que esta seleccionada, solo se puede 
     * seleccionar un elemento <tr> a la vez.
     * 
     * Tag:  <table>
     * Id:   #tblGrpPreparacion
     * View: gruposGeneral_view.jsp
     * 
     */
    $('#tblGrpPreparacion tbody').on('click', 'td.no-editable', function () {
        var tr = $(this).closest('tr');
        selectUnSelect('#tblGrpPreparacion', tr);
    });
    /*
     *  $("#tblCatAulas tbody").on('click', 'tr.noObsClass', function () {
     selectUnSelect('#tblCatAulas', $(this));
     }); 
     */


    $("#btnAceptarCatAulas").click(function () {

        var aula = $("#tblCatAulas tbody tr.selected").attr("data-aula");
        var key = $(this).attr('data-trCall');
        var tr = $("#tbodyGruposP tr[data-key=" + key + "]");
        var type = tr.attr("data-type");
        var day = $(this).attr('data-dayCall');
        var horIni = $(this).attr('data-HorIni');
        var horFin = $(this).attr('data-HorFin');
        var td = tr.children("td[data-day =" + day + "]");
        var originalContent = td.attr("data-original");
        day = Number(day);

        if (aula !== undefined) {

            var newContent = horIni.trim() + "-" + horFin.trim() + "/" + aula;

            switch (day) {
                case 1:
                    dataPrep[key].H_LUN = newContent;
                    break;
                case 2:
                    dataPrep[key].H_MAR = newContent;
                    break;
                case 3:
                    dataPrep[key].H_MIE = newContent;
                    break;
                case 4:
                    dataPrep[key].H_JUE = newContent;
                    break;
                case 5:
                    dataPrep[key].H_VIE = newContent;
                    break;
                case 6:
                    dataPrep[key].H_SAB = newContent;//SABADO
                    break;
            }

            td.text(newContent);
            td.removeClass("editando");
            td.addClass("editable");

            var result = validaNoDuplicados(toUpdate, type, key);
            toUpdate = (result !== undefined) ? result : toUpdate;

        } else {

            td.addClass("editable");
            td.removeClass("editando");
            td.text(originalContent);

        }

        $('#divModalCatAulas').modal("hide");

    });

    $('#divModalCatAulas').on("hidden.bs.modal", function () {
        $('#tblCatAulas tbody tr.selected').removeClass('selected');
    });

    $("#btnRepHor").click(function () {

        $.ajax({
            url: "aulas.do",
            type: 'POST',
            async: false,
            dataType: 'json',
            success: function (bean) {

                if (Number(bean.paCodError) === 0) {
                    if ($.fn.DataTable.isDataTable('#tblRepCatAulas')) {
                        tblRepCatAulas.destroy();
                    }
                    var tbody = $("#tbodyRepAulas");

                    tbody.find("tr").remove();
                    $("#thID").text("Aula");
                    $("#thDes").text("Edificio");
                    $.each(bean.retorno, function (key, object) {
                        var tr = $("<tr></tr>");
                        tbody.append(tr);
                        tr.attr("data-key", key);
                        tr.attr("data-aula", object.nombre);
                        tr.addClass("genReport");
                        var td = $("<td>" + object.nombre + "</td>");
                        tr.append(td);

                        var td = $("<td>" + object.edificio + "</td>");
                        tr.append(td);


                    });

                    tblRepCatAulas = $("#tblRepCatAulas").DataTable({
                        "language": {
                            "paginate": {
                                "next": "Siguiente",
                                "previous": "Anterior"
                            },
                            "search": "Buscar: ",
                            "zeroRecords": "No se encontraron coincidencias",
                            "lengthMenu": "Mostrar _MENU_ grupos"
                        },
                        "responsive": "true",
                        "info": false,
                        "destroy": true
                        , "lengthChange": false

                    });

                    $("#divRepCatAulas").modal("show");

                } else {
                    bootbox.alert(bean.paDesError);
                }

            }

        });




    });

    $("#tbodyRepAulas").on("click", "tr.genReport", function (e) {

        var aula = $(this).attr("data-aula");
        window.open('mapeohorario.pdf?op=0&aula=' + aula, '_blank');
    });

    $("#btnTodosRep").click(function (e) {
        window.open('mapeohorario.pdf?op=3', '_blank');
    });

    $('#tblGrpPreparacion tbody').on('click', 'td.gen-report', function () {
        var tr = $(this).closest('tr');
        var gpoId = tr.children('td').eq(1).text();
        window.open('listainscritos.pdf?grupoid=' + gpoId + "&oplis=2", '_blank');

    });

    $("#btnLiIns").click(function () {
        $("#modalGrupoEspecifico").modal("show");
    });

    $("#inpIdGrupo").keypress(function (evt) {
        validaNumeros(evt);
    });

    $("#btnGenRepInscritos").click(function () {
        var grupoid = $("#inpIdGrupo").val();
        if (grupoid === '')
        {
            bootbox.alert("Ingrese un número de grupo");
        } else {
            window.open('listainscritos.pdf?grupoid=' + grupoid + '&oplis=1', '_blank');
        }

    });

});




