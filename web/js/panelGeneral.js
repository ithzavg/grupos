/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function reloadActiveTab() {
    var tab = $("ul#ulTabGrupos li.active");
    var des = tab.attr("data-des");
    if (des === '2') {
        if ($.fn.DataTable.isDataTable('#tblGrpPreparacion')) {
            tblGrpPreparacion.destroy();
            $('#ulTabGrupos a[href="#divTabPreparacion"]').click();
        }
    } else if (des === '3') {

        if ($.fn.DataTable.isDataTable('#tblGrpPrepCompar')) {
            tblGrpPrepCompar.destroy();
            $('#ulTabGrupos a[href="#divTabCompPrep"]').click();
        }
    }

}



$(document).ready(function () {
    /*
     * Metodo que cambia la clave en el div con id #divEspecId segun el id
     * que tenga la especialidad seleccionada en el select que contienen 
     * todas las especialidades
     */

    $("#selEspec").change(function () {
        var option = $('option:selected', this).attr('value');
        $("#divEspecId").html(option);
        reloadActiveTab();
    });

    /*
     * Funcion que obtiene una cadena JSON de las especialidades a traves de una peticion POST a
     * modulogrupos.do segun la opcion que haya sido seleccionada.
     */
    $("#selPlanes").change(function () {
        var option = $('option:selected', this).attr('value');
        var $select = $('#selEspec');
        if (option === '#') {
            $select.find('option').remove();
            $('<option>').val('#').text('No se ha seleccionado plan').appendTo($select);
        } else {
            $.ajax({
                url: "modulogrupos.do",
                type: 'POST',
                async: false,
                dataType: 'json',
                data: {paPlanId: option, opcion: 1},
                success: function (especGen) {
                    var paCodError = especGen.paCodError;
                    if (Number(paCodError) === 0) {
                        $select.find('option').remove();
                        $.each(especGen.retorno, function (key, value) {
                            $('<option>').val(value.esp_id).text(value.nombre_espec).appendTo($select);
                        });

                        $('#selEspec').change();
                    }else{
                        bootbox.alert(especGen.paDesError);
                    }

                }
            });
        }
    });

});
