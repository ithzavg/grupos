/* 
 * Titulo: Handler para el modulo de Cordinacion: Grupos Preparación Compartidos
 * 
 * Nombre: gruposCompartidos_handler
 * 
 * Autor: Edgar Cirilo González
 * 
 * 
 */
var toSaveC;
var toUpdateC;


$(document).ready(function() {
    
    $('[data-toggle="tooltip"]').tooltip();

    var opCat = $("#bPeriodoNavbar").attr("data-catalogo");
    lGrupos = []; //lista del catalogo de grupos por carrera
    var nuevo = 0;
    /*
     * Objetivo: Desplegar catalogo de grupos según la carrera que se selecciona
     * 
     * Descripción: 
     * 
     * Metodo que detecta un cambio de seleccion en #selCarrera: cuando esto sucede
     * hace una peticion al servidor y obtiene el catalogo de grupos según la carrera
     * que fue seleccionada
     * 
     * Tag:  <select>
     * Id:   #selCarrera   
     * View: grupos_catmateriascompart_modal.jsp
     * 
     */
    $("#selCarrera").change(function() {

        if ($.fn.DataTable.isDataTable('#tblCatGrupos')) {
            tblCatGrupos.destroy();
        }
        var paCarreraId = $('option:selected', "#selCarrera").attr('value');

        $.ajax({//Peticion para obtener catalogo de materias
            url: "catalogogrupos.do",
            type: 'POST',
            async: false,
            dataType: 'json',
            data: {
                paCarreraId: paCarreraId
                , opcion: 4
            },
            success: function(result) {

                if (Number(result.paCodError) === 0) {

                    lGrupos = result.retorno;
                    var tbody = $("#tbodyGruposCat");

                    tbody.find("tr").remove();


                    $.each(result.retorno, function(key, object) {

                        var tr = $("<tr data-type='1'></tr>");
                        tbody.append(tr);
                        tr.attr("data-key", key);

                        var td = $("<td>" + object.grupo_Id + "</td>");
                        tr.append(td);

                        var td = $("<td>" + object.nombre_Materia + "</td>");
                        tr.append(td);

                        var td = $("<td>" + object.capacidad + "</td>");
                        tr.append(td);

                        var td = $("<td>" + object.inscritos + "</td>");
                        tr.append(td);

                        var td = $("<td>" + object.plan_Id + "</td>");
                        tr.append(td);

                        var td = $("<td>" + object.esp_Id + "</td>");
                        tr.append(td);

                        var td = $("<td>" + object.nombre_Prof + "</td>");
                        tr.append(td);

                        var td = $("<td></td>");
                        tr.append(td);
                        td.attr("class", "details-control");

                    });

                    tblCatGrupos = $('#tblCatGrupos').DataTable({
                        "language": {
                            "paginate": {
                                "next": "Siguiente",
                                "previous": "Anterior"
                            },
                            "search": "Buscar: ",
                            "zeroRecords": "No se encontraron coincidencias"
                        },
                        "responsive":true,
                        "info": false,
                        "destroy": true,
                        "lengthChange": false
                    });

                } else {
                    bootbox.alert(result.paDesError);
                }
            }
        });
    });

    /*
     * Objetivo: Abrir pestaña(tab) "Compartidos"
     * 
     * Descripción: 
     * 
     * Metodo que detecta un evento click en la pestaña de grupos compartidos, cuando
     * es detectado se llena la tabla segun el resultado de la peticion que realiza al servidor,
     * asi igual inicializa un objeto DataTables tblGrpPrepCompar el cual servirá 
     * para manejar la tabla dinamicamente.
     * 
     * Tag:  <ul>
     * Id:   #ulTabGrupos   
     * View: gruposGeneral_view.jsp
     * 
     */
    $('#ulTabGrupos a[href="#divTabCompPrep"]').click(function(e) {
        e.preventDefault();
        toSaveC = undefined;
        toUpdateC = undefined;
        nuevo = 0;

        openTab(tblGrpPrepCompar, 'tblGrpPrepCompar', 'tbodyGruposPC', (opCat - 1), 2, $(this));
    });

    $('#btnUpdateC').click(function() {
        $('#ulTabGrupos a[href="#divTabCompPrep"]').click();
    });
    /*
     * Objetivo: Guardar y actualizar los datos de los grupos preparación compartidos
     * 
     * Descripción: 
     * Metodo que escucha un evento click,una vez que se detecta el evento se lanza 
     * un cuadro de dialogo de confirmacion en caso de que la opcion elegida sea correcta,
     * se realiza el recorrido al arreglo listaUpdateC y listaNuevosC que contienen los 
     * id'sde aquellos grupos que tuvieron modificaciones y grupos nuevos respectivamente.
     * 
     * Tag:  <button>
     * Id:   #btnGuardarC
     * View: gruposGeneral_view.jsp
     * 
     * 
     */
    $("#btnGuardarC").click(function() {
//        bootbox.confirm("¿Esta seguro de guardar los cambios?", function(result) {
//            if (result) {

        var paPlanId = $('option:selected', "#selPlanes").attr('value');
        var paEspecId = $('option:selected', "#selEspec").attr('value');

        actualizarGrupo(toUpdateC, 2, 'tbodyGruposPC');

        var flag = guardarGrupo(paPlanId, paEspecId, toSaveC, 2, 'tbodyGruposPC');
        if (flag >= 0) {
            toSaveC = undefined;
            nuevo = 0;
        }
//        if (flag === 0) {
//            if ($.fn.DataTable.isDataTable('#tblGrpPrepCompar')) {
//                tblGrpPrepCompar.destroy();
//                completeAjax(2);
//            }
//        }
//            }//
//        });
    });

    /*
     * Objetivo: Agregar una fila vacia a la tabla y manejar los datos
     * 
     * Descripción: 
     * 
     * Escucha un evento click en el boton,cuando se genera el evento se agrega 
     * un elemento <tr> a la tabla, y se inicializa el componente de busqueda 
     * mediante la clase searchG,el cual desplegara una ventana emergente donde 
     * se muestra el catalogo de grupos según carrera.
     * 
     * Tag:  <button>
     * Id:   #btnAgregarGrupoPC
     * View: gruposGeneral_view.jsp
     * 
     * 
     */
    var lMaterias;
    $('#btnAgregarGrupoPC').on('click', function() {
        var paPlanId = $('option:selected', "#selPlanes").attr('value');
        var paEspecId = $('option:selected', "#selEspec").attr('value');
        var flag;

        if (nuevo === 1) {
            //alert("IF++nuevo::"+nuevo+"\ntoSaveC::"+toSaveC);
            flag = guardarGrupo(paPlanId, paEspecId, toSaveC, 2, 'tbodyGruposPC');
            if (flag === 0) {
                toSaveC = undefined;
//                if ($.fn.DataTable.isDataTable('#tblGrpPrepCompar')) {
//                    tblGrpPrepCompar.destroy();
//                    completeAjax(2);
//                }
            }
        } else if (nuevo === 0) {
            //alert("IFELSE++nuevo::"+nuevo+"\ntoSaveC::"+toSaveC);
            nuevo = 1;
            flag = 1;
        }
        if (flag >= 0) {

            if (opCat === "2" || opCat === "4") {
                var tr = $("<tr data-type='0'>" +
                        "<td class='bloqueGrupo'></td>" +
                        "<td class='searchG'><a href='#'><i class='fa fa-search'></i></a></td>" +
                        "<td class='no-editable'></td>" +
                        "<td class='gpoId'></td>" +
                        "<td class='cell-edit'></td>" +
                        "<td class='no-editable' data-day='1'></td>" +
                        "<td class='no-editable' data-day='2'></td>" +
                        "<td class='no-editable' data-day='3'></td>" +
                        "<td class='no-editable' data-day='4'></td>" +
                        "<td class='no-editable' data-day='5'></td>" +
                        "<td class='no-editable' data-day='6'></td>" + //SABADO
                        "<td class='no-editable'></td>" +
                        "<td ></td>" + //inscritos
                        "<td ></td>" + //inscritos totales
                        "<td class='details-control'></td>"
                        + "</tr>");//Creamos elemento <tr>
            } else {
                var tr = $("<tr data-type='0'>" +
                        "<td class='bloqueGrupo'></td>" +
                        "<td class='searchG'><a href='#'><i class='fa fa-search'></i></a></td>" +
                        "<td class='no-editable'></td>" +
                        "<td class='gpoId'></td>" +
                        "<td class='cell-edit'></td>" +
                        "<td class='no-editable' data-day='1'></td>" +
                        "<td class='no-editable' data-day='2'></td>" +
                        "<td class='no-editable' data-day='3'></td>" +
                        "<td class='no-editable' data-day='4'></td>" +
                        "<td class='no-editable' data-day='5'></td>" +
                        "<td class='no-editable' data-day='6'></td>" + //SABADO
                        "<td class='no-editable'></td>" +
                        "<td class='details-control'></td>"
                        + "</tr>");//Creamos elemento <tr>
            }
            tr.attr('data-key', (dataPrepC.length));//se le asigna un id, este sera la longitud del arreglo dataPrep

            dataPrepC.push([]);//se agrega un elemento vacio a dataPrepC

            var idNewTr = tr.attr('data-key');

            tblGrpPrepCompar.row.add(tr).draw();

            tr.on("click", "td.searchG", function(e) {
                e.preventDefault();

                $.getJSON("catalogogrupos.do", {opcion: 3}, function(data) {
                    if (Number(data.paCodError) === 0) {
                        var $select = $("#selCarrera");
                        $select.find('option').remove();
                        $.each(data.retorno, function(key, value) {
                            var opt = $('<option>').val(value.carrera_Id).text(value.nom_Cto).appendTo($select);
                            opt.attr("data-nomCto", value.nom_Cto);
                        });
                        $('#btnAceptarCatGrup').attr('data-trCall', idNewTr);

                        $("#selCarrera").change();

                        $('#divModalCatMatComp').modal("show");
                    } else {
                        bootbox.alert(data.paDesError);
                    }

                });
            });

            tr.on("click", "td.gpoId", function(e) {
                e.preventDefault();
                $.ajax({//Peticion para obtener catalogo de materias
                    url: "catalogomaterias.do",
                    type: 'POST',
                    async: false,
                    dataType: 'json',
                    data: {
                        paPlanId: paPlanId
                        , paEspeciaId: paEspecId
                    },
                    success: function(result) {
                        if ($.fn.DataTable.isDataTable('#tblCatMaterias')) {
                            tblCatMaterias.destroy();
                        }
                        var tbody = $("#tbodyMaterias");
                        var idNewTr = tr.attr('data-key');

                        tbody.find("tr").remove();

                        if (Number(result.paCodError) === 0) {
                            $('#btnAceptarEq').attr('data-trCall', idNewTr);
                            lMaterias = result.retorno;

                            $.each(result.retorno, function(key, object) {

                                var tr = $("<tr></tr>");
                                tbody.append(tr);
                                tr.attr("data-key", key);
                                tr.addClass(object.trClass);

                                var td = $("<td>" + object.materia_Id + "</td>");
                                tr.append(td);

                                var td = $("<td>" + object.nombre_Materia + "</td>");
                                tr.append(td);

                                var td = $("<td>" + object.creditos_Materia + "</td>");
                                tr.append(td);

                                var td = $("<td>" + object.academia_Id + "</td>");
                                tr.append(td);
                            });

                            tblCatMaterias = $('#tblCatMaterias').DataTable({
                                "responsive":true,
                                "language": {
                                    "paginate": {
                                        "next": "Siguiente",
                                        "previous": "Anterior"
                                    },
                                    "search": "Buscar: ",
                                    "zeroRecords": "No se encontraron coincidencias"
                                },
                                "info": false,
                                "destroy": true
                                , "lengthChange": false
                            });
                            $('#btnAceptarCatMat').css("display", "none");
                            $('#btnAceptarEq').css("display", "block");
                            $('#divModalCatMaterias').modal("show");
                        } else {

                            bootbox.alert(result.paDesError);

                        }

                    }
                });

            });

        } else if (flag === -1) {
            nuevo = 1;
        }
    });

    /*
     * Objetivo: Eliminar grupo de la tabla grupos
     * 
     * Descripcion:
     * 
     * Escucha un envento click, cuando lo detecta, lanza un mensaje de confirmacion
     * en caso de que se confirme la eliminacion, hace una peticion al servidor,
     * si la eliminacion fue correcta elimina la fila que se selccionó.
     * 
     * Tag:  <button>
     * Id:   #btnElimarC
     * View: gruposGeneral_view.jsp
     *  
     */
    $('#btnElimarC').click(function() {

        var tr = $("#tbodyGruposPC tr.selected");
        var index = tr.attr("data-key");
        var type = tr.attr("data-type");
        if (index !== undefined) {
            bootbox.confirm("¿Esta seguro de eliminar este grupo?", function(result) {
                if (result) {

                    if (type === '0') {
                        //Type 0 es igual a nuevo                    
                        tblGrpPrepCompar.row('.selected').remove().draw(false);
                        nuevo = 0;
                        toSaveC = undefined;
                    } else {

                        var paGrupoId = dataPrepC[index].grupo_Id;
                        var paPeriodo = dataPrepC[index].periodo;
                        var paAño = dataPrepC[index].año;

                        eliminarGrupo(paGrupoId, paPeriodo, paAño, tblGrpPrepCompar);
                    }
                }
            });
        }

    });

    /*
     * Objetivo: Obtener grupo seleccionado y asignarlo al grupo compartido
     * 
     * Descripcion: Escucha un evento click del boton aceptar del modal catalogo grupos
     * el cual asigna el grupo seleccionado de la tabla de catalogo grupos a la tabla de
     * grupos preparacion compartidos.
     * 
     * Tag:  <button>
     * Id:   #btnAceptarCatGrup
     * View: grupos_catmateriascompart_modal.jsp
     * 
     */
    $("#btnAceptarCatGrup").click(function() {


        var tr2 = $('#tblCatGrupos tbody tr.selected');
        var key = tr2.attr("data-key");
        if (key !== undefined) {
            var trn = $(this).attr('data-trCall');
            var tr = $("#tbodyGruposPC tr[data-key=" + trn + "]");
            var idComp = lGrupos[key].grupo_Id;
            dataPrepC[trn] = {
                periodo: lGrupos[key].periodo,
                año: lGrupos[key].año,
                bloque: lGrupos[key].bloque,
                grupo_Id: String(idComp).substring(0, String(idComp).length - 2),
                nombre_Materia: lGrupos[key].nombre_Materia,
                nom_Materia_Corto: lGrupos[key].nom_Materia_Corto, //
                creditos: lGrupos[key].creditos,
                capacidad: 0,
                inscritos: 0,
                inscTot: 0,
                tope_Alumno: 0,
                acad_Imparte_Mat: lGrupos[key].acad_Imparte_Mat,
                nombre_Prof: lGrupos[key].nombre_Prof,
                H_LUN: lGrupos[key].H_LUN,
                H_MAR: lGrupos[key].H_MAR,
                H_MIE: lGrupos[key].H_MIE,
                H_JUE: lGrupos[key].H_JUE,
                H_VIE: lGrupos[key].H_VIE,
                H_SAB: lGrupos[key].H_SAB, //SABADO
                grupoId_Real: lGrupos[key].grupo_Id,
                compartido: $('option:selected', "#selCarrera").attr("data-nomCto"),
                plan_Id: lGrupos[key].plan_Id,
                esp_Id: lGrupos[key].esp_Id
            };

            lGrupos = null;

            toSaveC = tr.attr('data-key');
            tr.children().eq(0).text(dataPrepC[trn].bloque);
            tr.children().eq(1).addClass("no-editable");
            tr.children().eq(1).text(dataPrepC[trn].grupoId_Real);
            tr.children().eq(2).text(dataPrepC[trn].nom_Materia_Corto);
            tr.children().eq(3).text(dataPrepC[trn].grupo_Id);
            tr.children().eq(3).html("<a href='#'><i class='fa fa-search'></i></a>");
            tr.children().eq(5).text(dataPrepC[trn].H_LUN);
            tr.children().eq(6).text(dataPrepC[trn].H_MAR);
            tr.children().eq(7).text(dataPrepC[trn].H_MIE);
            tr.children().eq(8).text(dataPrepC[trn].H_JUE);
            tr.children().eq(9).text(dataPrepC[trn].H_VIE);
            tr.children().eq(10).text(dataPrepC[trn].H_SAB); //SABADO
            tr.children().eq(11).text(dataPrepC[trn].compartido);
            tr.children("td.searchG").removeClass("searchG");
            $('#divModalCatMaterias').modal("hide");

        } else {

            bootbox.alert("Debe seleccionar un grupo");
        }

    });

    /*
     * Objetivo: Desplegar y Replegar tabla de detalles
     * 
     * Descripción: 
     * 
     * Escucha un evento click en el icono de detalles de la tabla ubicado en la 
     * ultima columna, cuando sucede el click concate una tabla a la fila donde 
     * se muestran los datos restantes del grupo. Si ya esta deplegada y sucede
     * el click se repliega la tabla de detalles.
     * 
     * Tag:  <table>
     * Id:   #tblGrpPrepCompar
     * View: gruposGeneral_view.jsp
     * 
     */
    $('#tblGrpPrepCompar tbody').on('click', 'td.details-control', function() {
        var tr = $(this).closest('tr');
        adjuntarDetalles(tblGrpPrepCompar, 2, tr, opCat);
    });


    /*
     * Objetivo: Desplegar y Replegar tabla de detalles
     * 
     * Descripción: 
     * 
     * Escucha un evento click en el icono de detalles de la tabla ubicado en la 
     * ultima columna, cuando sucede el click concate una tabla a la fila donde 
     * se muestran los datos restantes del grupo. Si ya esta deplegada y sucede
     * el click se repliega la tabla de detalles.
     * 
     * Tag:  <table>
     * Id:   #tblCatGrupos
     * View: grupos_catmateriascompart_modal.jsp
     * 
     */
    $('#tblCatGrupos tbody').on('click', 'td.details-control', function() {
        var tr = $(this).closest('tr');
        adjuntarDetalles(tblCatGrupos, 3, tr);
    });

    //Agregar un on change al input de la capacidad de grupo el cual va a la funcion

    /*
     * 
     * Objetivo: Editar el bloque del grupo 
     *
     * Descripción: 
     * Este metodo escucha un evento click
     * y habilita la edicion del campo bloque
     *  
     * Tag:  <table>
     * Id:   #tblGrpPrepCompar
     * View: gruposGeneral_view.jsp
     * 
     */
    $('#tblGrpPrepCompar tbody').on('click', 'td.bloqueGrupo', function() {
        var tr = $(this).closest('tr');
        editarCelda(tr, 'bloqueGrupo', $(this), 3, toUpdateC, 2, "tbodyGruposPC");

    });

    /*
     * 
     * Objetivo: Editar la capacidad de un grupo
     *
     * Descripción: 
     * Este metodo escucha un evento click
     * y habilita la edicion del campo capacidad
     *  
     * Tag:  <table>
     * Id:   #tblGrpPrepCompar
     * View: gruposGeneral_view.jsp
     * 
     */
    $('#tblGrpPrepCompar tbody').on('click', 'td.cell-edit', function() {
        var tr = $(this).closest('tr');
        editarCelda(tr, 'cell-edit', $(this), 4, toUpdateC, 2, "tbodyGruposPC");
    });

    $('#tblGrpPrepCompar tbody').on('click', 'td.ediTope', function() {
        var tr = $(this).closest('tr');
        editarCelda(tr, 'ediTope', $(this), 7, toUpdateC, 2, "tbodyGruposPC");
    });


    /*
     * Objetivo: Agregar o quitar clase selected cuando un <tr> es seleccionado
     * 
     * Descripción: 
     * 
     * Se agrega la clase selected a un elemento <tr> cuando este es seleccionado,
     * la clase sombrea la fila indicando que esta seleccionada, solo se puede 
     * seleccionar un elemento <tr> a la vez.
     * 
     * Tag:  <table>
     * Id:   #tblCatGrupos
     * View: grupos_catmateriascompart_modal.jsp
     * 
     */
    $("#tblCatGrupos tbody").on('click', 'tr', function() {
        var tr = $(this);
        selectUnSelect('#tblCatGrupos', tr);
    });

    /*
     * Objetivo: Agregar o quitar clase selected cuando un <tr> es seleccionado
     * 
     * Descripción: 
     * 
     * Se agrega la clase selected a un elemento <tr> cuando este es seleccionado,
     * la clase sombrea la fila indicando que esta seleccionada, solo se puede 
     * seleccionar un elemento <tr> a la vez.
     * 
     * Tag:  <table>
     * Id:   #tblGrpPrepCompar
     * View: gruposGeneral_view.jsp
     * 
     */
    $("#tblGrpPrepCompar tbody").on('click', 'td.no-editable', function() {
        var tr = $(this).closest('tr');
        selectUnSelect('#tblGrpPrepCompar', tr);
    });
    
    /*
     * Objetivo: Agregar el id de grupo compartido al tr
     * 
     * Descripción: 
     * 
     *Se agrega la variable mat al texto del td que pertenece al grupo_id 
     *del grupo compartido
     * 
     * Tag:  <table>
     * Id:   #tbodyGruposPC
     * View: gruposGeneral_view.jsp
     * 
     */

    $("#btnAceptarEq").on('click', function() {
        var tr2 = $('#tblCatMaterias tbody tr.selected');
        var key = tr2.attr("data-key");
        if (key !== undefined) {
            var trn = $(this).attr('data-trCall');
            var tr = $("#tbodyGruposPC tr[data-key=" + trn + "]");
            var mat = lMaterias[key].materia_Id;
            dataPrepC[trn].grupo_Id = mat;
            tr.children().eq(3).text(mat);
            $('#divModalCatMaterias').modal("hide");
        }
    });

});





