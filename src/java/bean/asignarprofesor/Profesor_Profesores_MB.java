/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bean.asignarprofesor;

/**
 *
 * @author Edgar
 */
public final class Profesor_Profesores_MB {
    private int pro_Id;
    private String nombre_Prof;
    private int academia_Id;
    private String horas_asig;
    private String horas_ocup;
    private String OBS;
    private String trClass;
    private String idClass;
    
    
    public Profesor_Profesores_MB(int pro_Id,
            String nombre_Prof,
            int academia_Id){
        this.pro_Id=pro_Id;
        this.nombre_Prof=nombre_Prof;
        this.academia_Id=academia_Id;
    }
    public Profesor_Profesores_MB(
            int pro_Id,
    String nombre_Prof,
       int academia_Id,
    String horas_ocup,
    String OBS,int comodin){
        this(pro_Id, nombre_Prof, academia_Id);
        this.horas_ocup = horas_ocup;
        setOBS(OBS);        
    }
    
    public Profesor_Profesores_MB(
    int pro_Id,
    String nombre_Prof,
    int academia_Id,
    String horas_asig,
    String horas_ocup
    ){
        this(pro_Id, nombre_Prof, academia_Id);
        this.horas_asig=horas_asig;        
        this.horas_ocup=horas_ocup;          
    }
    
    

    public int getPro_Id() {
        return pro_Id;
    }

    public void setPro_Id(int pro_Id) {
        this.pro_Id = pro_Id;
    }

    public String getNombre_Prof() {
        return nombre_Prof;
    }

    public void setNombre_Prof(String nombre_Prof) {
        this.nombre_Prof = nombre_Prof;
    }

    public int getAcademia_Id() {
        return academia_Id;
    }

    public void setAcademia_Id(int academia_Id) {
        this.academia_Id = academia_Id;
    }

    public String getHoras_asig() {
        return horas_asig;
    }

    public void setHoras_asig(String horas_asig) {
        this.horas_asig = horas_asig;
    }

    public String getHoras_ocup() {
        return horas_ocup;
    }

    public void setHoras_ocup(String horas_ocup) {
        this.horas_ocup = horas_ocup;
    }

    public String getOBS() {
        return OBS;
    }

    public void setOBS(String OBS) {
        if (OBS.equals("OK")) {
            setTrClass("noObsClass");
            setIdClass("highligth");
        }else{
            setTrClass("ObsClass");
            setIdClass("error");
        }
        this.OBS = OBS;
    }

    public String getTrClass() {
        return trClass;
    }

    public void setTrClass(String trClass) {
        this.trClass = trClass;
    }

    public String getIdClass() {
        return idClass;
    }

    public void setIdClass(String idClass) {
        this.idClass = idClass;
    }
}
