/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bean.asignarprofesor;

/**
 *
 * @author Edgar
 */
public class Academia_Profesores_MB {
    
    private int id;
    private String nivel;
    private String nom_Cto;
    private String nombre;
    
    public Academia_Profesores_MB(int id,String nivel,String nom_Cto, String nombre){
        this.id=id;
        this.nivel=nivel;
        this.nom_Cto=nom_Cto;
        this.nombre=nombre;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNivel() {
        return nivel;
    }

    public void setNivel(String nivel) {
        this.nivel = nivel;
    }

    public String getNom_Cto() {
        return nom_Cto;
    }

    public void setNom_Cto(String nom_Cto) {
        this.nom_Cto = nom_Cto;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
}
