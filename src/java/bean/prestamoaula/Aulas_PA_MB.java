/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bean.prestamoaula;

/**
 *
 * @author IthzaVG
 */
public class Aulas_PA_MB 
{
    private String aula;
    private int capacidad;

    
    public Aulas_PA_MB(String aula, int capacidad )
    {
        this.aula=aula;
        this.capacidad= capacidad;
     
        
    }

    public String getAula() {
        return aula;
    }

    public void setAula(String aula) {
        this.aula = aula;
    }

    public int getCapacidad() {
        return capacidad;
    }

    public void setCapacidad(int capacidad) {
        this.capacidad = capacidad;
    }

  
    
    
    
    
    
}
