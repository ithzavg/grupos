/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bean.prestamoaula;

/**
 *
 * @author IthzaVG
 */
public class Carreras_PA_MB {
    
    private int carreraId;
    private String nombre;
    private String nom_cto;
    
    public Carreras_PA_MB(int carreraId, String nombre, String nom_cto){
        this.carreraId=carreraId;
        this.nombre=nombre;
        this.nom_cto=nom_cto;
    }

    /**
     * @return the carreraId
     */
    public int getCarreraId() {
        return carreraId;
    }

    /**
     * @param carreraId the carreraId to set
     */
    public void setCarreraId(int carreraId) {
        this.carreraId = carreraId;
    }

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @return the nom_cto
     */
    public String getNom_cto() {
        return nom_cto;
    }

    /**
     * @param nom_cto the nom_cto to set
     */
    public void setNom_cto(String nom_cto) {
        this.nom_cto = nom_cto;
    }
    
}
