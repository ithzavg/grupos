/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bean.prestamoaula;

/**
 *
 * @author IthzaVG
 */
public class Edificios_PA_MB 
{
    String edificioId;
    String nombre;
    
    public Edificios_PA_MB(String edificioId, String nombre)
    {
        this.edificioId= edificioId;
        this.nombre=nombre;
        
    }

    public String getEdificioId() {
        return edificioId;
    }

    public void setEdificioId(String edificioId) {
        this.edificioId = edificioId;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
}
