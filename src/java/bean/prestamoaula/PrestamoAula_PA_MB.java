/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bean.prestamoaula;

/**
 *
 * @author IthzaVG
 */
public class PrestamoAula_PA_MB 
{
   private String edificio;
   private String aula;
   private String carrera_propietario;
   private String horario;
   private String dia;
   private String carrera_prestamo;
   private int id_carrera_prop;
   private int num_dia;
   private int id_carrera_prestamo;

    public PrestamoAula_PA_MB(String edificio, String aula, String carrera_propietario,int id_carrera_prop, 
            String horario, String dia, String carrera_prestamo, int num_dia, int id_carrera_prestamo) {
        this.edificio = edificio;
        this.aula = aula;
        this.carrera_propietario = carrera_propietario;
        this.horario = horario;
        this.dia = dia;
        this.carrera_prestamo = carrera_prestamo;
        this.id_carrera_prop = id_carrera_prop;
        this.num_dia = num_dia;
        this.id_carrera_prestamo = id_carrera_prestamo;
    }

  
 
   
  

    /**
     * @return the edificio
     */
    public String getEdificio() {
        return edificio;
    }

    /**
     * @param edificio the edificio to set
     */
    public void setEdificio(String edificio) {
        this.edificio = edificio;
    }

    /**
     * @return the aula
     */
    public String getAula() {
        return aula;
    }

    /**
     * @param aula the aula to set
     */
    public void setAula(String aula) {
        this.aula = aula;
    }

    /**
     * @return the carrera_propietario
     */
    public String getCarrera_propietario() {
        return carrera_propietario;
    }

    /**
     * @param carrera the carrera_propietario to set
     */
    public void setCarrera_propietario(String carrera) {
        this.carrera_propietario = carrera;
    }

    /**
     * @return the hora_inicio
     */
    public String getHorario() {
        return horario;
    }

    /**
     * @param horario the hora_inicio to set
     */
    public void setHorario(String horario) {
        this.horario = horario;
    }

    /**
     * @return the dia
     */
    public String getDia() {
        return dia;
    }

    /**
     * @param dia the dia to set
     */
    public void setDia(String dia) {
        this.dia = dia;
    }

      public String getCarrera_prestamo() {
        return carrera_prestamo;
    }

    public void setCarrera_prestamo(String carrera_prestamo) {
        this.carrera_prestamo = carrera_prestamo;
    }

    /**
     * @return the id_carrera_prop
     */
    public int getId_carrera_prop() {
        return id_carrera_prop;
    }

    /**
     * @param id_carrera_prop the id_carrera_prop to set
     */
    public void setId_carrera_prop(int id_carrera_prop) {
        this.id_carrera_prop = id_carrera_prop;
    }

    /**
     * @return the num_dia
     */
    public int getNum_dia() {
        return num_dia;
    }

    /**
     * @param num_dia the num_dia to set
     */
    public void setNum_dia(int num_dia) {
        this.num_dia = num_dia;
    }

    /**
     * @return the id_carrera_prestamo
     */
    public int getId_carrera_prestamo() {
        return id_carrera_prestamo;
    }

    /**
     * @param id_carrera_prestamo the id_carrera_prestamo to set
     */
    public void setId_carrera_prestamo(int id_carrera_prestamo) {
        this.id_carrera_prestamo = id_carrera_prestamo;
    }
   
   
    
}
