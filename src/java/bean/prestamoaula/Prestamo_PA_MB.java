/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bean.prestamoaula;

/**
 *
 * @author IthzaVG
 */
public class Prestamo_PA_MB 
{
    String aula;
    String edificio;
    int carrera_prestamo;
    int hora_inicio;
    int hora_fin;
    int dia;

    public Prestamo_PA_MB(String aula, String edificio, int carrera_prestamo, int hora_inicio, int hora_fin, int dia) 
    {
        this.aula = aula;
        this.edificio = edificio;
        this.carrera_prestamo = carrera_prestamo;
        this.hora_inicio = hora_inicio;
        this.hora_fin = hora_fin;
        this.dia = dia;
    }
    
    

    public String getAula() {
        return aula;
    }

    public void setAula(String aula) {
        this.aula = aula;
    }

    public String getEdificio() {
        return edificio;
    }

    public void setEdificio(String edificio) {
        this.edificio = edificio;
    }

    public int getCarrera_prestamo() {
        return carrera_prestamo;
    }

    public void setCarrera_prestamo(int carrera_prestamo) {
        this.carrera_prestamo = carrera_prestamo;
    }

    public int getHora_inicio() {
        return hora_inicio;
    }

    public void setHora_inicio(int hora_inicio) {
        this.hora_inicio = hora_inicio;
    }

    public int getHora_fin() {
        return hora_fin;
    }

    public void setHora_fin(int hora_fin) {
        this.hora_fin = hora_fin;
    }

    public int getDia() {
        return dia;
    }

    public void setDia(int dia) {
        this.dia = dia;
    }
}
