/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bean.prestamoaula;

/**
 *
 * @author IthzaVG
 */
public class OtrasAulas_PA_MB 
{
   private String edificio;
   private String aula;
   private String carrera_propietario;
   private String horario;
   private String dia;
   private String carrera_prestamo;
  

  
 
   
   public OtrasAulas_PA_MB(String edificio, String aula, String carrera_propietario, String carrera_prestamo, String horario, String dia)
   {
      this.edificio=edificio;
      this.aula=aula;
      this.carrera_propietario=carrera_propietario;
      this.carrera_prestamo=carrera_prestamo;
      this.horario=horario;
      this.dia=dia;
     
   }

    /**
     * @return the edificio
     */
    public String getEdificio() {
        return edificio;
    }

    /**
     * @param edificio the edificio to set
     */
    public void setEdificio(String edificio) {
        this.edificio = edificio;
    }

    /**
     * @return the aula
     */
    public String getAula() {
        return aula;
    }

    /**
     * @param aula the aula to set
     */
    public void setAula(String aula) {
        this.aula = aula;
    }

    /**
     * @return the carrera_propietario
     */
    public String getCarrera_propietario() {
        return carrera_propietario;
    }

    /**
     * @param carrera the carrera_propietario to set
     */
    public void setCarrera_propietario(String carrera) {
        this.carrera_propietario = carrera;
    }

    /**
     * @return the hora_inicio
     */
    public String getHorario() {
        return horario;
    }

    /**
     * @param horario the hora_inicio to set
     */
    public void setHorario(String horario) {
        this.horario = horario;
    }

    /**
     * @return the hora_fin
     */
    public String getDia() {
        return dia;
    }

    /**
     * @param dia the hora_fin to set
     */
    public void setDia(String dia) {
        this.dia = dia;
    }

      public String getCarrera_prestamo() {
        return carrera_prestamo;
    }

    public void setCarrera_prestamo(String carrera_prestamo) {
        this.carrera_prestamo = carrera_prestamo;
    }

   
    
}
