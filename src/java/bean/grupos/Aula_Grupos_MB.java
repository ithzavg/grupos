/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bean.grupos;

/**
 *
 * @author Edgar Cirilo González
 */
public final class Aula_Grupos_MB {
     private String nombre;
     private String edificio;
     private int capacidad;     
     private String obs;     
     private String idClass;
     private String trClass;

    public Aula_Grupos_MB(String nombre, String edificio, int capacidad, String obs) {
        this.nombre = nombre;
        this.edificio = edificio;
        this.capacidad = capacidad;
        setObs(obs);
    }
 
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getCapacidad() {
        return capacidad;
    }

    public void setCapacidad(int capacidad) {
        this.capacidad = capacidad;
    }

    public String getObs() {
        return obs;
    }

    public void setObs(String obs) {
        if (obs.equals("OK")) {
            setTrClass("noObsClass");
            setIdClass("highligth");
        }else{
            setTrClass("ObsClass");
            setIdClass("error");
        }
        this.obs = obs;
    }

    public String getIdClass() {
        return idClass;
    }

    public void setIdClass(String idClass) {
        this.idClass = idClass;
    }

    public String getTrClass() {
        return trClass;
    }

    public void setTrClass(String trClass) {
        this.trClass = trClass;
    }

    public String getEdificio() {
        return edificio;
    }

    public void setEdificio(String edificio) {
        this.edificio = edificio;
    }
     
}
