/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bean.grupos;

/**
 *
 * @author Edgar
 */
public final class Materia_Grupos_MB {
    private int materia_Id;
    private String nombre_Materia;
    private int creditos_Materia;
    private String nombre_Corto;
    private String academia_Id;
    private String trClass;
    
    public Materia_Grupos_MB(int materia_Id,
    String nombre_Materia,
    int creditos_Materia,
    String nombre_Corto,
    String academia_Id){
        this.creditos_Materia=creditos_Materia;
        this.nombre_Materia=nombre_Materia;
        this.materia_Id=materia_Id;
        this.nombre_Corto=nombre_Corto;
        setAcademia_Id(academia_Id);
    }

    public String getTrClass() {
        return trClass;
    }

    public void setTrClass(String trClass) {
        this.trClass = trClass;
    }

    public int getMateria_Id() {
        return materia_Id;
    }

    public void setMateria_Id(int materia_Id) {
        this.materia_Id = materia_Id;
    }

    public String getNombre_Materia() {
        return nombre_Materia;
    }

    public void setNombre_Materia(String nombre_Materia) {
        this.nombre_Materia = nombre_Materia;
    }

    public int getCreditos_Materia() {
        return creditos_Materia;
    }

    public void setCreditos_Materia(int creditos_Materia) {
        this.creditos_Materia = creditos_Materia;
    }

    public String getNombre_Corto() {
        return nombre_Corto;
    }

    public void setNombre_Corto(String nombre_Corto) {
        this.nombre_Corto = nombre_Corto;
    }

    public String getAcademia_Id() {
        return academia_Id;
    }

    public void setAcademia_Id(String academia_Id) {
        try{
            Integer.parseInt(academia_Id);
            trClass = "isSelectable";            
        }catch(NumberFormatException ex){
            trClass = "danger";            
        }
        
        this.academia_Id = academia_Id;
    }
}
