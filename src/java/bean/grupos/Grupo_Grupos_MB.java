/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bean.grupos;

/**
 *
 * @author Edgar
 */
public final class Grupo_Grupos_MB {

    private int periodo;
    private int año;
    private int bloque;
    private int grupo_Id;
    private String nombre_Materia;
    private String nom_Materia_Corto;
    private int creditos;
    private int capacidad;
    private int inscritos;
    private int insTotales;
    private int tope_Alumno;
    private String acad_Imparte_Mat;
    private String nombre_Prof;
    private String H_LUN = null;
    private String H_MAR = null;
    private String H_MIE = null;
    private String H_JUE = null;
    private String H_VIE = null;
    private String H_SAB = null;
    private int plan_Id;
    private int esp_Id;
    
    //Variables para Grupos Compartidos
    private int grupoId_Real;
    private String compartido;
 

    public Grupo_Grupos_MB(
            int bloque,
            int grupo_Id,
            String nombre_Materia,
            String nom_Materia_Corto,
            int creditos,
            int capacidad,
            int periodo,
            int año,
            String acad_Imparte_Mat,
            String H_LUN,
            String H_MAR,
            String H_MIE,
            String H_JUE,
            String H_VIE,
            String H_SAB//SABADO
    ) {

        this.bloque = bloque;
        this.grupo_Id = grupo_Id;
        this.nombre_Materia = nombre_Materia;
        this.nom_Materia_Corto = nom_Materia_Corto;
        this.creditos = creditos;
        this.capacidad = capacidad;
        this.periodo = periodo;
        this.año = año;
        this.acad_Imparte_Mat = acad_Imparte_Mat;
        setH_LUN(H_LUN);
        setH_MAR(H_MAR);
        setH_MIE(H_MIE);
        setH_JUE(H_JUE);
        setH_VIE(H_VIE);
        setH_SAB(H_SAB);//SABADO
    }
    
     public Grupo_Grupos_MB(
            int bloque,
            int grupo_Id,
            String nombre_Materia,
            String nom_Materia_Corto,
            int creditos,
            int capacidad,                        
            int periodo,
            int año,
            String acad_Imparte_Mat,            
            String H_LUN,
            String H_MAR,
            String H_MIE,
            String H_JUE,
            String H_VIE, 
            String H_SAB, //SABADO
            int inscritos,
            int insTotales,
            int tope_Alumno,
            String nombre_Prof
    ) {
        this(bloque, grupo_Id, nombre_Materia, nom_Materia_Corto, creditos, capacidad, periodo, año, acad_Imparte_Mat, H_LUN, H_MAR, H_MIE, H_JUE, H_VIE,H_SAB);        
        this.inscritos=inscritos;
        this.insTotales=insTotales;
        this.tope_Alumno=tope_Alumno;
        this.nombre_Prof=nombre_Prof;
        
    }
    
    public Grupo_Grupos_MB(
            int bloque,
            int grupo_Id,
            String nombre_Materia,
            String nom_Materia_Corto,
            int creditos,
            int capacidad,                        
            int periodo,
            int año,
            String acad_Imparte_Mat,            
            String H_LUN,
            String H_MAR,
            String H_MIE,
            String H_JUE,
            String H_VIE, 
            String H_SAB, //SABADO
            int inscritos,
            int insTotales,
            int tope_Alumno,
            String nombre_Prof,
            int plan_Id,
            int esp_Id
    ) {
        this(bloque, grupo_Id, nombre_Materia, nom_Materia_Corto, creditos, capacidad, periodo, año, acad_Imparte_Mat, H_LUN, H_MAR, H_MIE, H_JUE, H_VIE,H_SAB,inscritos,insTotales,tope_Alumno,nombre_Prof);        
        this.plan_Id = plan_Id;
        this.esp_Id = esp_Id;
        
        
    }
    
    public Grupo_Grupos_MB(
            int bloque,
            int grupo_Id,
            String nombre_Materia,
            String nom_Materia_Corto,
            int creditos,
            int capacidad,                        
            int periodo,
            int año,
            String acad_Imparte_Mat,            
            String H_LUN,
            String H_MAR,
            String H_MIE,
            String H_JUE,
            String H_VIE, 
            String H_SAB, //SABADO 
            int inscritos,
            int tope_Alumno,
            String nombre_Prof,
            int groupId_Real,
            String compartido
    ) {
        this(bloque, grupo_Id, nombre_Materia, nom_Materia_Corto, creditos, capacidad, periodo, año, acad_Imparte_Mat, H_LUN, H_MAR, H_MIE, H_JUE, H_VIE,H_SAB, inscritos,0 ,tope_Alumno, nombre_Prof);
        this.grupoId_Real =  groupId_Real;
        this.compartido = compartido;        
    }
                          
    public int getGrupo_Id() {
        return grupo_Id;
    }

    public void setGrupo_Id(int grupo_Id) {
        this.grupo_Id = grupo_Id;
    }

    public String getNombre_Materia() {
        return nombre_Materia;
    }

    public void setNombre_Materia(String nombre_Materia) {
        this.nombre_Materia = nombre_Materia;
    }

    public int getCapacidad() {
        return capacidad;
    }

    public void setCapacidad(int capacidad) {
        this.capacidad = capacidad;
    }

    public int getInscritos() {
        return inscritos;
    }

    public void setInscritos(int inscritos) {
        this.inscritos = inscritos;
    }

    public String getNombre_Prof() {
        return nombre_Prof;
    }

    public void setNombre_Prof(String nombre_Prof) {
        this.nombre_Prof = nombre_Prof;
    }

    public int getBloque() {
        return bloque;
    }

    public void setBloque(int bloque) {
        this.bloque = bloque;
    }

    public String getNom_Materia_Corto() {
        return nom_Materia_Corto;
    }

    public void setNom_Materia_Corto(String nom_Materia_Corto) {
        this.nom_Materia_Corto = nom_Materia_Corto;
    }

    public int getCreditos() {
        return creditos;
    }

    public void setCreditos(int creditos) {
        this.creditos = creditos;
    }

    public int getTope_Alumno() {
        return tope_Alumno;
    }

    public void setTope_Alumno(int tope_Alumno) {
        this.tope_Alumno = tope_Alumno;
    }

    public String getAcad_Imparte_Mat() {
        return acad_Imparte_Mat;
    }

    public void setAcad_Imparte_Mat(String acad_Imparte_Mat) {
        this.acad_Imparte_Mat = acad_Imparte_Mat;
    }

    public String getH_LUN() {
        return H_LUN;
    }

    public void setH_LUN(String H_LUN) {
        if (H_LUN != null && !H_LUN.equals("")) {
            this.H_LUN = H_LUN;
        }

    }

    public String getH_MAR() {
        return H_MAR;
    }

    public void setH_MAR(String H_MAR) {
        if (H_MAR != null && !H_MAR.equals("")) {
            this.H_MAR = H_MAR;
        }

    }

    public String getH_MIE() {
        return H_MIE;
    }

    public void setH_MIE(String H_MIE) {
        if (H_MIE != null && !H_MIE.equals("")) {
            this.H_MIE = H_MIE;
        }

    }

    public String getH_JUE() {
        return H_JUE;
    }

    public void setH_JUE(String H_JUE) {
        if (H_JUE != null && !H_JUE.equals("")) {
            this.H_JUE = H_JUE;
        }

    }

    public String getH_VIE() {
        return H_VIE;
    }

    public void setH_VIE(String H_VIE) {
        if (H_VIE != null && !H_VIE.equals("")) {
            this.H_VIE = H_VIE;
        }
    }
    
    public String getH_SAB() {
        return H_SAB;
    }

    public void setH_SAB(String H_SAB) {
        if (H_SAB != null && !H_SAB.equals("")) {
            this.H_SAB = H_SAB;
        }
    }

    public int getPeriodo() {
        return periodo;
    }

    public void setPeriodo(int periodo) {
        this.periodo = periodo;
    }

    public int getAño() {
        return año;
    }

    public void setAño(int año) {
        this.año = año;
    }
    
    public boolean validarHorario(){
        boolean flag;        
        flag = !((H_LUN==null || H_LUN.isEmpty())
               &&(H_MAR==null || H_MAR.isEmpty())
               &&(H_MIE==null || H_MIE.isEmpty())
               &&(H_JUE==null || H_JUE.isEmpty())
               &&(H_VIE==null || H_VIE.isEmpty())        
               &&(H_SAB==null || H_SAB.isEmpty()));        //SABADO
        return flag;
    }
    
    public boolean validarBloque(){
        return bloque!=0;
    }
    
   
    public int getGrupoId_Real() {
        return grupoId_Real;
    }

    public void setGrupoId_Real(int grupoId_Real) {
        this.grupoId_Real = grupoId_Real;
    }

    public String getCompartido() {
        return compartido;
    }

    public void setCompartido(String compartido) {
        this.compartido = compartido;
    }

    public int getPlan_Id() {
        return plan_Id;
    }

    public void setPlan_Id(int plan_Id) {
        this.plan_Id = plan_Id;
    }

    public int getEsp_Id() {
        return esp_Id;
    }

    public void setEsp_Id(int esp_Id) {
        this.esp_Id = esp_Id;
    }
    public  boolean validaIdCompartido(){
    return grupo_Id!=0;
        }

    /**
     * @return the insTotales
     */
    public int getInsTotales() {
        return insTotales;
    }

    /**
     * @param insTotales the insTotales to set
     */
    public void setInsTotales(int insTotales) {
        this.insTotales = insTotales;
    }

}
