/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bean.general;

/**
 *
 * @author Edgar
 */
public final class Carreras_Profesores_MB {
    private int carrera_Id;
    private String nombre;
    private String nom_Cto;
    private int noAsignados;
    String checkClass="&#xf00d;";
    
    public Carreras_Profesores_MB(int carrera_Id, String nombre,String nom_Cto){
        this.carrera_Id=carrera_Id;
        this.nombre=nombre;
        this.nom_Cto=nom_Cto;
    }
    public Carreras_Profesores_MB(int carrera_Id, String nombre,String nom_Cto, int noAsignados){
        this(carrera_Id, nombre, nom_Cto);
        setNoAsignados(noAsignados);
    }
    public int getCarrera_Id() {
        return carrera_Id;
    }

    public void setCarrera_Id(int carrera_Id) {
        this.carrera_Id = carrera_Id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getNom_Cto() {
        return nom_Cto;
    }

    public void setNom_Cto(String nom_Cto) {
        this.nom_Cto = nom_Cto;
    }

    public int getNoAsignados() {
        return noAsignados;
    }

    public void setNoAsignados(int noAsignados) {
        if (noAsignados==0) {
            checkClass="&#xf00c;";
        }else{
            checkClass="&#xf12a;";
        }
        this.noAsignados = noAsignados;
    }
}
