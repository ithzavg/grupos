/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bean.general;

/**
 *
 * @author Edgar
 */
public final class Plan_Grupos_MB {
    private int plan_id;
    private String clave;
    private char competencia;
    private int noAsignados;
    private String checkClass="&#xf00d;";


    public Plan_Grupos_MB(int plan_id, String clave, char competencia){
        this.plan_id=plan_id;
        this.clave=clave;
        this.competencia=competencia;
    }
    
    public Plan_Grupos_MB(int plan_id, String clave, char competencia,int noAsignados){
        this(plan_id,clave,competencia);
        setNoAsignados(noAsignados);
    }
    public int getPlan_id() {
        return plan_id;
    }

    public void setPlan_id(int plan_id) {
        this.plan_id = plan_id;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public char getCompetencia() {
        return competencia;
    }

    public void setCompetencia(char competencia) {
        this.competencia = competencia;
    }

    public int getNoAsignados() {
        return noAsignados;
    }

    public void setNoAsignados(int noAsignados) {
        if (noAsignados==0) {
            checkClass="&#xf00c;";
        }else{
            checkClass="&#xf12a;";
        }
        this.noAsignados = noAsignados;
    }

    public String getCheckClass() {
        return checkClass;
    }

    public void setCheckClass(String checkClass) {
        this.checkClass = checkClass;
    }
}
