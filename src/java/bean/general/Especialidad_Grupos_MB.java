/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bean.general;

/**
 *
 * @author Edgar
 */
public final class Especialidad_Grupos_MB {
    private int esp_id;
    private String nombre_espec;
    private int pendientes;
    private String checkClass="&#xf00d;";
    
    
    public Especialidad_Grupos_MB(int esp_id, String nombre_espec){
        this.esp_id=esp_id;
        this.nombre_espec=nombre_espec;
    }

    public Especialidad_Grupos_MB(int esp_id, String nombre_espec, int pendientes) {
       this(esp_id,nombre_espec);
        setPendientes(pendientes);
    }
    
    

    public int getEsp_id() {
        return esp_id;
    }

    public void setEsp_id(int esp_id) {
        this.esp_id = esp_id;
    }

    public String getNombre_espec() {
        return nombre_espec;
    }

    public void setNombre_espec(String nombre_espec) {
        this.nombre_espec = nombre_espec;
    }

    public int getPendientes() {
        return pendientes;
    }
    
    public void setPendientes(int pendientes) {
        if (pendientes==0) {
            checkClass="&#xf00c;";
        }else{
            checkClass="&#xf12a;";
        }
        this.pendientes = pendientes;
    }
}
