/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bean.general;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Edgar
 * @param <T>
 */
public class GenericReturn<T> {
     private T retorno;
     private int paCodError = 1000;
     private String paDesError = "ERROR";
     private final Map<String,Object> extraParams = new HashMap<>(0);

    public Map<String, Object> getExtraParams() {
        return extraParams;
    }

    public T getRetorno() {
        return retorno;
    }

    public void setRetorno(T retorno) {
        this.retorno = retorno;
    }

    public int getPaCodError() {
        return paCodError;
    }

    public void setPaCodError(int paCodError) {
        this.paCodError = paCodError;
    }

    public String getPaDesError() {
        return paDesError;
    }

    public void setPaDesError(String paDesError) {
        this.paDesError = paDesError;
    }
}
