/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bean.reportes;

/**
 *
 * @author Edgar
 */
public class Inscrito_Grupos_MB {
     private final int noCtl;
    private final String nombre;    
    private final  String fechaOGrupo;
    
  
    
    public Inscrito_Grupos_MB(int noCtl, String nombre, String fechaOGrupo) {
        this.noCtl = noCtl;
        this.nombre = nombre;
        this.fechaOGrupo = fechaOGrupo;
    }

    public int getNoCtl() {
        return noCtl;
    }

    public String getNombre() {
        return nombre;
    }

    public String getFechaOGrupo() {
        return fechaOGrupo;
    }

   
    
}
