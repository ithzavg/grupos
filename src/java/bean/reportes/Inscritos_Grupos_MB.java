/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bean.reportes;

import java.util.List;

/**
 *
 * @author Edgar
 */
public class Inscritos_Grupos_MB {

    private String carrera;
    private String periodo;
    private String materia;
    private String profesor;
    private int tarjeta;
    private int grupo;
    private int plan;
    private String hLun;
    private String hMar;
    private String hMie;
    private String hJue;
    private String hVie;
    private String hSab;
    private List<Inscrito_Grupos_MB> lInscritos;

    
    public String getCarrera() {
        return carrera;
    }

    public void setCarrera(String carrera) {
        this.carrera = carrera;
    }

    public String getPeriodo() {
        return periodo;
    }

    public void setPeriodo(String periodo) {
        this.periodo = periodo;
    }

    public String getMateria() {
        return materia;
    }

    public void setMateria(String materia) {
        this.materia = materia;
    }

    public String getProfesor() {
        return profesor;
    }

    public void setProfesor(String profesor) {
        this.profesor = profesor;
    }

    public int getTarjeta() {
        return tarjeta;
    }

    public void setTarjeta(int tarjeta) {
        this.tarjeta = tarjeta;
    }

    public int getGrupo() {
        return grupo;
    }

    public void setGrupo(int grupo) {
        this.grupo = grupo;
    }

    public int getPlan() {
        return plan;
    }

    public void setPlan(int plan) {
        this.plan = plan;
    }

    public String gethLun() {
        return hLun;
    }

    public void sethLun(String hLun) {
        this.hLun = hLun;
    }

    public String gethMar() {
        return hMar;
    }

    public void sethMar(String hMar) {
        this.hMar = hMar;
    }

    public String gethMie() {
        return hMie;
    }

    public void sethMie(String hMie) {
        this.hMie = hMie;
    }

    public String gethJue() {
        return hJue;
    }

    public void sethJue(String hJue) {
        this.hJue = hJue;
    }

    public String gethVie() {
        return hVie;
    }

    public void sethVie(String hVie) {
        this.hVie = hVie;
    }

    public String gethSab() {
        return hSab;
    }

    public void sethSab(String hSab) {
        this.hSab = hSab;
    }

    public List<Inscrito_Grupos_MB> getlInscritos() {
        return lInscritos;
    }

    public void setlInscritos(List<Inscrito_Grupos_MB> lInscritos) {
        this.lInscritos = lInscritos;
    }
}
