/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bean.reportes;

/**
 *
 * @author Edgar
 */
public class Profesor_Profesor_MB {
    
    private final int id;
    private final String nombre;
    
    
    public Profesor_Profesor_MB(int id, String nombre){
        this.id     = id;
        this.nombre = nombre;
    }
    public int getId() {
        return id;
    }

   
    public String getNombre() {
        return nombre;
    }

   
    
    
}
