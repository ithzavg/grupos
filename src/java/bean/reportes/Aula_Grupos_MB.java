/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bean.reportes;

/**
 *
 * @author Edgar
 */
public class Aula_Grupos_MB {
     private final String nombre;
     private final String edificio;

    public Aula_Grupos_MB(String nombre, String edificio) {
        this.nombre = nombre;
        this.edificio = edificio;
    }

    public String getNombre() {
        return nombre;
    }

    public String getEdificio() {
        return edificio;
    }
    
    
}
