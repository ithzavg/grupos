/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bean.reportes;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Edgar
 */
public class Mapeo_Grupos_MB {

    private String genHeader;
    private List<Grupo_Grupos_MB> lunes;
    private List<Grupo_Grupos_MB> martes;
    private List<Grupo_Grupos_MB> miercoles;
    private List<Grupo_Grupos_MB> jueves;
    private List<Grupo_Grupos_MB> viernes;
    private List<Grupo_Grupos_MB> sabado;    
    private final int[] count = new int[6];
    private int horasGrupo;
    private int horasPlaza;
    private String leyenda;

    
    public String getGenHeader() {
        return genHeader;
    }

    public void setGenHeader(String genHeader) {
        this.genHeader = genHeader;
    }

    public List<Grupo_Grupos_MB> getLunes() {
        return lunes;
    }

    public void setLunes(List<Grupo_Grupos_MB> lunes) {
        lunes = splitHorario(lunes);
        count[0] = lunes.size();
        this.lunes = fillWithBlank(lunes);
    }

    public List<Grupo_Grupos_MB> getMartes() {
        return martes;
    }

    public void setMartes(List<Grupo_Grupos_MB> martes) {
        martes = splitHorario(martes);
        count[1] = martes.size();
        this.martes = fillWithBlank(martes);
    }

    public List<Grupo_Grupos_MB> getMiercoles() {
        return miercoles;
    }

    public void setMiercoles(List<Grupo_Grupos_MB> miercoles) {
        miercoles = splitHorario(miercoles);
        count[2] = miercoles.size();
        this.miercoles = fillWithBlank(miercoles);
    }

    public List<Grupo_Grupos_MB> getJueves() {
        return jueves;
    }

    public void setJueves(List<Grupo_Grupos_MB> jueves) {
        jueves = splitHorario(jueves);
        count[3] = jueves.size();
        this.jueves = fillWithBlank(jueves);
    }

    public List<Grupo_Grupos_MB> getViernes() {
        return viernes;
    }

    public void setViernes(List<Grupo_Grupos_MB> viernes) {
        viernes = splitHorario(viernes);
        count[4] = viernes.size();
        this.viernes = fillWithBlank(viernes);
    }

    public List<Grupo_Grupos_MB> getSabado() {
        return sabado;
    }

    public void setSabado(List<Grupo_Grupos_MB> sabado) {
        sabado = splitHorario(sabado);
        count[5] = sabado.size();
        this.sabado = fillWithBlank(sabado);
    }
    
    public int[] getCount(){
        return this.count;
    }
    private List<Grupo_Grupos_MB> splitHorario(List<Grupo_Grupos_MB> dia) {

        List<Grupo_Grupos_MB> d = new ArrayList<>();

        for (Grupo_Grupos_MB d1 : dia) {
            int ini = d1.getHor_ini();
            int fin = d1.getHor_fin();

            if (!(ini >= 21)) {
                if ((ini + 1) == fin) {
                    d.add(d1);
                } else {
                    for (int i = ini; (i + 1) <= fin; i++) {
                        d.add(new Grupo_Grupos_MB(d1.getId(), d1.getNombre(), d1.getProfAula(), i, (i + 1)));
                    }
                }
            }

        }
        return d;
    }

    private List<Grupo_Grupos_MB> fillWithBlank(List<Grupo_Grupos_MB> dia) {

        if (dia.size() > 0) {
            for (int hora = 7, i = 0; hora < 21; i++, hora++) {
                if (i < dia.size()) {
                    Grupo_Grupos_MB grupo = dia.get(i);
                    int ini = grupo.getHor_ini();
                    if (!(ini == hora)) {
                        dia.add(i, new Grupo_Grupos_MB(0, "", "", hora, (hora + 1)));
                    }
                } else {
                    dia.add(i, new Grupo_Grupos_MB(0, "", "", hora, (hora + 1)));
                }

            }
        } else {
            for (int hora = 7; hora < 21; hora++) {
                dia.add(new Grupo_Grupos_MB(0, "", "", hora, (hora + 1)));
            }
        }

        return dia;
    }

    public int getHorasGrupo() {
        return horasGrupo;
    }

    public void setHorasGrupo(int horasGrupo) {
        this.horasGrupo = horasGrupo;
    }

    public int getHorasPlaza() {
        return horasPlaza;
    }

    public void setHorasPlaza(int horasPlaza) {
        this.horasPlaza = horasPlaza;
    }

    public String getLeyenda() {
        return leyenda;
    }

    public void setLeyenda(String leyenda) {
        this.leyenda = leyenda;
    }

    

}
