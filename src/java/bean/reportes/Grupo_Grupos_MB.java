/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bean.reportes;

/**
 *
 * @author Edgar
 */
public class Grupo_Grupos_MB {

    private final int id;
    private final String nombre;
    private final String prof_aula;
    private final int hor_ini;
    private final int hor_fin;

    public Grupo_Grupos_MB(int id, String nombre, String prof_aula,int hor_ini, int hor_fin) {
        this.id = id;
        this.nombre = nombre;
        this.prof_aula = prof_aula;
        this.hor_ini = hor_ini;
        this.hor_fin = hor_fin;
    }

    public int getId() {
        return id;
    }

    public String getNombre() {
        return nombre;
    }

    public int getHor_ini() {
        return hor_ini;
    }

    public int getHor_fin() {
        return hor_fin;
    }
    
    public String getProfAula(){
        return prof_aula;
    }

}
