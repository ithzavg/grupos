/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bean.session;

/**
 *
 * @author 3lyyzZ
 */
public class UsuarioMB {
    private String nomUsuario; 
    private String passUsuario;
    private int academiaUs; 
    private int carreraUS; 
    private int tipoUs; 
    private int tipoPeriodoUs;
    private int periodo;
    private int año;
    private String nombreCarrera;
    private String nomAcademia;
    private int modalidad; //0--> a Presencial
                          //1--> a Distancia
    /**
     * @return the nomUsuario
     */
    public String getNomUsuario() {
        return nomUsuario;
    }

    public UsuarioMB() {
    }
//Agregar modalidad
    public UsuarioMB(String nomUsuario, String passUsuario, int academiaUs, int carreraUS, int tipoUs, int tipoPeriodoUs, String nombreCarrera, String nomAcademia) {
        this.nomUsuario = nomUsuario;
        this.passUsuario = passUsuario;        
        this.academiaUs = academiaUs;
        this.carreraUS = carreraUS;
        this.tipoUs = tipoUs;
        this.tipoPeriodoUs = tipoPeriodoUs;
        this.nombreCarrera = nombreCarrera;
        this.nomAcademia=nomAcademia;
    }

    public String getNombreCarrera() {
        return nombreCarrera;
    }

    public void setNombreCarrera(String nombreCarrera) {
        this.nombreCarrera = nombreCarrera;
    }

    /**
     * @param nomUsuario the nomUsuario to set
     */
    public void setNomUsuario(String nomUsuario) {
        this.nomUsuario = nomUsuario;
    }

    /**
     * @return the passUsuario
     */
    public String getPassUsuario() {
        return passUsuario;
    }

    /**
     * @param passUsuario the passUsuario to set
     */
    public void setPassUsuario(String passUsuario) {
        this.passUsuario = passUsuario;
    }

    /**
     * @return the academiaUs
     */
    public int getAcademiaUs() {
        return academiaUs;
    }

    /**
     * @param academiaUs the academiaUs to set
     */
    public void setAcademiaUs(int academiaUs) {
        this.academiaUs = academiaUs;
    }

    /**
     * @return the carreraUS
     */
    public int getCarreraUS() {
        return carreraUS;
    }

    /**
     * @param carreraUS the carreraUS to set
     */
    public void setCarreraUS(int carreraUS) {
        this.carreraUS = carreraUS;
    }

    /**
     * @return the tipoUs
     */
    public int getTipoUs() {
        return tipoUs;
    }

    /**
     * @param tipoUs the tipoUs to set
     */
    public void setTipoUs(int tipoUs) {
        this.tipoUs = tipoUs;
    }

    /**
     * @return the nomAcademia
     */
    public String getNomAcademia() {
        return nomAcademia;
    }

    /**
     * @param nomAcademia the nomAcademia to set
     */
    public void setNomAcademia(String nomAcademia) {
        this.nomAcademia = nomAcademia;
    }

    /**
     * @return the modalidad
     */
    public int getModalidad() {
        return modalidad;
    }

    /**
     * @param modalidad the modalidad to set
     */
    public void setModalidad(int modalidad) {
        this.modalidad = modalidad;
    }

    public int getTipoPeriodoUs() {
        return tipoPeriodoUs;
    }

    public void setTipoPeriodoUs(int tipoPeriodoUs) {
        this.tipoPeriodoUs = tipoPeriodoUs;
    }

    public int getPeriodo() {
        return periodo;
    }

    public void setPeriodo(int periodo) {
        this.periodo = periodo;
    }

    public int getAño() {
        return año;
    }

    public void setAño(int año) {
        this.año = año;
    }
    
    
    

}
