/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets.session;

import bean.general.GenericReturn;
import bean.session.UsuarioMB;
import dao.session.UserDAO;
import interfaces.strings.AppInfo;
import java.io.IOException;
import java.util.HashMap;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import mx.edu.ittoluca.ittlogin.commons.security.Decrypter;

/**
 *
 * @author Edgar
 */
public class SessionFilter implements Filter {
    

   private static final String EXPIRED = "/views/general/checkout.jsp";

    public SessionFilter() {
    }

    /**
     *
     * @param request The servlet request we are processing
     * @param response The servlet response we are creating
     * @param chain The filter chain we are processing
     *
     * @exception IOException if an input/output error occurs
     * @exception ServletException if a servlet error occurs
     */
    @Override
    public void doFilter(ServletRequest request, ServletResponse response,
            FilterChain chain)
            throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse res = (HttpServletResponse) response;
        String msj = "";
        String url = req.getServletPath();
        boolean redirect = false;

        if (url.equals("/modulogrupos.do")) {

            UsuarioMB user;
            String opcion = request.getParameter("opcion");

            if (opcion == null) {

                HttpSession session = req.getSession();
                user = (UsuarioMB) session.getAttribute("objUsuario");

                if (user == null) {

                    String token = request.getParameter("d");
                    if (token != null) {
                        GenericReturn<UsuarioMB> userGen = getUsPass(token);

                        if (userGen != null) {
                            if (userGen.getPaCodError() == 0) {
                                session.setAttribute("objUsuario", userGen.getRetorno());// lleva todos los atributos del usuario                        
                                session.setAttribute("usuario", userGen.getRetorno().getTipoUs());
                            } else {
                                msj = userGen.getPaDesError();
                                redirect = true;
                            }
                        } else {
                            msj = "Estimado usuario ocurrió un error al desencriptar el Token, "
                                    + "Intentelo de nuevo por favor. Si persisten las molestias consulte con su administrador";
                            redirect = true;
                        }
                    } else {
                        msj = "Ingreso ilegal a la aplicación, debe iniciar sesión para poder usar este módulo";
                        redirect = true;
                    }
                }

            } else {

                redirect = validaSesion(req, res);

            }
        } else {

            redirect = validaSesion(req, res);

        }

        if (redirect) {
            req.setAttribute("causa", msj);
            req.getRequestDispatcher(EXPIRED).forward(request, response);
        } else {
            chain.doFilter(req, res);
        }

    }

    /**
     * Destroy method for this filter
     */
    @Override
    public void destroy() {
    }

    /**
     * Init method for this filter
     *
     * @param filterConfig
     */
    @Override
    public void init(FilterConfig filterConfig) {

    }

    private GenericReturn<UsuarioMB> getUsPass(String token) {
        GenericReturn<UsuarioMB> userGen = null;
        Decrypter deCrypter = new Decrypter();
        HashMap<String, String> tokUserPass = deCrypter.decryptAndValidate(token, AppInfo.APP_NAME_1, AppInfo.APP_MOD_SES);
        if (tokUserPass != null) {
            
            userGen = UserDAO.getUsuarioGpos(tokUserPass.get("username"), tokUserPass.get("password"), AppInfo.APP_NAME_1, AppInfo.APP_MOD_SES);
            
            if (userGen != null) {
            
                if (userGen.getPaCodError() == 0) {
                
                    GenericReturn<String> acadGen = UserDAO.getAcademiaJefe(userGen.getRetorno(),  AppInfo.APP_NAME_1, AppInfo.APP_MOD_SES);
                    
                    if (acadGen != null) {
                    
                        if (acadGen.getPaCodError() == 0) {
                        
                            userGen.getRetorno().setNomAcademia(acadGen.getRetorno());
                        
                        } else {
                        
                            userGen.getRetorno().setNomAcademia(acadGen.getPaDesError());
                            
                        }
                        
                    }
                    
                }
                
            }
            
        }

        return userGen;
    }

    private boolean validaSesion(HttpServletRequest req, HttpServletResponse res) throws IOException {
        boolean valida = false;
        HttpSession session = req.getSession(false);

        if (session == null) {

            valida = true;

        } else {

            UsuarioMB user = (UsuarioMB) session.getAttribute("objUsuario");

            if (user == null) {
                valida = true;
            }

        }
        return valida;
    }
    
}
