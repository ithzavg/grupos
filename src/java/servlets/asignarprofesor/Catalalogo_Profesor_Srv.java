/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets.asignarprofesor;

import bean.asignarprofesor.Academia_Profesores_MB;
import bean.general.Carreras_Profesores_MB;
import bean.general.Especialidad_Grupos_MB;
import bean.general.GenericReturn;
import bean.general.Plan_Grupos_MB;
import bean.session.UsuarioMB;
import com.google.gson.Gson;
import dao.asignarprofesor.Catalogo_Profesores_DAO;
import interfaces.strings.AppInfo;
import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Edgar
 */
public class Catalalogo_Profesor_Srv extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        HttpSession session = request.getSession(false);
        UsuarioMB user = (UsuarioMB) session.getAttribute("objUsuario");
        int opcion = Integer.parseInt(request.getParameter("opcion"));

        switch (opcion) {
            case 1:
                getAcademias(request, response, user);
                break;
            case 2:
                getCarreras(request, response, user);
                break;
            case 3:
                getPlanes(request, response, user);
                break;
            case 4:
                getEspecialidades(request, response, user);
                break;
            default:
                throw new IllegalArgumentException("Opcion no valida para Catalogo_Profesor_Srv");

        }

    }

    private void getAcademias(HttpServletRequest request, HttpServletResponse response, UsuarioMB user) throws IOException {

        GenericReturn<List<Academia_Profesores_MB>> acadGen = Catalogo_Profesores_DAO.getAcademias(user, AppInfo.APP_NAME_2, AppInfo.APP_2_MOD_R);

        if (acadGen != null) {
            String gson = new Gson().toJson(acadGen);
            response.getWriter().write(gson);
        }
    }

    private void getCarreras(HttpServletRequest request, HttpServletResponse response, UsuarioMB user) throws IOException {

        int paAcademiaId = user.getAcademiaUs();
        int paPeriodo = Integer.parseInt(request.getParameter("paPeriodo"));
        int paAño = Integer.parseInt(request.getParameter("paAnio"));

        GenericReturn<List<Carreras_Profesores_MB>> carrGen = Catalogo_Profesores_DAO.getCarreras(paAcademiaId, paPeriodo, paAño, user,AppInfo.APP_NAME_2, AppInfo.APP_2_MOD_R);

        if (carrGen != null) {
            String gson = new Gson().toJson(carrGen);          
            response.getWriter().write(gson);
        }
    }

    private void getPlanes(HttpServletRequest request, HttpServletResponse response, UsuarioMB user) throws IOException {

        int paAcademiaId = user.getAcademiaUs();
        int paCarreraId = Integer.parseInt(request.getParameter("paCarreraId"));
        int paPeriodo = Integer.parseInt(request.getParameter("paPeriodo"));
        int paAño = Integer.parseInt(request.getParameter("paAnio"));

        GenericReturn<List<Plan_Grupos_MB>> planGen = Catalogo_Profesores_DAO.getPlanes(paAcademiaId, paPeriodo, paAño, paCarreraId, user,AppInfo.APP_NAME_2, AppInfo.APP_2_MOD_R);

        if (planGen != null) {
            String gson = new Gson().toJson(planGen);            
            response.getWriter().write(gson);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private void getEspecialidades(HttpServletRequest request, HttpServletResponse response, UsuarioMB user) throws IOException {
        int paPlanId = Integer.parseInt(request.getParameter("paPlanId"));
        int paPeriodo = Integer.parseInt(request.getParameter("paPeriodo"));
        int paAño = Integer.parseInt(request.getParameter("paAnio"));

        GenericReturn<List<Especialidad_Grupos_MB>> espGen = Catalogo_Profesores_DAO.getEspecialidades(paPeriodo, paAño, paPlanId, user,AppInfo.APP_NAME_2, AppInfo.APP_2_MOD_R);

        if (espGen != null) {
            String gson = new Gson().toJson(espGen);
            response.setContentType("application/json");
            response.setCharacterEncoding("UTF-8");
            response.getWriter().write(gson);
        } else {
            throw new NullPointerException("ERROR FATAL de getEspecialidades en Catalogo_Profesor_Srv");
        }
    }

}
