/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets.asignarprofesor;

import bean.asignarprofesor.Profesor_Profesores_MB;
import bean.general.GenericReturn;
import bean.session.UsuarioMB;
import com.google.gson.Gson;
import dao.asignarprofesor.Profesor_Profesores_DAO;
import interfaces.strings.AppInfo;
import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Edgar
 */
public class Profesor_Profesores_Srv extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession(false);
        UsuarioMB user = (UsuarioMB) session.getAttribute("objUsuario");
        
        int paPeriodo = Integer.parseInt(request.getParameter("paPeriodo"));
        int paAño = Integer.parseInt(request.getParameter("paAño"));
        int opcion = Integer.parseInt(request.getParameter("opcion"));
        GenericReturn<List<Profesor_Profesores_MB>> profGen = null;
        if (opcion == 1) {

            int paGrupoId = Integer.parseInt(request.getParameter("paGrupoId"));
            int paAcademiaId = Integer.parseInt(request.getParameter("paAcademiaId"));
            profGen = Profesor_Profesores_DAO.getProfesores(paAcademiaId, paGrupoId, paPeriodo, paAño, user, AppInfo.APP_NAME_2,AppInfo.APP_2_MOD_R);

        } else if (opcion == 2) {

            profGen = Profesor_Profesores_DAO.getProfesores(user.getAcademiaUs(), paPeriodo, paAño, user,AppInfo.APP_NAME_2, AppInfo.APP_2_MOD_R);

        }

        if (profGen != null) {
            String json = new Gson().toJson(profGen);
            response.setContentType("application/json");
            response.setCharacterEncoding("UTF-8");
            response.getWriter().write(json);
        }else{
            throw  new NullPointerException("ERROR FATAL: Ocurrio un error al obtener los profesores");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
