/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets.asignarprofesor;

import bean.general.GenericReturn;
import bean.session.UsuarioMB;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import dao.asignarprofesor.Profesor_Profesores_DAO;
import interfaces.strings.AppInfo;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Edgar
 */
public class Horario_Profesores_Srv extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        HttpSession session = request.getSession(false);
        int opcion = Integer.parseInt(request.getParameter("opcion"));// opcion==0-->Insertar/ opcion==1-->Consultar / opcion == 2 --> updateHoras Asignadas
        int paProfId = Integer.parseInt(request.getParameter("paProfId"));
        int paPeriodo = Integer.parseInt(request.getParameter("paPeriodo"));
        int paAño = Integer.parseInt(request.getParameter("paAño"));
        String lHorario = request.getParameter("lHorario");
        UsuarioMB user = (UsuarioMB) session.getAttribute("objUsuario");
        Gson g = new Gson();

        switch (opcion) {
            case 0:

                int paHorasAsign = Integer.parseInt(request.getParameter("paHorasAsign"));
                List<String[]> list = g.fromJson(lHorario, new TypeToken<List<String[]>>() {
                }.getType());

                if (list.get(0).length > 0 || list.get(1).length > 0 || list.get(2).length > 0 || list.get(3).length > 0 || list.get(4).length > 0 || list.get(5).length > 0) {//MOVER A SABADO

                    List<String[]> sorted = new ArrayList<>();

                    for (String[] ll : list) {
                        sorted.add(sortedHorarios(ll));
                    }

                    GenericReturn gen = Profesor_Profesores_DAO.guardarHorarios(paProfId, paPeriodo, paAño, paHorasAsign, sorted.get(0), sorted.get(1), sorted.get(2), sorted.get(3), sorted.get(4), sorted.get(5), user, AppInfo.APP_NAME_2, AppInfo.APP_2_MOD_HORARIOS_C);
                    if (gen != null) {
                        String json = g.toJson(gen);
                        response.getWriter().write(json);
                    }

                }else{
                    GenericReturn gen = Profesor_Profesores_DAO.guardarHorarios(paProfId, paPeriodo, paAño, paHorasAsign, list.get(0), list.get(1), list.get(2), list.get(3), list.get(4), list.get(5), user, AppInfo.APP_NAME_2, AppInfo.APP_2_MOD_HORARIOS_C);
                    if (gen != null) {
                        String json = g.toJson(gen);
                        response.getWriter().write(json);
                    }
                }

                break;
            case 1:

                GenericReturn<List<List<String>>> horaGen = Profesor_Profesores_DAO.getHorarioProfesor(paProfId, paPeriodo, paAño, user, AppInfo.APP_NAME_2, AppInfo.APP_2_MOD_HORARIOS_R);
                if (horaGen.getPaCodError() == 0) {
                    List<List<String>> lhorario = getHorarios(horaGen.getRetorno());
                    String json = g.toJson(lhorario);
                    response.getWriter().write(json);
                } 

                break;
            case 2:

                int paHorasAsig = Integer.parseInt(request.getParameter("paHorasAsign"));

                GenericReturn horGen = Profesor_Profesores_DAO.updateHorAsigProfesor(paHorasAsig, paProfId, paPeriodo, paAño, user, AppInfo.APP_NAME_2, AppInfo.APP_2_MOD_HORARIOS_U);
                if (horGen != null) {
                    String json2 = g.toJson(horGen);
                    response.getWriter().write(json2);
                }

                break;
        }

    }

    private static List<List<String>> getHorarios(List<List<String>> lhorarios) {
        List<List<String>> horarios = null;
        if (lhorarios != null && lhorarios.size() > 0) {
            horarios = new ArrayList<>();
            for (List<String> l : lhorarios) {
                List<String> ll = new ArrayList<>();
                for (String s : l) {
                    String[] sub = s.split("-");
                    Integer ini = Integer.valueOf(sub[0]);
                    Integer fin = Integer.valueOf(sub[1]);
                    if ((ini + 1) == fin) {
                        ll.add(s);
                    } else {
                        for (int i = ini; (i + 1) <= fin; i++) {
                            StringBuilder sb = new StringBuilder();
                            sb.append(i).append("-").append((i + 1));
                            ll.add(sb.toString());
                        }
                    }
                }
                horarios.add(ll);
            }
        }
        return horarios;
    }

    private static List<String> getSortedHorarios(List<String> lista) {
        for (int i = 0; i < lista.size(); i++) {
            if (i + 1 == lista.size()) {
                break;
            }
            String[] sub1 = lista.get(i).split("-");
            String[] sub2 = lista.get(i + 1).split("-");
            Integer ini1 = Integer.valueOf(sub1[0]);
            Integer ini2 = Integer.valueOf(sub2[0]);
            Integer fin1 = Integer.valueOf(sub1[1]);
            Integer fin2 = Integer.valueOf(sub2[1]);

            if (fin1.compareTo(ini2) == 0) {
                StringBuilder sb = new StringBuilder();
                sb.append(ini1).append("-").append(fin2);
                lista.set(i, sb.toString());
                lista.remove(i + 1);
                --i;
            }
        }
        return lista;
    }

    private static String[] sortedHorarios(String[] lista) {
        Map<Integer, String> map = new HashMap();
        for (String s : lista) {
            String[] sub = s.split("-");
            Integer key = Integer.valueOf(sub[0]);
            map.put(key, s);
        }
        map = new TreeMap<>(map);
        List<String> lis = new ArrayList<>(map.values());
        return getSortedHorarios(lis).toArray(new String[lis.size()]);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
