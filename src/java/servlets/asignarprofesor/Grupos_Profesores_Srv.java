/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets.asignarprofesor;

import bean.general.GenericReturn;
import bean.grupos.Grupo_Grupos_MB;
import bean.session.UsuarioMB;
import com.google.gson.Gson;
import dao.asignarprofesor.Profesor_Profesores_DAO;
import interfaces.strings.AppInfo;
import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Edgar
 */
public class Grupos_Profesores_Srv extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession(false);
        UsuarioMB user = (UsuarioMB) session.getAttribute("objUsuario");

        int paAcademiaId = user.getAcademiaUs();
        int paOpcionCatalogo = Integer.parseInt(request.getParameter("paOpcionCatalogo"));
        int paPlanId = Integer.parseInt(request.getParameter("paPlanId"));
        int paEspecId = Integer.parseInt(request.getParameter("paEspecId"));
        
        GenericReturn<List<Grupo_Grupos_MB>> gruposGen = Profesor_Profesores_DAO.getGruposJefDepto(paPlanId, paEspecId, paAcademiaId, paOpcionCatalogo, user,AppInfo.APP_NAME_2,AppInfo.APP_2_MOD_R_1);

        if (gruposGen != null) {
            String json = new Gson().toJson(gruposGen);
            response.setContentType("application/json");
            response.setCharacterEncoding("UTF-8");
            response.getWriter().write(json);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
