/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets.grupos;

import bean.general.GenericReturn;
import bean.grupos.Grupo_Grupos_MB;
import bean.session.UsuarioMB;
import com.google.gson.Gson;
import dao.grupos.DML_Grupos_DAO;
import interfaces.strings.AppInfo;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Edgar
 */
public class Actualizar_Grupos_Srv extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter printWriter = response.getWriter();
        HttpSession session = request.getSession(false);
        UsuarioMB user = (UsuarioMB) session.getAttribute("objUsuario");

        int paOpcion = Integer.parseInt(request.getParameter("paOpcion"));
        Grupo_Grupos_MB grupo = new Gson().fromJson(request.getParameter("grupoMB"), Grupo_Grupos_MB.class);

        if (grupo != null) {

            if (grupo.validarBloque()) {
                if (user.getModalidad() == 1) {
                    updateGpo(printWriter, grupo, user, paOpcion);
                } else {
                    if (grupo.validarHorario()) { 
                        updateGpo(printWriter, grupo, user, paOpcion);
                    } else {
                        printWriter.write("Necesita indicar al menos un horario");
                    }
                }
            } else {
                printWriter.write("Necesita indicar un bloque");
            }
        } else {
            printWriter.write("Sin grupos para actualizar");
        }

    }

// <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private void updateGpo(PrintWriter printWriter, Grupo_Grupos_MB grupo, UsuarioMB user, int paOpcion) {
        GenericReturn retorno = DML_Grupos_DAO.updateGrupos(grupo, paOpcion, user, AppInfo.APP_NAME_1, AppInfo.APP_MOD_U);

        int paCodError = retorno.getPaCodError();
        String paDesError = retorno.getPaDesError();

        if (paCodError == 0) {
            printWriter.write(String.valueOf(paCodError));
        } else {
            printWriter.write(paDesError);
        }
    }

}
