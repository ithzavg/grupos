/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets.grupos;

import bean.general.GenericReturn;
import bean.grupos.Grupo_Grupos_MB;
import bean.session.UsuarioMB;
import com.google.gson.Gson;
import dao.grupos.DML_Grupos_DAO;
import interfaces.strings.AppInfo;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Edgar
 */
public class Guardar_Grupos_Srv extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        PrintWriter printWriter = response.getWriter();
        HttpSession session = request.getSession(false);
        UsuarioMB user = (UsuarioMB) session.getAttribute("objUsuario");
        int paPlanId = Integer.parseInt(request.getParameter("paPlanId"));
        int paEspId = Integer.parseInt(request.getParameter("paEspId"));
        int paOpcion = Integer.parseInt(request.getParameter("paOpcion"));

        String jsonGrupo = request.getParameter("grupoMB");
        
        //Transformamos la cadena JSON que viene desde js a un Bean Grupo_Grupos_MB
        Grupo_Grupos_MB grupo = new Gson().fromJson(jsonGrupo, Grupo_Grupos_MB.class);

        if (grupo != null) {
            //Validamos que el grupo tenga horario y bloque asignado
            if (grupo.validarBloque()) {

                if (user.getModalidad() == 1) {
                    guardaGpo(grupo, printWriter, user, paPlanId, paEspId, paOpcion);
                } else {

                    if (grupo.validarHorario()) {
                        guardaGpo(grupo, printWriter, user, paPlanId, paEspId, paOpcion);
                    } else {
                        printWriter.write("Debe de asignar al menos un horario");
                    }
                }

            } else {
                printWriter.write("Debe de asignar un bloque");
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    public void guardaGpo(Grupo_Grupos_MB grupo, PrintWriter printWriter, UsuarioMB user, int paPlanId, int paEspId, int paOpcion) {

        if (grupo.validaIdCompartido()) {
            GenericReturn<Integer> retorno = DML_Grupos_DAO.insertGrupos(grupo, paPlanId, paEspId, paOpcion, user, AppInfo.APP_NAME_1, AppInfo.APP_MOD_C);//Obtenemos el ID del nuevo grupo creado                  
            int paCodError = retorno.getPaCodError();

            if (paCodError == 0) {
                printWriter.write(retorno.getRetorno().toString());
            } else {
                printWriter.write(retorno.getPaDesError());
            }
        } else {
            printWriter.write("Debe asignar un id");
        }
    }
}
