/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets.grupos;

import bean.general.GenericReturn;
import bean.session.UsuarioMB;
import dao.grupos.DML_Grupos_DAO;
import interfaces.strings.AppInfo;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Edgar
 */
public class Eliminar_Grupos_Srv extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession(false);
        int paGrupoId = Integer.parseInt(request.getParameter("paGrupoId"));
        int paPeriodo = Integer.parseInt(request.getParameter("paPeriodo"));
        int paAño = Integer.parseInt(request.getParameter("paAño"));        
        UsuarioMB user = (UsuarioMB) session.getAttribute("objUsuario");
        GenericReturn retorno = DML_Grupos_DAO.delGrupos(paGrupoId, paPeriodo, paAño, user,AppInfo.APP_NAME_1,AppInfo.APP_MOD_D);
        int paCodError = retorno.getPaCodError();
        String paDesError = retorno.getPaDesError();
        
        if (paCodError == 0) {
            response.getWriter().write(String.valueOf(paCodError));
        }else{
            response.getWriter().write(paDesError);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
