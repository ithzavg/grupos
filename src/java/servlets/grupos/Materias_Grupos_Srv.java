/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets.grupos;

import bean.general.GenericReturn;
import bean.grupos.Materia_Grupos_MB;
import bean.session.UsuarioMB;
import com.google.gson.Gson;
import dao.grupos.Catalogos_Grupos_DAO;
import interfaces.strings.AppInfo;
import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Edgar
 */
public class Materias_Grupos_Srv extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession(false);
        int paPlanId = Integer.parseInt(request.getParameter("paPlanId"));
        int paEspeciaId = Integer.parseInt(request.getParameter("paEspeciaId"));
        UsuarioMB user = (UsuarioMB) session.getAttribute("objUsuario");

        GenericReturn<List<Materia_Grupos_MB>> materiasGen = Catalogos_Grupos_DAO.getMaterias(paPlanId, paEspeciaId, user,AppInfo.APP_NAME_1,AppInfo.APP_MOD_R);

        if (materiasGen != null) {
            String json = new Gson().toJson(materiasGen);            
            response.setContentType("application/json");
            response.setCharacterEncoding("UTF-8");
            response.getWriter().write(json);
        }else{
            throw new NullPointerException("ERROR FATAL::[Class:Materias_Grupos_Srv]");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
