/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets.grupos;

import bean.general.Carreras_Profesores_MB;
import bean.general.GenericReturn;
import bean.grupos.Aula_Grupos_MB;
import bean.grupos.Grupo_Grupos_MB;
import bean.session.UsuarioMB;
import com.google.gson.Gson;
import dao.grupos.Catalogos_Grupos_DAO;
import interfaces.strings.AppInfo;
import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Edgar
 */
public class Catalogos_Grupos_Srv extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        HttpSession session = request.getSession(false);
        int opcion = Integer.parseInt(request.getParameter("opcion"));
        UsuarioMB user = (UsuarioMB) session.getAttribute("objUsuario");

        switch (opcion) {
            case 1:
                getGruposCreados(request, response, user);// Grupos Preparacion
                break;
            case 2:
                getGruposCompCreados(request, response, user); // Grupos Preparacion Compartidos 
                break;
            case 3:
                getCarreras(request, response, user);// Carreras
                break;
            case 4:
                getCatalogoGrupos(request, response, user);
                break;
            case 5:
                getAulas(request, response, user);
                break;            
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private void getGruposCreados(HttpServletRequest request, HttpServletResponse response, UsuarioMB user) throws IOException {
        int paOpcionCatalogo = Integer.parseInt(request.getParameter("paOpcionCatalogo"));
        int paPlanId = Integer.parseInt(request.getParameter("paPlanId"));
        int paEspecId = Integer.parseInt(request.getParameter("paEspecId"));
        
        GenericReturn<List<Grupo_Grupos_MB>> gruposGen = Catalogos_Grupos_DAO.getGrupos(paPlanId, paEspecId, paOpcionCatalogo, user,AppInfo.APP_NAME_1,AppInfo.APP_MOD_R);
        
        if (gruposGen != null) {
            String json = new Gson().toJson(gruposGen);
            response.getWriter().write(json);
        }else{
            throw new NullPointerException("ERROR FATAL::[Class:Catalogos_Grupos_Srv, Metodo:gruposPreparacion]");
        }
    }

    private void getGruposCompCreados(HttpServletRequest request, HttpServletResponse response, UsuarioMB user) throws IOException {
        int paPlanId = Integer.parseInt(request.getParameter("paPlanId"));
        int paEspecId = Integer.parseInt(request.getParameter("paEspecId"));
        int paOpcionCatalogo = Integer.parseInt(request.getParameter("paOpcionCatalogo"));
        
        GenericReturn<List<Grupo_Grupos_MB>> gruposGen = Catalogos_Grupos_DAO.getGruposComp(paPlanId, paEspecId, paOpcionCatalogo, user,AppInfo.APP_NAME_1,AppInfo.APP_MOD_R);
        if (gruposGen != null) {
            String json = new Gson().toJson(gruposGen);
            response.getWriter().write(json);
        }else{
            throw new NullPointerException("ERROR FATAL::[Class:Catalogos_Grupos_Srv, Metodo:gruposPrepCompartidos]");
        }
    }
    
    private void getCarreras(HttpServletRequest request, HttpServletResponse response, UsuarioMB user) throws IOException {
        GenericReturn<List<Carreras_Profesores_MB>> carrerasGen = Catalogos_Grupos_DAO.getCarreras(user,AppInfo.APP_NAME_1,AppInfo.APP_MOD_R);
        if (carrerasGen != null) {
            String json = new Gson().toJson(carrerasGen);
            response.getWriter().write(json);
        }else{
            throw new NullPointerException("ERROR FATAL::[Class:Catalogos_Grupos_Srv, Metodo:getCarreras]");
        }
    }

    private void getCatalogoGrupos(HttpServletRequest request, HttpServletResponse response, UsuarioMB user) throws IOException {
        int paCarreraId = Integer.parseInt(request.getParameter("paCarreraId"));
        GenericReturn<List<Grupo_Grupos_MB>> gruposGen = Catalogos_Grupos_DAO.getGruposByCarrera(paCarreraId, user.getTipoPeriodoUs(), user,AppInfo.APP_NAME_1,AppInfo.APP_MOD_R);
        
        if (gruposGen != null) {
            String json = new Gson().toJson(gruposGen);
            response.getWriter().write(json);
        }else{
            throw new NullPointerException("ERROR FATAL::[Class:Catalogos_Grupos_Srv, Metodo:getGruposPreparacion]");
        }
    }
   

    private void getAulas(HttpServletRequest request, HttpServletResponse response, UsuarioMB user) throws IOException {
        int paCarreraId = user.getCarreraUS();
        int paGrupoId = Integer.parseInt(request.getParameter("paGrupoId"));
        int paDia = Integer.parseInt(request.getParameter("paDia"));
        int paCapacidad = Integer.parseInt(request.getParameter("paCapacidad"));
        int paHor_Ini = Integer.parseInt(request.getParameter("paHor_Ini"));
        int paHor_Fin = Integer.parseInt(request.getParameter("paHor_Fin"));
        
        GenericReturn<List<Aula_Grupos_MB>> aulasGen = Catalogos_Grupos_DAO.getAulas(paGrupoId,paCarreraId, paDia, paCapacidad, paHor_Ini, paHor_Fin, user,AppInfo.APP_NAME_1,AppInfo.APP_MOD_R);
        
        if (aulasGen != null) {
            String gson = new Gson().toJson(aulasGen);
            response.setContentType("application/json");
            response.setCharacterEncoding("UTF-8");
            response.getWriter().write(gson);
        }else{
            throw new NullPointerException("ERROR FATAL::[Class:Catalogos_Grupos_Srv, Metodo:getAulas]");
        }
    }

   
}
