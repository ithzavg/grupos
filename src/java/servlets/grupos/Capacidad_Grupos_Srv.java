/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets.grupos;

import bean.general.GenericReturn;
import bean.grupos.Grupo_Grupos_MB;
import bean.session.UsuarioMB;
import com.google.gson.Gson;
import dao.grupos.CheckCapacidad_Grupos_DAO;
import interfaces.strings.AppInfo;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author 3lyyzZ
 * @updated by Edgar
 *
 */
public class Capacidad_Grupos_Srv extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        HttpSession session = request.getSession(false);
        PrintWriter out = response.getWriter();
        Grupo_Grupos_MB grupo = new Gson().fromJson(request.getParameter("Grupo"), Grupo_Grupos_MB.class);
        UsuarioMB user = (UsuarioMB) session.getAttribute("objUsuario");

        if (grupo.validarHorario()) {
            int Grupo;
            int tipo = Integer.parseInt(request.getParameter("tipo"));
            int opc = user.getTipoPeriodoUs();//periodo 1-actuales 2-preparacion 3-intersemestral
            if (tipo == 0) {//real
                Grupo = grupo.getGrupo_Id();
            } else {//Compartidos
                Grupo = grupo.getGrupoId_Real();
            }
            StringBuilder aulasProblema = new StringBuilder();
            int errorProblema = 0;
            String[] aulas = Aula(grupo);
            GenericReturn bean;
            for (String aula : aulas) {
                if (aula != null) {
                    bean = CheckCapacidad_Grupos_DAO.validaCapacidad(grupo, aula, user, AppInfo.APP_NAME_1, AppInfo.APP_MOD_CHK);
                    if (bean.getPaCodError() != 0) {
                        errorProblema += 1;
                        aulasProblema.append(bean.getPaDesError()).append("\n");
                    }
                }
            }
            if (errorProblema == 0) {
                out.write(String.valueOf(0));
            } else {
                out.write(aulasProblema.toString());
            }

        } else {
                out.write(String.valueOf(0));
            }

        }

        // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
        /**
         * Handles the HTTP <code>GET</code> method.
         *
         * @param request servlet request
         * @param response servlet response
         * @throws ServletException if a servlet-specific error occurs
         * @throws IOException if an I/O error occurs
         */
        @Override
        protected void doGet
        (HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
            processRequest(request, response);
        }

        /**
         * Handles the HTTP <code>POST</code> method.
         *
         * @param request servlet request
         * @param response servlet response
         * @throws ServletException if a servlet-specific error occurs
         * @throws IOException if an I/O error occurs
         */
        @Override
        protected void doPost
        (HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
            processRequest(request, response);
        }

        /**
         * Returns a short description of the servlet.
         *
         * @return a String containing servlet description
         */
        @Override
        public String getServletInfo
        
            () {
        return "Short description";
        }// </editor-fold>

    

    private String[] Aula(Grupo_Grupos_MB grupo) {
        String[] aulas = new String[6];
        String hor;
        for (int i = 0; i < aulas.length; i++) {
            switch (i) {
                case 0:
                    if (grupo.getH_LUN() == null) {
                        hor = null;
                    } else {
                        hor = grupo.getH_LUN();
                    }
                    break;
                case 1:
                    if (grupo.getH_MAR() == null) {
                        hor = null;
                    } else {
                        hor = grupo.getH_MAR();
                    }
                    break;
                case 2:
                    if (grupo.getH_MIE() == null) {
                        hor = null;
                    } else {
                        hor = grupo.getH_MIE();
                    }
                    break;
                case 3:
                    if (grupo.getH_JUE() == null) {
                        hor = null;
                    } else {
                        hor = grupo.getH_JUE();
                    }
                    break;
                case 4:
                    if (grupo.getH_VIE() == null) {
                        hor = null;
                    } else {
                        hor = grupo.getH_VIE();
                    }
                    //FALTA SABADO
                    break;
                case 5:
                    if (grupo.getH_SAB() == null) {
                        hor = null;
                    } else {
                        hor = grupo.getH_SAB();
                    }
                    break;
                default:
                    hor = null;
                    break;
            }
            if (hor != null && !"".equals(hor)) {
                String[] horario = hor.split("/");
                aulas[i] = horario[1];
            } else {
                aulas[i] = null;
            }
        }
        return aulas;
    }

}
