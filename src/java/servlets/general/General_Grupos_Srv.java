/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets.general;

import bean.general.Especialidad_Grupos_MB;
import bean.general.GenericReturn;
import bean.general.Plan_Grupos_MB;
import bean.session.UsuarioMB;
import com.google.gson.Gson;
import dao.general.General_Grupos_DAO;
import interfaces.strings.AppInfo;
import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Edgar
 */
public class General_Grupos_Srv extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        HttpSession session = request.getSession(false);
        String opcion = request.getParameter("opcion");
        long current = System.currentTimeMillis();
        if (opcion == null) {

            response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate"); // HTTP 1.1.
            response.setHeader("Pragma", "no-cache"); // HTTP 1.0.
            response.setHeader("Expires", "0"); // Proxies.

            request.setAttribute("current", current);
            int periodo = Integer.parseInt(request.getParameter("p"));
            UsuarioMB user = (UsuarioMB) session.getAttribute("objUsuario");

            switch (user.getTipoUs()) {
                case 0:
//                    user.setPeriodoUs(periodo);
                    user.setTipoPeriodoUs(periodo);
                    getPlanes(request, user);
                    getPeriodoAño(request, response, user);
                    request.setAttribute("opCat", (periodo + 1));
                    request.setAttribute("opPeriodo", periodo);
                    request.getRequestDispatcher("views/coordinador/gruposGeneral_view.jsp").forward(request, response);
                    break;
                case 1:
                    if (periodo != 100) {
//                        user.setPeriodoUs(periodo);
                        user.setTipoPeriodoUs(periodo);
                        getPlanes(request, user);
                        getPeriodoAño(request, response, user);
                        request.setAttribute("opCat", (periodo + 1));
                        request.setAttribute("opPeriodo", periodo);
                        request.setAttribute("vista", 0);
                        request.getRequestDispatcher("views/coordinador/gruposGeneral_view.jsp").forward(request, response);
                    } else {
                        request.setAttribute("vista", 1);
                        request.getRequestDispatcher("views/jefeDepartamento/profesores_view.jsp").forward(request, response);
                    }
                    break;
                default:

                    throw new IllegalArgumentException("Tipo de usuario incorrecto");

            }

        } else {

            int op = Integer.parseInt(opcion);
            UsuarioMB user = (UsuarioMB) session.getAttribute("objUsuario");

            switch (op) {
                case 1:
                    getEspecialidad(request, response, user);
                    break;
                case 2:
                    getPeriodoAño(request, response, user);
                    break;
                default:
                    throw new IllegalArgumentException("Opcion invalida en modulogrupos.do");

            }

        }
    }

    private void getEspecialidad(HttpServletRequest request, HttpServletResponse response, UsuarioMB user) throws IOException {
        int paPlanId = Integer.parseInt(request.getParameter("paPlanId"));
        GenericReturn<List<Especialidad_Grupos_MB>> especGen = General_Grupos_DAO.getEspecialidad(paPlanId, user, AppInfo.APP_NAME_1, AppInfo.APP_MOD_R);

        if (especGen != null) {
            String json = new Gson().toJson(especGen);
            response.setContentType("application/json");
            response.setCharacterEncoding("UTF-8");
            response.getWriter().write(json);
        } else {
            throw new NullPointerException("ERROR FATAL: Al obtener las especialidades de grupos [Class: General_Grupos_Srv,Method:getEspecialidad]");
        }
    }

    private void getPlanes(HttpServletRequest request, UsuarioMB user) throws ServletException, IOException {

        GenericReturn<List<Plan_Grupos_MB>> planGen = General_Grupos_DAO.getPlanes(user.getCarreraUS(), user, AppInfo.APP_NAME_1, AppInfo.APP_MOD_R);
        if (planGen.getPaCodError() == 0) {
            request.setAttribute("planes", planGen.getRetorno());
        } else {
            request.setAttribute("error", planGen.getPaDesError());
        }

    }

    private void getPeriodoAño(HttpServletRequest request, HttpServletResponse response, UsuarioMB user) throws ServletException, IOException {
        GenericReturn perAñoGen = new GenericReturn();
        if (user.getTipoUs() == 0) {
            getLeyendaCoor(perAñoGen, request, user);

        } else if (user.getTipoUs() == 1) {

            if ((user.getTipoPeriodoUs() == 1) || (user.getTipoPeriodoUs() == 2)||(user.getTipoPeriodoUs() == 3)) {
                getLeyendaCoor(perAñoGen, request, user);
            } else {
                int paOpcion = Integer.parseInt(request.getParameter("paOpcion"));
                perAñoGen = General_Grupos_DAO.getLeyendaPeriodo(paOpcion, user, "Asignar Profesor", "Obtener Periodo y Año");
                String gson = new Gson().toJson(perAñoGen);
                response.setContentType("application/json");
                response.setCharacterEncoding("UTF-8");                
                response.getWriter().write(gson);
            }
        }

    }

    private void getLeyendaCoor(GenericReturn perAñoGen, HttpServletRequest request, UsuarioMB user) {
        perAñoGen = General_Grupos_DAO.getLeyendaPeriodo(user.getTipoPeriodoUs(), user, AppInfo.APP_NAME_1, AppInfo.APP_MOD_R);
        if (perAñoGen.getPaCodError() == 0) {
            int periodo = (int) perAñoGen.getExtraParams().get("paPeriodo");
            int año = (int) perAñoGen.getExtraParams().get("paAño");
//            user.setPeriodo(periodo);
//            user.setAño(año);
            String paLeyenda = (String) perAñoGen.getExtraParams().get("paLeyenda");
            request.setAttribute("paPeriodoDes", paLeyenda);
            request.setAttribute("paAño", año);
            request.setAttribute("paPeriodo", periodo);
        } else {
            throw new RuntimeException("FATAL ERROR: getPeriodoAño en General_Grupos_Srv " + perAñoGen.getPaDesError());
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
