/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets.reportes;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import java.awt.Color;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author IthzaVG
 */
public class JefeAtiende_Grupos_Srv extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/pdf");
        crearPDF(response, getServletContext());

    }

    private void crearPDF(HttpServletResponse response, ServletContext c) throws IOException {

        try {
            Document document = new Document(PageSize.LETTER);
            PdfWriter.getInstance(document, response.getOutputStream());
            document.open();

            String url_header = "img/header.png";
            String absolute_url_head = c.getRealPath(url_header);
            Image header_img = Image.getInstance(absolute_url_head);
            header_img.scaleToFit(526, 800);
            header_img.setAlignment(Element.ALIGN_CENTER);

            Font chapterFont = FontFactory.getFont(FontFactory.HELVETICA, 14, Font.BOLD);
            Font paraFont = FontFactory.getFont(FontFactory.HELVETICA, 11, Font.BOLD);

            Chunk chunk = new Chunk("Subdirección Académica", chapterFont);
            Paragraph title = new Paragraph(chunk);
            title.setAlignment(Element.ALIGN_CENTER);

            Chunk chunkp = new Chunk("División de Estudios Profesionales", paraFont);
            Paragraph sub = new Paragraph(chunkp);
            sub.setAlignment(Element.ALIGN_CENTER);

            Paragraph acad = new Paragraph();
            acad.add(new Phrase("Relación de Grupos", paraFont));
            acad.setAlignment(Element.ALIGN_CENTER);

            Paragraph academia = new Paragraph();
            academia.add(new Phrase("Academia: ", paraFont));
            academia.setAlignment(Element.ALIGN_CENTER);

            Paragraph fecha = new Paragraph();
            fecha.add(new Phrase("Fecha:", paraFont));
            fecha.setAlignment(Element.ALIGN_LEFT);

            Paragraph hra = new Paragraph();
            hra.add(new Phrase("Hora:", paraFont));
            hra.setAlignment(Element.ALIGN_LEFT);

            Paragraph vacio = new Paragraph("    ", FontFactory.getFont("arial", 10, java.awt.Font.BOLD));
            vacio.setAlignment(Element.ALIGN_CENTER);

            document.add(header_img);
            document.add(title);
            document.add(sub);
            document.add(acad);
            document.add(academia);
            document.add(fecha);
            document.add(hra);
            document.add(vacio);
            renderData(document);
            document.add(vacio);
            renderList(document);
            document.close();

        } catch (DocumentException ex) {
            Logger.getLogger(JefeAtiende_Grupos_Srv.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void renderList(Document document) throws DocumentException {

        float[] columnWidths = {1,1,1,1,1,1,1,1,1,1,2,2};
        PdfPTable atentidos = new PdfPTable(columnWidths);
        atentidos.setWidthPercentage(100);

        Font headersFont = FontFactory.getFont(FontFactory.HELVETICA, 8, Font.BOLD);

        PdfPCell headers = new PdfPCell();
        headers.setBorder(PdfPCell.NO_BORDER);
        headers.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);

        String[] heads = {"Bloque", "Gpo", "H_Lun", "H_Mar","H_Mier","H_Juev","H_Vier",
        "Cap","Inscritos","Créditos", "Materia","Profesor"};

        for (String head : heads) {
            headers.setPhrase(new Phrase(head, headersFont));
            atentidos.addCell(headers);
        }

        document.add(atentidos);
    }
    
     public void renderData(Document document) throws DocumentException {

        float[] columnWidths = {1,2,2};
        PdfPTable datosCarrera = new PdfPTable(columnWidths);
        datosCarrera.setWidthPercentage(100);

        Font headersFont = FontFactory.getFont(FontFactory.HELVETICA, 11, Font.BOLD);

        PdfPCell headers = new PdfPCell();
        headers.setBorder(PdfPCell.NO_BORDER);
        headers.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);

        String[] heads = {"Carrera:","Plan:", "Espec.:"};

        for (String head : heads) {
            headers.setPhrase(new Phrase(head, headersFont));
            datosCarrera.addCell(headers);
        }

        document.add(datosCarrera);
    }

// <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
