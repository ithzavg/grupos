/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets.reportes;

import bean.general.GenericReturn;
import bean.reportes.Aula_Grupos_MB;
import bean.reportes.Grupo_Grupos_MB;
import bean.reportes.Mapeo_Grupos_MB;
import bean.reportes.Profesor_Profesor_MB;
import bean.session.UsuarioMB;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Font.FontFamily;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.GrayColor;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import dao.reportes.Mapeo_Grupos_DAO;
import interfaces.strings.AppInfo;
import java.io.IOException;
import java.util.List;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Edgar
 */
public class MapeoHorario_Grupos_Srv extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    int cols = 0;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("application/pdf");

        int op = Integer.parseInt(request.getParameter("op"));

        HttpSession session = request.getSession(false);

        UsuarioMB user = (UsuarioMB) session.getAttribute("objUsuario");

        crearSimpleOMultiplePDF(response, request, getServletContext(), user, op);
    }

    private void crearSimpleOMultiplePDF(HttpServletResponse response, HttpServletRequest request, ServletContext d, UsuarioMB user, int op) {

        try {

            Document document = new Document(PageSize.LETTER.rotate());
            document.setMargins(50, 50, 20, 10);
            PdfWriter.getInstance(document, response.getOutputStream());
            document.open();

            String url_head = "img/header.png";
            String absolute_url_head = d.getRealPath(url_head);

            Image head_img = Image.getInstance(absolute_url_head);
            head_img.scaleToFit(680, 1000);
            head_img.setAlignment(Element.ALIGN_CENTER);

            Font chapterFont = FontFactory.getFont(FontFactory.HELVETICA, 12, Font.BOLDITALIC);
            Font paragraphFont = FontFactory.getFont(FontFactory.HELVETICA, 10, Font.NORMAL);

            Chunk chunk = new Chunk("DIVISIÓN DE ESTUDIOS PROFESIONALES", chapterFont);
            

            Paragraph title = new Paragraph(chunk);
            title.setAlignment(Element.ALIGN_CENTER);

            Paragraph sub = new Paragraph(user.getNomAcademia(), paragraphFont);
            sub.setAlignment(Element.ALIGN_CENTER);

            Paragraph vacio = new Paragraph("    ", FontFactory.getFont("arial", 10, java.awt.Font.BOLD));
            vacio.setAlignment(Element.ALIGN_CENTER);

            switch (user.getTipoUs()) {
                case 0:
                    this.cols = 7;
                    switch (op) {
                        case 0:
                            String aula = request.getParameter("aula");

                            GenericReturn<Mapeo_Grupos_MB> mapeo = Mapeo_Grupos_DAO.getMapeoGrupos(aula, user, AppInfo.APP_NAME_1, AppInfo.APP_MOD_REP_H);

                            if (mapeo.getPaCodError() == 0) {

                                PdfPTable table1 = getTable(mapeo.getRetorno());
                                document.add(head_img);
                                document.add(title);
                                document.add(sub);
                                document.add(vacio);
                                document.add(table1);
                                renderFechaLeyenda(document, mapeo.getRetorno());
                            } else {

                                Chunk error = new Chunk(mapeo.getPaDesError(), chapterFont);
                                document.add(error);

                            }

                            break;
                        case 3:

                            GenericReturn<List<Aula_Grupos_MB>> bean = Mapeo_Grupos_DAO.getAulasMapeo(user, AppInfo.APP_NAME_1, AppInfo.APP_MOD_REP_H);

                            if (bean.getPaCodError() == 0) {

                                List<Aula_Grupos_MB> aulas = bean.getRetorno();

                                for (Aula_Grupos_MB aul : aulas) {

                                    GenericReturn<Mapeo_Grupos_MB> map = Mapeo_Grupos_DAO.getMapeoGrupos(aul.getNombre(), user, AppInfo.APP_NAME_1, AppInfo.APP_MOD_REP_H);

                                    if (map.getPaCodError() == 0) {

                                        PdfPTable table2 = getTable(map.getRetorno());
                                        document.add(head_img);
                                        document.add(title);
                                        document.add(sub);
                                        document.add(vacio);
                                        document.add(table2);
                                        renderFechaLeyenda(document, map.getRetorno());
                                    } else {

                                        Chunk error = new Chunk(map.getPaDesError(), chapterFont);
                                        document.add(error);

                                    }
                                }
                            } else {

                                Chunk error = new Chunk(bean.getPaDesError(), chapterFont);
                                document.add(error);

                            }

                            break;
                        default:
                            Chunk error = new Chunk("OPCION INVALIDA INTENTE DE NUEVO", chapterFont);
                            document.add(error);
                            break;

                    }
                    break;
                case 1:
                    this.cols = 8;
                    switch (op) {
                         case 0:
                            String aula = request.getParameter("aula");

                            GenericReturn<Mapeo_Grupos_MB> mapeoJ = Mapeo_Grupos_DAO.getMapeoGrupos(aula, user, AppInfo.APP_NAME_1, AppInfo.APP_MOD_REP_H);

                            if (mapeoJ.getPaCodError() == 0) {

                                PdfPTable table1 = getTable(mapeoJ.getRetorno());
                                document.add(head_img);
                                document.add(title);
                                document.add(sub);
                                document.add(vacio);
                                document.add(table1);
                                renderFechaLeyenda(document, mapeoJ.getRetorno());
                            } else {

                                Chunk error = new Chunk(mapeoJ.getPaDesError(), chapterFont);
                                document.add(error);

                            }

                            break;
                        case 1:
                            int prof = Integer.parseInt(request.getParameter("id"));
                            int opC = Integer.parseInt(request.getParameter("opC"));

                            GenericReturn<Mapeo_Grupos_MB> mapeo = Mapeo_Grupos_DAO.getMapeoGrupos(prof, opC, user, AppInfo.APP_NAME_2, AppInfo.APP_2_MOD_REP_H);

                            if (mapeo.getPaCodError() == 0) {

                                PdfPTable table1 = getTable(mapeo.getRetorno());
                                document.add(head_img);
                                document.add(title);
                                document.add(sub);
                                renderInfoProf(document, mapeo.getRetorno());
                                document.add(vacio);
                                document.add(table1);

                            } else {

                                Chunk error = new Chunk(mapeo.getPaDesError(), chapterFont);
                                document.add(error);

                            }

                            break;
                        case 2:

                            int opT = Integer.parseInt(request.getParameter("opC"));
                            GenericReturn<List<Profesor_Profesor_MB>> bean = Mapeo_Grupos_DAO.getProfesorMapeo(user, opT, AppInfo.APP_NAME_2, AppInfo.APP_2_MOD_REP_H);
                            if (bean.getPaCodError() == 0) {

                                List<Profesor_Profesor_MB> profs = bean.getRetorno();

                                for (Profesor_Profesor_MB prof1 : profs) {

                                    GenericReturn<Mapeo_Grupos_MB> map = Mapeo_Grupos_DAO.getMapeoGrupos(prof1.getId(), opT, user, AppInfo.APP_NAME_2, AppInfo.APP_2_MOD_REP_H);

                                    if (map.getPaCodError() == 0) {

                                        PdfPTable table2 = getTable(map.getRetorno());
                                        document.add(head_img);
                                        document.add(title);
                                        document.add(sub);
                                        renderInfoProf(document, map.getRetorno());
                                        document.add(vacio);
                                        document.add(table2);

                                    } else {

                                        Chunk error = new Chunk(map.getPaDesError(), chapterFont);
                                        document.add(error);

                                    }
                                }
                            } else {

                                Chunk error = new Chunk(bean.getPaDesError(), chapterFont);
                                document.add(error);

                            }

                            break;
                            case 3:

                            GenericReturn<List<Aula_Grupos_MB>> beanJ = Mapeo_Grupos_DAO.getAulasMapeo(user, AppInfo.APP_NAME_1, AppInfo.APP_MOD_REP_H);

                            if (beanJ.getPaCodError() == 0) {

                                List<Aula_Grupos_MB> aulas = beanJ.getRetorno();

                                for (Aula_Grupos_MB aul : aulas) {

                                    GenericReturn<Mapeo_Grupos_MB> map = Mapeo_Grupos_DAO.getMapeoGrupos(aul.getNombre(), user, AppInfo.APP_NAME_1, AppInfo.APP_MOD_REP_H);

                                    if (map.getPaCodError() == 0) {

                                        PdfPTable table2 = getTable(map.getRetorno());
                                        document.add(head_img);
                                        document.add(title);
                                        document.add(sub);
                                        document.add(vacio);
                                        document.add(table2);
                                        renderFechaLeyenda(document, map.getRetorno());
                                    } else {

                                        Chunk error = new Chunk(map.getPaDesError(), chapterFont);
                                        document.add(error);

                                    }
                                }
                            } else {

                                Chunk error = new Chunk(beanJ.getPaDesError(), chapterFont);
                                document.add(error);

                            }

                            break;
                        default:
                            Chunk error = new Chunk("OPCION INVALIDA INTENTE DE NUEVO", chapterFont);
                            document.add(error);
                            break;

                    }
                    break;

            }

            document.addTitle("Mapeo Grupos ");
            document.close();

        } catch (DocumentException | IOException ex) {

        }
    }

    private PdfPTable getTable(Mapeo_Grupos_MB mapeo) {
        int size = (cols == 8) ? 25 : 29;
        PdfPTable table = new PdfPTable(7);
        table.setTotalWidth((cols == 8) ? 690 : 650);

        table.setLockedWidth(true);

        CustomPDFCell cell = new CustomPDFCell(mapeo.getGenHeader(), false, size, true);

        table.addCell(cell);

        for (int i = 1; i < 7; i++) {

            CustomPDFCell cell2 = new CustomPDFCell(getDay(i), false, size, true);
            table.addCell(cell2);

        }

        int idxLun = 0, idxMar = 0, idxMie = 0, idxJue = 0, idxVie = 0, idxSab = 0;

        for (int hor_ini = 7; hor_ini < 21; hor_ini += 2) {

            CustomPDFCell cell2 = new CustomPDFCell(hor_ini + ":00 - " + (hor_ini + 2) + ":00", true, 0, true);
            table.addCell(cell2);

            for (int j = 0; j < 2; idxLun++, idxMar++, idxMie++, idxJue++, idxVie++, idxSab++, j++) {

                idxLun = setColumnCell(mapeo.getLunes(), idxLun, hor_ini, table);

                idxMar = setColumnCell(mapeo.getMartes(), idxMar, hor_ini, table);

                idxMie = setColumnCell(mapeo.getMiercoles(), idxMie, hor_ini, table);

                idxJue = setColumnCell(mapeo.getJueves(), idxJue, hor_ini, table);

                idxVie = setColumnCell(mapeo.getViernes(), idxVie, hor_ini, table);

                idxSab = setColumnCell(mapeo.getSabado(), idxSab, hor_ini, table);
                               
            }
            
//            PdfPCell p = new PdfPCell(new Phrase(""));
//            p.setRowspan(2);
//            table.addCell(p);

        }

        if (cols == 8) {
            int suma = 0;
            CustomPDFCell total = new CustomPDFCell("TOTAL HRS", true, 25, true);
            table.addCell(total);
            for (int t : mapeo.getCount()) {
                CustomPDFCell totalb = new CustomPDFCell(String.valueOf(t), true, 25, true);
                table.addCell(totalb);
                suma += t;
            }
            CustomPDFCell totalrb = new CustomPDFCell(String.valueOf(suma), true, 25, true);
            table.addCell(totalrb);
        }
        return table;
    }

    private void setCells(List<Grupo_Grupos_MB> gen, int i, PdfPTable table, int hor_ini) {
        int size = (cols == 8) ? 50 : 58;
        CustomPDFCell cell;
        if ((i + 1) < gen.size()) {

            if (gen.get(i).getId() == gen.get(i + 1).getId()
                    && gen.get(i + 1).getHor_ini() < (hor_ini + 2)
                    && gen.get(i + 1).getHor_fin() > hor_ini) {

                cell = new CustomPDFCell(gen.get(i).getId(), gen.get(i).getNombre(), gen.get(i).getProfAula(), true, size);

                table.addCell(cell);

                gen.remove(i + 1);

            } else {

                cell = new CustomPDFCell(gen.get(i).getId(), gen.get(i).getNombre(), gen.get(i).getProfAula(), false, size / 2);
                table.addCell(cell);

            }

        } else {

            if (i < gen.size()) {

                cell = new CustomPDFCell(gen.get(i).getId(), gen.get(i).getNombre(), gen.get(i).getProfAula(), false, size / 2);
                table.addCell(cell);

            }

        }
    }

    private String getDay(int n) {
        String dia = "noDay";

        switch (n) {
            case 1:
                dia = "LUNES";
                break;
            case 2:
                dia = "MARTES";
                break;
            case 3:
                dia = "MIERCOLES";
                break;
            case 4:
                dia = "JUEVES";
                break;
            case 5:
                dia = "VIERNES";
                break;
            case 6:
                dia = "SABADO";
                break;
            case 7:
                dia = "TOTAL HRS";
                break;

        }

        return dia;
    }

    private int setColumnCell(List<Grupo_Grupos_MB> dia, int index, int hor_ini, PdfPTable table) {
        if (index < dia.size()) {
            if (dia.get(index).getHor_ini() < (hor_ini + 2) && dia.get(index).getHor_fin() > hor_ini) {
                setCells(dia, index, table, hor_ini);
            } else {
                index--;
            }

        }
        return index;
    }

    private void renderInfoProf(Document doc, Mapeo_Grupos_MB mapeo) throws DocumentException {
        float[] columnWidths = {5, 5};
        PdfPTable container = new PdfPTable(columnWidths);
        container.setWidthPercentage(100);

        Font font = FontFactory.getFont(FontFactory.HELVETICA, 8, Font.BOLD);
        Font font1 = FontFactory.getFont(FontFactory.HELVETICA, 8, Font.NORMAL);

        String[][] chars = {{"NOMBRE: ", mapeo.getGenHeader()},
        {"HRS PLAZA: ", String.valueOf(mapeo.getHorasPlaza())}};

        for (String[] strings : chars) {
            PdfPCell cell1 = new PdfPCell();
            cell1.setBorder(PdfPCell.NO_BORDER);
            Paragraph p = new Paragraph();
            p.add(new Phrase(strings[0], font1));
            p.add(new Phrase(strings[1], font));

            cell1.addElement(p);
            container.addCell(cell1);
        }

        PdfPCell cell2 = new PdfPCell();
        cell2.setColspan(2);
        cell2.setBorder(PdfPCell.NO_BORDER);
        Paragraph p = new Paragraph();
        p.add(new Phrase("HRS FRENTE A GRUPO: ", font1));
        p.add(new Phrase(String.valueOf(mapeo.getHorasGrupo()), font));

        cell2.addElement(p);

        container.addCell(cell2);

        doc.add(container);
    }
    
    private void renderFechaLeyenda(Document document, Mapeo_Grupos_MB mapeo)throws DocumentException{
        
        Paragraph p = new Paragraph(new Chunk(mapeo.getLeyenda(),FontFactory.getFont(FontFactory.HELVETICA,8,Font.BOLD)));
        p.setAlignment(Element.ALIGN_RIGHT);
        document.add(p);
    }

    private class CustomPDFCell extends PdfPCell {

        public CustomPDFCell(String content, boolean rowSpan, int h, boolean header) {

            Font f = new Font(FontFamily.HELVETICA, 8, Font.BOLD, GrayColor.BLACK);

            if (header) {
                this.setPhrase(new Phrase(content, f));
            } else {
                this.setPhrase(new Phrase(content));
            }

            this.setVerticalAlignment(Element.ALIGN_MIDDLE);
            this.setHorizontalAlignment(Element.ALIGN_CENTER);

            if (h > 0) {
                this.setFixedHeight(h);
            }

            if (rowSpan) {
                this.setRowspan(2);
            }
        }

        //Constructor para grupos
        public CustomPDFCell(int id, String nombre, String profesor, boolean rowSpan, int h) {

            Font colID = FontFactory.getFont("Courier", 6, BaseColor.RED);
            Font colGr = FontFactory.getFont("Helvetica", 8, BaseColor.BLACK);
            Font colPr = FontFactory.getFont("Courier", 6, BaseColor.BLUE);

            String idS = (id > 0) ? String.valueOf(id).concat("\n") : "";
            Chunk chID = new Chunk(idS, colID);
            Chunk chGr = new Chunk(nombre.concat("\n"), colGr);
            Chunk chPr = new Chunk(profesor, colPr);

            Phrase p = new Phrase();
            p.add(chID);
            p.add(chGr);
            p.add(chPr);

            this.setPhrase(p);
            this.setVerticalAlignment(Element.ALIGN_MIDDLE);
            this.setHorizontalAlignment(Element.ALIGN_CENTER);

            if (h > 0) {
                this.setFixedHeight(h);
            }

            if (rowSpan) {
                this.setRowspan(2);
            }

        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
