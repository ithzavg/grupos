/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets.reportes;

import bean.general.GenericReturn;
import bean.reportes.Inscrito_Grupos_MB;
import bean.reportes.Inscritos_Grupos_MB;
import bean.session.UsuarioMB;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.draw.VerticalPositionMark;
import dao.reportes.Inscritos_Grupos_DAO;
import interfaces.strings.AppInfo;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Edgar
 */
public class ListaInscritos_Grupos_Srv extends HttpServlet {

    private final Font fontNormal = FontFactory.getFont(FontFactory.HELVETICA, 10, Font.NORMAL);
    private final Font fontHeaders = FontFactory.getFont(FontFactory.HELVETICA, 10, Font.BOLD);

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/pdf");
        int grupoId = Integer.parseInt(request.getParameter("grupoid"));
        int opLis = Integer.parseInt(request.getParameter("oplis"));
        HttpSession session = request.getSession(false);
        UsuarioMB user = (UsuarioMB) session.getAttribute("objUsuario");

        System.out.println("" + grupoId);
        System.out.println("" + opLis);
        createPDF(grupoId, user, opLis, response, getServletContext());
    }

    private void createPDF(int grupoId, UsuarioMB user, int opLis, HttpServletResponse response, ServletContext d) {
        try {

            Document document = new Document(PageSize.LETTER);
            PdfWriter.getInstance(document, response.getOutputStream());
            document.open();

            String url_head = "img/header.png";
            String absolute_url_head = d.getRealPath(url_head);

            Image head_img = Image.getInstance(absolute_url_head);
            head_img.scaleToFit(526, 800);
            head_img.setAlignment(Element.ALIGN_CENTER);

            Font chapterFont = FontFactory.getFont(FontFactory.HELVETICA, 12, Font.BOLD);

            Chunk chunk = new Chunk("Lista de Inscritos", chapterFont);

            Paragraph title = new Paragraph(chunk);
            title.setAlignment(Element.ALIGN_CENTER);

            Paragraph vacio = new Paragraph("    ", FontFactory.getFont("arial", 10, java.awt.Font.BOLD));
            vacio.setAlignment(Element.ALIGN_CENTER);

            GenericReturn<Inscritos_Grupos_MB> bean = Inscritos_Grupos_DAO.getInfoInscritos(grupoId, opLis, user, AppInfo.APP_NAME_1, AppInfo.APP_MOD_REP_I);
            document.add(head_img);
            document.add(title);
            if (bean != null) {
                if (bean.getPaCodError() == 0) {
                    renderFechayHora(document);
                    renderHeaders(document, bean.getRetorno());
                    document.add(vacio);
                    renderTable(document, bean.getRetorno().getlInscritos(), opLis);
                } else {
                    Chunk errorCh = new Chunk(bean.getPaDesError(), chapterFont);
                    Paragraph erroTl = new Paragraph(errorCh);
                    title.setAlignment(Element.ALIGN_CENTER);
                    document.add(erroTl);
                }
            }else{
                throw  new NullPointerException("Ocurrio un error fatal en {class: ListaInscritos_Grupos_Srv,method:createPDF}");
            }

            document.close();

        } catch (DocumentException | IOException ex) {

        }
    }

    private String getCurrentlyDate(boolean justHour) {
        SimpleDateFormat fecha = new SimpleDateFormat("dd-MMMM-yyyy");
        SimpleDateFormat hora = new SimpleDateFormat("kk:mm:ss");
        return (justHour) ? hora.format(new Date()) : fecha.format(new Date());
    }

    private void renderTable(Document document, List<Inscrito_Grupos_MB> inscritos, int opLis) throws DocumentException {
        float[] columnWidths = {1, 5, 1};
        PdfPTable inscritosT = new PdfPTable(columnWidths);
        inscritosT.setWidthPercentage(100);

        Font headersFont = FontFactory.getFont(FontFactory.HELVETICA, 10, Font.BOLD);

        PdfPCell headers = new PdfPCell();
        headers.setBackgroundColor(BaseColor.GRAY);
        headers.setBorder(PdfPCell.NO_BORDER);
        headers.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);

        String[] heads = {"No. Ctrl", "Nombre", (opLis == 1) ? "Fecha Inscrito" : "Grupo"};

        for (String head : heads) {
            headers.setPhrase(new Phrase(head, headersFont));
            inscritosT.addCell(headers);
        }

        switch (opLis) {
            case 1:
                for (Inscrito_Grupos_MB inscrito : inscritos) {
                    inscritosT.addCell(new CustomCell(String.valueOf(inscrito.getNoCtl()), PdfPCell.ALIGN_CENTER));
                    inscritosT.addCell(new CustomCell(inscrito.getNombre(), PdfPCell.ALIGN_LEFT));
                    inscritosT.addCell(new CustomCell(inscrito.getFechaOGrupo(), PdfPCell.ALIGN_CENTER));
                }
                break;
            case 2:
                for (Inscrito_Grupos_MB inscrito : inscritos) {
                    inscritosT.addCell(new CustomCell(String.valueOf(inscrito.getNoCtl()), PdfPCell.ALIGN_CENTER));
                    inscritosT.addCell(new CustomCell(inscrito.getNombre(), PdfPCell.ALIGN_LEFT));
                    inscritosT.addCell(new CustomCell(inscrito.getFechaOGrupo(), PdfPCell.ALIGN_CENTER));
                }
                break;
            default:
                PdfPCell errorcell = new PdfPCell(new Phrase("Opcion Incorrecta, consulte con su administrador"));
                errorcell.setColspan(3);
                errorcell.setBorder(PdfPCell.NO_BORDER);
                errorcell.setPadding(5);
                inscritosT.addCell(errorcell);
                break;
        }

        PdfPCell count = new PdfPCell(new Phrase("Total:  " + inscritos.size(), headersFont));
        count.setColspan(3);
        count.setBorder(PdfPCell.NO_BORDER);
        count.setPaddingTop(5);

        inscritosT.addCell(count);

        document.add(inscritosT);
    }
   
    private void renderFechayHora(Document document) throws DocumentException {
        Font date = FontFactory.getFont(FontFactory.HELVETICA, 8, Font.BOLD);
        Chunk fecha = new Chunk("Fecha: ", date);
        Chunk hora = new Chunk("Hora: ", date);
        Paragraph p = new Paragraph();

        p.setFont(FontFactory.getFont(FontFactory.HELVETICA, 8, Font.NORMAL));
        p.add(fecha);
        p.add(getCurrentlyDate(false).concat("      "));
        p.add(hora);
        p.add(getCurrentlyDate(true).concat(" hrs"));
        p.setSpacingAfter(5);
        document.add(p);
    }

    private void renderHeaders(Document document, Inscritos_Grupos_MB inscritos) throws DocumentException {
        float[] columnWidths = {5, 2};
        PdfPTable container = new PdfPTable(columnWidths);
        container.setWidthPercentage(100);

        PdfPCell primaryCell = new PdfPCell();
        primaryCell.setBorder(PdfPCell.TOP);
        primaryCell.setPaddingRight(50);

        String[][] dataHeads = {{"Carrera: ", inscritos.getCarrera(), "Periodo: ", inscritos.getPeriodo()}, {"Materia: ", inscritos.getMateria(), "Plan: ", String.valueOf(inscritos.getPlan())}, {"Profesor: ", inscritos.getProfesor(), "Tarjeta: ", String.valueOf(inscritos.getTarjeta())}, {"Grupo: ", String.valueOf(inscritos.getGrupo()), "", ""}
        };

        for (String[] dataHead : dataHeads) {
            CustomPara line = new CustomPara(dataHead[0], dataHead[1], dataHead[2], dataHead[3]);
            primaryCell.addElement(line);
        }

        float[] columnWidths2 = {1, 4};
        PdfPTable horario = new PdfPTable(columnWidths2);

        PdfPCell dia = new PdfPCell();
        PdfPCell hor = new PdfPCell();
        String[] dias = {"Lun", "Mar", "Mie", "Jue", "Vie", "Sab"};
        String[] horas = {inscritos.gethLun(), inscritos.gethMar(), inscritos.gethMie(), inscritos.gethJue(), inscritos.gethVie(), inscritos.gethSab()};

        dia.setBorder(PdfPCell.NO_BORDER);

        for (int i = 0; i < dias.length; i++) {
            dia.setPhrase(new Phrase(dias[i], fontHeaders));
            hor.setPhrase(new Phrase(horas[i], fontNormal));
            horario.addCell(dia);
            horario.addCell(hor);
        }

        PdfPCell secondCell = new PdfPCell(horario);
        secondCell.setBorder(PdfPCell.TOP);
        secondCell.setPaddingTop(5);
        secondCell.setPaddingLeft(20);

        container.addCell(primaryCell);
        container.addCell(secondCell);

        document.add(container);

    }

    private class CustomCell extends PdfPCell {

        private final Font font = FontFactory.getFont(FontFactory.HELVETICA, 9, Font.NORMAL);

        public CustomCell(String content, int align) {
            this.setHorizontalAlignment(align);
            this.setPaddingTop(3);
            this.setPhrase(new Phrase(content, font));
            this.setBorder(PdfPCell.NO_BORDER);
        }
    }

    private class CustomPara extends Paragraph {

        Chunk glue = new Chunk(new VerticalPositionMark());

        public CustomPara(String s1, String s2, String s3, String s4) {
            Chunk c1 = new Chunk(s2, fontHeaders);
            Chunk c2 = new Chunk(s4, fontHeaders);
            this.setFont(fontNormal);
            this.add(s1);
            this.add(c1);
            this.add(glue);
            this.add(s3);
            this.add(c2);
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
