/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets.prestamoaula;

import bean.general.GenericReturn;
import bean.prestamoaula.Prestamo_PA_MB;
import bean.session.UsuarioMB;
import com.google.gson.Gson;
import dao.prestamoaula.PrestamoAula_PA_DAO;
import interfaces.strings.AppInfo;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author IthzaVG
 */
@WebServlet(name = "GuardarPrestamo_PA_Srv", urlPatterns = {"/GuardarPrestamo_PA_Srv"})
public class GuardarPrestamo_PA_Srv extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException {
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        HttpSession session = request.getSession(false);
        UsuarioMB user = (UsuarioMB) session.getAttribute("objUsuario");
        String jsonAula = request.getParameter("prestamo");

        Prestamo_PA_MB aulap = new Gson().fromJson(jsonAula, Prestamo_PA_MB.class);

        GenericReturn retorno = PrestamoAula_PA_DAO.setPrestamo(aulap, user, AppInfo.APP_NAME_1, AppInfo.APP_MOD_PA);

        if (retorno != null) 
        {
            String json = new Gson().toJson(retorno);
            response.getWriter().write(json);
            
        }
    }

    //Si regresa necesita parametros y si no solo pacoderror y el padescerror
    //Si el procedimiento regresa una lista etc se utilizan listas de lo contrario no necesita regresar nada
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(GuardarPrestamo_PA_Srv.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(GuardarPrestamo_PA_Srv.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
