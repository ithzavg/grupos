/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets.prestamoaula;

import bean.general.GenericReturn;
import bean.prestamoaula.PrestamoAula_PA_MB;
import bean.prestamoaula.Prestamo_PA_MB;
import bean.session.UsuarioMB;
import dao.prestamoaula.PrestamoAula_PA_DAO;
import interfaces.strings.AppInfo;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author IthzaVG
 */
public class EliminaPrestamo_PA_Srv extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        HttpSession session = request.getSession(false);
        UsuarioMB user = (UsuarioMB) session.getAttribute("objUsuario");
        PrestamoAula_PA_MB a = null;
        String paAula = request.getParameter("paAula");
        String paEdificio = request.getParameter("paEdificio");
        String paHorarioAula = request.getParameter("paHorarioAula");
        int paDiaAula = Integer.parseInt(request.getParameter("paDia"));
        int paCarrera = Integer.parseInt(request.getParameter("paCarreraProp"));
        int paCarreraPrestamo = Integer.parseInt(request.getParameter("paCarreraPrestamo"));
        
        GenericReturn retorno = PrestamoAula_PA_DAO.delPrestamo(paAula, paEdificio, paCarrera, paHorarioAula, paDiaAula, paCarreraPrestamo,user, AppInfo.APP_NAME_1, AppInfo.APP_MOD_PA);
        
        int paCodError = retorno.getPaCodError();
        String paDesError = retorno.getPaDesError();
        
        if (paCodError == 0) 
        {
            response.getWriter().write(String.valueOf(paCodError));
        }else{
            response.getWriter().write(paDesError);
        }
        
        
        
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
