/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets.prestamoaula;

import bean.general.GenericReturn;
import bean.prestamoaula.Aulas_PA_MB;
import bean.prestamoaula.Carreras_PA_MB;
import bean.prestamoaula.Edificios_PA_MB;
import bean.prestamoaula.PrestamoAula_PA_MB;
import bean.session.UsuarioMB;
import com.google.gson.Gson;
import dao.general.General_Grupos_DAO;
import dao.prestamoaula.Catalogos_PA_DAO;
import interfaces.strings.AppInfo;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author IthzaVG
 */
@WebServlet(name = "Prestar_Aula_Srv", urlPatterns = {"/Prestar_Aula_Srv"})
public class PrestarAula_PA_Srv extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession(false);
        String opcion = request.getParameter("opcion");
        GenericReturn perAño = new GenericReturn();
        long current = System.currentTimeMillis();
        request.setAttribute("current", current);
         
        if (opcion == null) 
        {
            UsuarioMB user = (UsuarioMB) session.getAttribute("objUsuario");
            getEdificios(request, response, user);
            getCarreras(request, response, user);
            getLeyenda(perAño, request, user);
            request.getRequestDispatcher("views/coordinador/prestarAula_view.jsp").forward(request, response);
            
        }else{
            int opc = Integer.parseInt(opcion);
            UsuarioMB user = (UsuarioMB) session.getAttribute("objUsuario");
            
            switch(opc){
                case 1: 
                    getAulas(request, response, user);
                    break;
                
            }
        }
    }

  
    
    private void getEdificios(HttpServletRequest request, HttpServletResponse response, UsuarioMB user) throws IOException 
    {
        GenericReturn<List<Edificios_PA_MB>> edificios = Catalogos_PA_DAO.getEdificios(user, AppInfo.APP_NAME_1, AppInfo.APP_MOD_PA);
       
        if (edificios != null) {
            if (edificios.getPaCodError()== 0) 
            {
                request.setAttribute("edificios", edificios.getRetorno());
                
            }else{
                request.setAttribute("error", edificios.getPaDesError());
            }
        
        } else {
            throw new NullPointerException("Error");
        }

    }
    
    private void getAulas (HttpServletRequest request, HttpServletResponse response, UsuarioMB user) throws IOException
    {
      String paEdificioId = request.getParameter("paEdificioId");
      GenericReturn <List<Aulas_PA_MB>> aulas = Catalogos_PA_DAO.getAulas(paEdificioId, user, AppInfo.APP_NAME_1, AppInfo.APP_MOD_PA);
      
        if (aulas != null) 
        {
            String json = new Gson().toJson(aulas);
            response.setContentType("application/json");
            response.setCharacterEncoding("UTF-8");
            response.getWriter().write(json);
            
        } else{
            throw new NullPointerException("ERROR: Al obtener las aulas");
        }
    }
    
    public void getCarreras(HttpServletRequest request, HttpServletResponse response, UsuarioMB user)
    {
        GenericReturn <List<Carreras_PA_MB>> carreras = Catalogos_PA_DAO.getCarreras(user, AppInfo.APP_NAME_1, AppInfo.APP_MOD_PA);
        
        if (carreras != null) 
        {
            if (carreras.getPaCodError() == 0) 
            {
                request.setAttribute("carreras", carreras.getRetorno());
                
            } else {
                request.setAttribute("error", carreras.getPaDesError());
            }
            
        } else{
            throw new NullPointerException("Error: Al obtener carreras");
        }
        
    }
    
     private void getLeyenda(GenericReturn perAñoGen, HttpServletRequest request, UsuarioMB user) {
        perAñoGen = General_Grupos_DAO.getLeyendaPeriodo(user.getTipoPeriodoUs(), user, AppInfo.APP_NAME_1, AppInfo.APP_MOD_R);
        if (perAñoGen.getPaCodError() == 0) {
            int periodo = (int) perAñoGen.getExtraParams().get("paPeriodo");
            int año = (int) perAñoGen.getExtraParams().get("paAño");
            String paLeyenda = (String) perAñoGen.getExtraParams().get("paLeyenda");
            request.setAttribute("paPeriodoDes", paLeyenda);
            request.setAttribute("paAño", año);
            request.setAttribute("paPeriodo", periodo);
        } else {
            throw new RuntimeException("FATAL ERROR: getPeriodoAño en General_Grupos_Srv " + perAñoGen.getPaDesError());
        }
    }
   

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
