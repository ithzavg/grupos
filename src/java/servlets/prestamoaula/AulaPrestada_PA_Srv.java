/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets.prestamoaula;

import bean.general.GenericReturn;
import bean.prestamoaula.PrestamoAula_PA_MB;
import bean.session.UsuarioMB;
import dao.general.General_Grupos_DAO;
import dao.prestamoaula.PrestamoAula_PA_DAO;
import interfaces.strings.AppInfo;
import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author IthzaVG
 */
@WebServlet(name = "Aula_Prestada_Srv", urlPatterns = {"/Aula_Prestada_Srv"})
public class AulaPrestada_PA_Srv extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        HttpSession session = request.getSession(false);
        GenericReturn perAño = new GenericReturn();
        long current = System.currentTimeMillis();
        request.setAttribute("current", current);

        UsuarioMB user = (UsuarioMB) session.getAttribute("objUsuario");

        getReportePrestamo(request, response, user);
        getLeyenda(perAño, request, user);
        request.getRequestDispatcher("views/coordinador/aulasPrestadas_view.jsp").forward(request, response);

    }

    private void getReportePrestamo(HttpServletRequest request, HttpServletResponse response, UsuarioMB user) {

        GenericReturn<List<PrestamoAula_PA_MB>> aPrestada = PrestamoAula_PA_DAO.getReportePrestamo(user, AppInfo.APP_NAME_1, AppInfo.APP_MOD_PA);

        if (aPrestada != null) {
            if (aPrestada.getPaCodError() == 0) {
                request.setAttribute("prestadaRep", aPrestada.getRetorno());

            } else {
                request.setAttribute("error", aPrestada.getPaDesError());
            }
        }else{
            throw new NullPointerException("Error al obtener reporte");
        }

    }

     private void getLeyenda(GenericReturn perAñoGen, HttpServletRequest request, UsuarioMB user) {
        perAñoGen = General_Grupos_DAO.getLeyendaPeriodo(user.getTipoPeriodoUs(), user, AppInfo.APP_NAME_1, AppInfo.APP_MOD_R);
        if (perAñoGen.getPaCodError() == 0) {
            int periodo = (int) perAñoGen.getExtraParams().get("paPeriodo");
            int año = (int) perAñoGen.getExtraParams().get("paAño");
            String paLeyenda = (String) perAñoGen.getExtraParams().get("paLeyenda");
            request.setAttribute("paPeriodoDes", paLeyenda);
            request.setAttribute("paAño", año);
            request.setAttribute("paPeriodo", periodo);
        } else {
            throw new RuntimeException("FATAL ERROR: getPeriodoAño en General_Grupos_Srv " + perAñoGen.getPaDesError());
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
