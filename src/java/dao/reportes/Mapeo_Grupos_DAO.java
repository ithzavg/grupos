/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao.reportes;

import bean.general.GenericReturn;
import bean.reportes.Aula_Grupos_MB;
import bean.reportes.Grupo_Grupos_MB;
import bean.reportes.Mapeo_Grupos_MB;
import bean.reportes.Profesor_Profesor_MB;
import bean.session.UsuarioMB;
import itt.web.conexion.Conexion;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import mx.edu.ittoluca.logutils.Logger;
import oracle.jdbc.OracleTypes;

/**
 *
 * @author Edgar
 */
public class Mapeo_Grupos_DAO {

    public static GenericReturn<Mapeo_Grupos_MB> getMapeoGrupos(String aula, UsuarioMB user, String app, String mod) {

        final Logger logger = new Logger();
        GenericReturn<Mapeo_Grupos_MB> bean = new GenericReturn<>();
        Connection con = Conexion.getConnection(user.getNomUsuario(), user.getPassUsuario(), app, mod);

        if (con != null) {

            try (CallableStatement cs = con.prepareCall("{call DEPTOS.PQ_REPORTES_GPO_COOR.REPORTE_MAPEO_AULA_SP(?,?,?,?,?,?,?,?,?,?,?)}")) {

                cs.setString("paAula", aula);
                cs.setInt("paOpcionC", user.getTipoPeriodoUs());

                cs.registerOutParameter("paLeyenda", OracleTypes.VARCHAR);
                cs.registerOutParameter("paCurRetornoLun", OracleTypes.CURSOR);
                cs.registerOutParameter("paCurRetornoMart", OracleTypes.CURSOR);
                cs.registerOutParameter("paCurRetornoMie", OracleTypes.CURSOR);
                cs.registerOutParameter("paCurRetornoJuev", OracleTypes.CURSOR);
                cs.registerOutParameter("paCurRetornoVie", OracleTypes.CURSOR);
                cs.registerOutParameter("paCurRetornoSab", OracleTypes.CURSOR);
                cs.registerOutParameter("paCodError", OracleTypes.NUMBER);
                cs.registerOutParameter("paDescError", OracleTypes.VARCHAR);

                cs.execute();

                bean.setPaCodError(cs.getInt("paCodError"));
                bean.setPaDesError(cs.getString("paDescError"));

                if (bean.getPaCodError() == 0) {

                    Mapeo_Grupos_MB mapeo = new Mapeo_Grupos_MB();

                    mapeo.setGenHeader(aula);
                    mapeo.setLeyenda(cs.getString("paLeyenda"));
                    mapeo.setLunes(getDia((ResultSet) cs.getObject("paCurRetornoLun"), 1));
                    mapeo.setMartes(getDia((ResultSet) cs.getObject("paCurRetornoMart"), 1));
                    mapeo.setMiercoles(getDia((ResultSet) cs.getObject("paCurRetornoMie"), 1));
                    mapeo.setJueves(getDia((ResultSet) cs.getObject("paCurRetornoJuev"), 1));
                    mapeo.setViernes(getDia((ResultSet) cs.getObject("paCurRetornoVie"), 1));
                    mapeo.setSabado(getDia((ResultSet) cs.getObject("paCurRetornoSab"), 1));

                    bean.setRetorno(mapeo);

                }

            } catch (SQLException ex) {

                logger.registrarErrorSQL(ex, app, mod, user.getNomUsuario());
                bean.setPaCodError(ex.getErrorCode());
                bean.setPaDesError(logger.getMensajeError());

            } finally {
                Conexion.cerrarConexion(con);
            }

        } else {
            bean.setPaDesError(Conexion.getConnectionErrorMessage());
        }

        return bean;

    }

    public static GenericReturn<Mapeo_Grupos_MB> getMapeoGrupos(int prof, int opC, UsuarioMB user, String app, String mod) {

        GenericReturn<Mapeo_Grupos_MB> bean = new GenericReturn<>();
        final Logger logger = new Logger();

        Connection con = Conexion.getConnection(user.getNomUsuario(), user.getPassUsuario(), app, mod);

        if (con != null) {
            try (CallableStatement cs = con.prepareCall("{call DEPTOS.PQ_REPORTES_GPO_JDPTO.REPORTE_MAPEO_PROF_SP(?,?,?,?,?,?,?,?,?,?,?,?,?)}")) {
                cs.setInt("paProfId", prof);
                cs.setInt("paOpcionC", opC);

                cs.registerOutParameter("paNombreProfesor", OracleTypes.VARCHAR);
                cs.registerOutParameter("paCurRetornoLun", OracleTypes.CURSOR);
                cs.registerOutParameter("paCurRetornoMart", OracleTypes.CURSOR);
                cs.registerOutParameter("paCurRetornoMie", OracleTypes.CURSOR);
                cs.registerOutParameter("paCurRetornoJuev", OracleTypes.CURSOR);
                cs.registerOutParameter("paCurRetornoVie", OracleTypes.CURSOR);
                cs.registerOutParameter("paCurRetornoSab", OracleTypes.CURSOR);
                cs.registerOutParameter("paHorasPlaza", OracleTypes.NUMBER);
                cs.registerOutParameter("paHorasFrenteGpo", OracleTypes.NUMBER);
                
                cs.registerOutParameter("paCodError", OracleTypes.NUMBER);
                cs.registerOutParameter("paDescError", OracleTypes.VARCHAR);

                cs.execute();

                bean.setPaCodError(cs.getInt("paCodError"));
                bean.setPaDesError(cs.getString("paDescError"));

                if (bean.getPaCodError() == 0) {
                    Mapeo_Grupos_MB mapeo = new Mapeo_Grupos_MB();

                    mapeo.setGenHeader(cs.getString("paNombreProfesor"));
                    mapeo.setHorasGrupo(cs.getInt("paHorasFrenteGpo"));
                    //mapeo.setHorasGrupo(4);
                    mapeo.setHorasPlaza(cs.getInt("paHorasPlaza"));
                    mapeo.setLunes(getDia((ResultSet) cs.getObject("paCurRetornoLun"), 2));
                    mapeo.setMartes(getDia((ResultSet) cs.getObject("paCurRetornoMart"), 2));
                    mapeo.setMiercoles(getDia((ResultSet) cs.getObject("paCurRetornoMie"), 2));
                    mapeo.setJueves(getDia((ResultSet) cs.getObject("paCurRetornoJuev"), 2));
                    mapeo.setViernes(getDia((ResultSet) cs.getObject("paCurRetornoVie"), 2));
                    mapeo.setSabado(getDia((ResultSet) cs.getObject("paCurRetornoSab"), 2));

                    bean.setRetorno(mapeo);

                }

            } catch (SQLException ex) {

                logger.registrarErrorSQL(ex, app, mod, user.getNombreCarrera());
                bean.setPaCodError(ex.getErrorCode());
                bean.setPaDesError(logger.getMensajeError());

            } finally {
                Conexion.cerrarConexion(con);
            }

        } else {
            bean.setPaDesError(Conexion.getConnectionErrorMessage());
        }

        return bean;
    }

    public static GenericReturn<List<Aula_Grupos_MB>> getAulasMapeo(UsuarioMB user, String app, String mod) {

        final Logger logger = new Logger();
        GenericReturn<List<Aula_Grupos_MB>> bean = new GenericReturn<>();

        Connection con
                = Conexion.getConnection(user.getNomUsuario(), user.getPassUsuario(), app, mod);

        if (con != null) {

            try (CallableStatement cs = con.prepareCall("{call DEPTOS.PQ_GET_CATALOG_APP_GRUPOS_C.GET_AULAS_X_CARRERA_SP(?,?,?,?,?)}")) {

                cs.setInt("paCarreraId", user.getCarreraUS());
                cs.setInt("paOpcPeriodo", user.getTipoPeriodoUs());
                cs.registerOutParameter("paCurRetorno", OracleTypes.CURSOR);
                cs.registerOutParameter("paCodError", OracleTypes.NUMBER);
                cs.registerOutParameter("paDescError", OracleTypes.VARCHAR);

                cs.execute();

                bean.setPaCodError(cs.getInt("paCodError"));
                bean.setPaDesError(cs.getString("paDescError"));

                if (bean.getPaCodError() == 0) {
                    ResultSet rs = (ResultSet) cs.getObject("paCurRetorno");
                    List<Aula_Grupos_MB> aulas = new ArrayList<>();
                    while (rs.next()) {
                        aulas.add(new Aula_Grupos_MB(rs.getString("AULA"), rs.getString("EDIFICIO")));
                    }
                    bean.setRetorno(aulas);
                }
            } catch (SQLException ex) {

                logger.registrarErrorSQL(ex, app, mod, user.getNomUsuario());
                bean.setPaCodError(ex.getErrorCode());
                bean.setPaDesError(logger.getMensajeError());

            } finally {

                Conexion.cerrarConexion(con);

            }
        } else {

            bean.setPaDesError(Conexion.getConnectionErrorMessage());

        }

        return bean;
    }

    public static GenericReturn<List<Profesor_Profesor_MB>> getProfesorMapeo(UsuarioMB user, int opC, String app, String mod) {
        GenericReturn<List<Profesor_Profesor_MB>> bean = new GenericReturn<>();
        final Logger logger = new Logger();

        Connection con = Conexion.getConnection(user.getNomUsuario(), user.getPassUsuario(), app, mod);
        if (con != null) {
            try (CallableStatement cs = con.prepareCall("{call DEPTOS.PQ_GET_CATALOG_APP_GRUPOS_C.GET_PROF_X_ACADEMIA_SP(?,?,?,?,?)}")) {
                cs.setInt("paAcademiaId", user.getAcademiaUs());
                cs.setInt("paOpcPeriodo", opC);

                cs.registerOutParameter("paCurRetorno", OracleTypes.CURSOR);
                cs.registerOutParameter("paCodError", OracleTypes.NUMBER);
                cs.registerOutParameter("paDescError", OracleTypes.VARCHAR);

                cs.execute();

                bean.setPaCodError(cs.getInt("paCodError"));
                bean.setPaDesError(cs.getString("paDescError"));

                if (bean.getPaCodError() == 0) {
                    ResultSet rs = (ResultSet) cs.getObject("paCurRetorno");
                    List<Profesor_Profesor_MB> lProf = new ArrayList<>();
                    while (rs.next()) {
                        lProf.add(new Profesor_Profesor_MB(rs.getInt("PROF_ID"), rs.getString("NOMBRE_PROF")));
                    }

                    bean.setRetorno(lProf);
                }

            } catch (SQLException ex) {
                logger.registrarErrorSQL(ex, app, mod, user.getNomUsuario());
                bean.setPaCodError(ex.getErrorCode());
                bean.setPaDesError(logger.getMensajeError());
            } finally {
                Conexion.cerrarConexion(con);
            }
        }

        return bean;
    }

    private static List<Grupo_Grupos_MB> getDia(ResultSet rs, int op) throws SQLException {

        List<Grupo_Grupos_MB> dia = new ArrayList<>();

        switch (op) {
            case 1:
                while (rs.next()) {
                    dia.add(new Grupo_Grupos_MB(rs.getInt("GRUPO_ID"), rs.getString("MATERIA"), rs.getString("NOMBRE_PROF"), rs.getInt("HOR_INI"), rs.getInt("HOR_FIN")));
                }
                break;
            case 2:
                while (rs.next()) {
                    dia.add(new Grupo_Grupos_MB(rs.getInt("GPO_ID"), rs.getString("MATERIA"), rs.getString("AULA"), rs.getInt("HOR_INI"), rs.getInt("HOR_FIN")));
                }
                break;
        }

        return dia;
    }


}
