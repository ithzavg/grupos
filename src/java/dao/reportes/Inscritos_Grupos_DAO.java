/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao.reportes;

import bean.general.GenericReturn;
import bean.reportes.Inscrito_Grupos_MB;
import bean.reportes.Inscritos_Grupos_MB;
import bean.session.UsuarioMB;
import itt.web.conexion.Conexion;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import mx.edu.ittoluca.logutils.Logger;
import oracle.jdbc.OracleTypes;

/**
 *
 * @author Edgar
 */
public class Inscritos_Grupos_DAO {

    public static GenericReturn<Inscritos_Grupos_MB> getInfoInscritos(int gpoId, int opLis, UsuarioMB user, String app, String mod) {
        final Logger logger = new Logger();
        GenericReturn<Inscritos_Grupos_MB> bean = new GenericReturn<>();
        Connection con = Conexion.getConnection(user.getNomUsuario(), user.getPassUsuario(), app, mod);

        if (con != null) {
            try (CallableStatement cs = con.prepareCall("{call DEPTOS.PQ_REPORTES_GPO_COOR.GET_INFO_GPO_INSCRITOS_SP(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}")) {
                cs.setInt("paGrupoIdPar", gpoId);
                cs.setInt("paOpcPeriodo", user.getTipoPeriodoUs());
                cs.setInt("paOpcionListado", opLis);

                cs.registerOutParameter("paCarreraDelGrupo", OracleTypes.VARCHAR);
                cs.registerOutParameter("paPeriodoLeyenda", OracleTypes.VARCHAR);
                cs.registerOutParameter("paMateriaId", OracleTypes.NUMBER);
                cs.registerOutParameter("paNombreMateria", OracleTypes.VARCHAR);
                cs.registerOutParameter("paProfesor", OracleTypes.VARCHAR);
                cs.registerOutParameter("paTarjetaProf", OracleTypes.NUMBER);
                cs.registerOutParameter("paGrupoId", OracleTypes.NUMBER);
                cs.registerOutParameter("paPlanId", OracleTypes.NUMBER);
                cs.registerOutParameter("paHLun", OracleTypes.VARCHAR);
                cs.registerOutParameter("paHMar", OracleTypes.VARCHAR);
                cs.registerOutParameter("paHMie", OracleTypes.VARCHAR);
                cs.registerOutParameter("paHJue", OracleTypes.VARCHAR);
                cs.registerOutParameter("paHVie", OracleTypes.VARCHAR);
                cs.registerOutParameter("paHSab", OracleTypes.VARCHAR);
                cs.registerOutParameter("paCurRetorno", OracleTypes.CURSOR);
                cs.registerOutParameter("paCodError", OracleTypes.NUMBER);
                cs.registerOutParameter("paDescError", OracleTypes.VARCHAR);

                cs.execute();

                bean.setPaCodError(cs.getInt("paCodError"));
                bean.setPaDesError(cs.getString("paDescError"));

                if (bean.getPaCodError() == 0) {

                    List<Inscrito_Grupos_MB> inscrito = new ArrayList<>();
                    Inscritos_Grupos_MB inscritos = new Inscritos_Grupos_MB();
                    inscritos.setCarrera(cs.getString("paCarreraDelGrupo"));
                    inscritos.setPeriodo(cs.getString("paPeriodoLeyenda"));
                    inscritos.setMateria(cs.getString("paNombreMateria"));
                    inscritos.setProfesor(cs.getString("paProfesor"));
                    inscritos.setTarjeta(cs.getInt("paTarjetaProf"));
                    inscritos.setGrupo(cs.getInt("paGrupoId"));
                    inscritos.setPlan(cs.getInt("paPlanId"));
                    inscritos.sethLun(cs.getString("paHLun"));
                    inscritos.sethMar(cs.getString("paHMar"));
                    inscritos.sethMie(cs.getString("paHMie"));
                    inscritos.sethJue(cs.getString("paHJue"));
                    inscritos.sethVie(cs.getString("paHVie"));
                    inscritos.sethSab(cs.getString("paHSab"));

                    ResultSet rs = (ResultSet) cs.getObject("paCurRetorno");
                    switch (opLis) {
                        case 1:
                            while (rs.next()) {                                
                                inscrito.add(new Inscrito_Grupos_MB(rs.getInt("ID_ALUMNO")
                                                                   ,rs.getString("NOMBRE_ALUMNO")
                                                                   ,rs.getString("FECHA_INSCRIPCION")));
                            }
                            break;
                        case 2:
                            while (rs.next()) {
                                inscrito.add(new Inscrito_Grupos_MB(rs.getInt("ID_ALUMNO")
                                                                   ,rs.getString("NOMBRE_ALUMNO")
                                                                   ,rs.getString("GRUPO")));
                            }
                            break;
                    }
                    inscritos.setlInscritos(inscrito);
                    bean.setRetorno(inscritos);
                }

            } catch (SQLException ex) {
                logger.registrarErrorSQL(ex, app, mod, user.getNomUsuario());
                bean.setPaCodError(ex.getErrorCode());
                bean.setPaDesError(logger.getMensajeError());
            } finally {
                Conexion.cerrarConexion(con);
            }
        } else {
            bean.setPaDesError(Conexion.getConnectionErrorMessage());
        }
        return bean;
    }

}
