/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao.prestamoaula;

import bean.general.GenericReturn;
import bean.prestamoaula.Aulas_PA_MB;
import bean.prestamoaula.Carreras_PA_MB;
import bean.prestamoaula.Edificios_PA_MB;
import bean.prestamoaula.PrestamoAula_PA_MB;
import bean.reportes.Aula_Grupos_MB;
import bean.session.UsuarioMB;
import itt.web.conexion.Conexion;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import mx.edu.ittoluca.logutils.Logger;
import oracle.jdbc.OracleTypes;

/**
 *
 * @author IthzaVG
 */
public class Catalogos_PA_DAO {

    /**
     * Descripción: Este método va a obtener las carreras
     *
     * @param user: contiene los datos para abrir una sesión
     * @param paOpcionCatalogo: obtiene las carreras 5: carreras
     * @param app:Nombre de la aplicacion
     * @param mod: Nombre del modulo
     * @return
     */
    public static GenericReturn<List<Carreras_PA_MB>> getCarreras(UsuarioMB user, String app, String mod) {
        GenericReturn retorna = new GenericReturn();
        final Logger logger = new Logger();
        Connection con = Conexion.getConnection(user.getNomUsuario(), user.getPassUsuario(), app, mod);

        CallableStatement cs;

        if (con != null) {
            try {
                cs = con.prepareCall("{call DEPTOS.PQ_GET_CATALOG_APP_GRUPOS_A.GET_CATALOG_GENERICOS_SP(?,?,?,?,?,?)}");

                cs.setString("paPlanId", null);
                cs.setString("paEspeciaId", null);
                cs.setInt("paOpcionCatalogo", 5);

                cs.registerOutParameter("paCurRetorno", OracleTypes.CURSOR);
                cs.registerOutParameter("paCodError", OracleTypes.NUMBER);
                cs.registerOutParameter("paDescError", OracleTypes.VARCHAR);

                cs.execute();

                retorna.setPaCodError(cs.getInt("paCodError"));
                retorna.setPaDesError(cs.getString("paDescError"));

                if (retorna.getPaCodError() == 0) {
                    List<Carreras_PA_MB> lCarreras = new ArrayList<>();

                    ResultSet rs = (ResultSet) cs.getObject("paCurRetorno");

                    while (rs.next()) {
                        lCarreras.add(new Carreras_PA_MB(
                                rs.getInt("ID"),
                                rs.getString("NOMBRE"),
                                rs.getString("NOM_CTO")
                        ));

                    }
                    retorna.setRetorno(lCarreras);
                }

            } catch (SQLException ex) {
                logger.registrarErrorSQL(ex, app, mod, user.getNomUsuario());
                retorna.setPaCodError(ex.getErrorCode());
                retorna.setPaDesError(logger.getMensajeError());
                
            } finally {
                Conexion.cerrarConexion(con);
            }

        } else {
            retorna.setPaDesError(Conexion.getConnectionErrorMessage());
        }

        return retorna;
    }

    /**
     * Descripción: Este método va a obtener los edificios
     *
     * @param user: Contiene los datos del usuario
     * @param app:nombre de la aplicación
     * @param mod: nombre del modulo
     * @return
     */
    public static GenericReturn<List<Edificios_PA_MB>> getEdificios(UsuarioMB user, String app, String mod) {
        GenericReturn retorna = new GenericReturn();
        final Logger logger = new Logger();
        Connection con = Conexion.getConnection(user.getNomUsuario(), user.getPassUsuario(), app, mod);

        CallableStatement cs;

        if (con != null) {
            try {
                cs = con.prepareCall("{call DEPTOS.PQ_GET_CATALOG_APP_GRUPOS_A.GET_CATALOG_GENERICOS_SP(?,?,?,?,?,?)}");

                cs.setString("paPlanId", null);
                cs.setString("paEspeciaId", null);
                cs.setInt("paOpcionCatalogo", 6);

                cs.registerOutParameter("paCurRetorno", OracleTypes.CURSOR);
                cs.registerOutParameter("paCodError", OracleTypes.NUMBER);
                cs.registerOutParameter("paDescError", OracleTypes.VARCHAR);

                cs.execute();

                retorna.setPaCodError(cs.getInt("paCodError"));
                retorna.setPaDesError(cs.getString("paDescError"));

                if (retorna.getPaCodError() == 0) {

                    List<Edificios_PA_MB> lEdificios = new ArrayList<>();
                    ResultSet rs = (ResultSet) cs.getObject("paCurRetorno");

                    while (rs.next()) {
                        lEdificios.add(new Edificios_PA_MB(
                                rs.getString("EDIFICIO_ID"),
                                rs.getString("NOMBRE")
                        ));

                    }
                    retorna.setRetorno(lEdificios);
                }

            } catch (SQLException ex) {
                logger.registrarErrorSQL(ex, app, mod, user.getNomUsuario());
                retorna.setPaCodError(ex.getErrorCode());
                retorna.setPaDesError(logger.getMensajeError());
            } finally {
                Conexion.cerrarConexion(con);
            }

        } else {
            retorna.setPaDesError(Conexion.getConnectionErrorMessage());
        }

        return retorna;
    }

    /**
     * Descripción: Este metodo obtiene las aulas de los edificios
     *
     * @param paEdificioId: identificador de edificio
     * @param user: contiene los datos de usuario en sesion
     * @param app:nombre de la aplicación
     * @param mod: nombre del modulo
     * @return
     */
    public static GenericReturn<List<Aulas_PA_MB>> getAulas(String paEdificioId, UsuarioMB user, String app, String mod) {
        final Logger logger = new Logger();
        GenericReturn retorna = new GenericReturn();
        Connection con = Conexion.getConnection(user.getNomUsuario(), user.getPassUsuario(), app, mod);

        if (con != null) {
            CallableStatement cs;

            try {

                cs = con.prepareCall("{call DEPTOS.PQ_GET_CATALOG_APP_GRUPOS_A.GET_AULAS_X_EDIF_SP(?,?,?,?)}");

                cs.setString("paEdificioId", paEdificioId);

                cs.registerOutParameter("paCurRetorno", OracleTypes.CURSOR);
                cs.registerOutParameter("paCodError", OracleTypes.NUMBER);
                cs.registerOutParameter("paDescError", OracleTypes.VARCHAR);

                cs.execute();

                retorna.setPaCodError(cs.getInt("paCodError"));
                retorna.setPaDesError(cs.getString("paDescError"));
                
                if (retorna.getPaCodError() == 0) {
                    ResultSet rs = (ResultSet) cs.getObject("paCurRetorno");
                    List<Aulas_PA_MB> lAulas = new ArrayList<>();

                    while (rs.next()) {
                        lAulas.add(new Aulas_PA_MB(
                                rs.getString("AULA"),
                                rs.getInt("CAPACIDAD_AULA")
                        ));
                        
                    }
                    retorna.setRetorno(lAulas);

                }

            } catch (SQLException ex) {
                logger.registrarErrorSQL(ex, app, mod, user.getNomUsuario());
                retorna.setPaCodError(ex.getErrorCode());
                retorna.setPaDesError(logger.getMensajeError());

            } finally {
                Conexion.cerrarConexion(con);
            }

        }

        return retorna;
    }

}
