/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao.prestamoaula;

import bean.general.GenericReturn;
import bean.prestamoaula.OtrasAulas_PA_MB;
import bean.prestamoaula.PrestamoAula_PA_MB;
import bean.prestamoaula.Prestamo_PA_MB;
import bean.session.UsuarioMB;
import itt.web.conexion.Conexion;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import mx.edu.ittoluca.logutils.Logger;
import oracle.jdbc.OracleTypes;

/**
 *
 * @author IthzaVG
 */
public class PrestamoAula_PA_DAO {

    /**
     * Descripción: Realiza el prestamo de aulas
     *
     *
     * @param prestamo: contiene datos del aula que se va a prestar
     * @param user: datos del usuario para la sesión
     * @param app: nombre de la aplicación
     * @param mod: nombre del módulo
     *
     * @return
     */
    public static GenericReturn setPrestamo(Prestamo_PA_MB prestamo, UsuarioMB user, String app, String mod) throws SQLException {
        final Logger logger = new Logger();
        GenericReturn retorna = new GenericReturn();
        Connection con = Conexion.getConnection(user.getNomUsuario(), user.getPassUsuario(), app, mod);
        CallableStatement cs;

        if (con != null) {

            try {

                cs = con.prepareCall("{call DEPTOS.PQ_GET_CATALOG_APP_GRUPOS_C.SET_PRESTAMO_AULAS_COOR_SP(?,?,?,?,?,?,?,?,?)}");

                cs.setString("paAula", prestamo.getAula());
                cs.setString("paEdificio", prestamo.getEdificio());
                cs.setInt("paCarreraPrestamo", prestamo.getCarrera_prestamo());
                cs.setInt("paHoraIni", prestamo.getHora_inicio());
                cs.setInt("paHoraFinal", prestamo.getHora_fin());
                cs.setInt("paDia", prestamo.getDia());
                cs.setInt("paOpcPeriodo", user.getTipoPeriodoUs());

                cs.registerOutParameter("paCodError", OracleTypes.NUMBER);
                cs.registerOutParameter("paDescError", OracleTypes.VARCHAR);

                cs.execute();

                int paCodError = cs.getInt("paCodError");
                retorna.setPaCodError(paCodError);
                retorna.setPaDesError(cs.getString("paDescError"));

            } catch (SQLException ex) {
                logger.registrarErrorSQL(ex, app, mod, user.getNomUsuario());
                retorna.setPaCodError(ex.getErrorCode());
                retorna.setPaDesError(logger.getMensajeError());
            } finally {
                Conexion.cerrarConexion(con);
            }

        } else {

            retorna.setPaDesError(Conexion.getConnectionErrorMessage());

        }

        return retorna;
    }
    
    /**
     * Descripción: método que elimina un prestamo de aula
     * @param paAula: identifica el aula del prestamo a eliminar
     * @param paEdificio: identifica el edificio del prestamo a eliminar
     * @param paCarreraProp: identifica la carrera propietario del prestamo a eliminar
     * @param paHorarioAula: identifica el horario del aula del prestamo a elimnar
     * @param paDiaAula: identifica el dia del prestamo a eliminar
     * @param user datos del usuario en sesión
     * @param app: nombre de la aplicación
     * @param mod: nombre del modulo
     * paOpcPeriodo: identifica el periodo del prestamo a eliminar 1:Grupos actuales, 2:Grupos preparacion
     * 3:Grupos intersemestral
     * @return 
     */
    
    public static GenericReturn delPrestamo(String paAula, String paEdificio,int paCarreraProp, String paHorarioAula,int paDiaAula, int paCarreraPrestamo,UsuarioMB user, String app, String mod)
    {
        final Logger logger = new Logger();
        GenericReturn retorna = new GenericReturn();
        Connection con = Conexion.getConnection(user.getNomUsuario(), user.getPassUsuario(), app, mod);
        CallableStatement cs;
        
        if (con != null) 
        {
            try
            {
                cs = con.prepareCall("{call DEPTOS.PQ_DML_APP_GRUPOS_COOR.DELETE_PRESTAMO_AULAS_SP(?,?,?,?,?,?,?,?,?)}");
                cs.setString("paAula", paAula);
                cs.setString("paEdificio", paEdificio);
                cs.setInt("paCarreraProp", paCarreraProp);
                cs.setString("paHorarioAula", paHorarioAula);
                cs.setInt("paDiaAula", paDiaAula);
                cs.setInt("paCarreraPrestamo", paCarreraPrestamo);
                cs.setInt("paOpcPeriodo", user.getTipoPeriodoUs());
                
                cs.registerOutParameter("paCodError", OracleTypes.NUMBER);
                cs.registerOutParameter("paDescError", OracleTypes.VARCHAR);
                
                cs.execute();
                
                int paCodError = cs.getInt("paCodError");
                retorna.setPaCodError(paCodError);
                retorna.setPaDesError(cs.getString("paDescError"));
            }catch(SQLException ex)
            {
                logger.registrarErrorSQL(ex, app, mod, user.getNomUsuario());
                retorna.setPaCodError(ex.getErrorCode());
                retorna.setPaDesError(logger.getMensajeError());
                
            }finally{
                Conexion.cerrarConexion(con);
            }
            
        }else{
            retorna.setPaDesError(Conexion.getConnectionErrorMessage());
        }
        
        return retorna;
        
    }
    
    
    /**
     * Descripción: método que obtiene reporte de las aulas prestadas
     * @param user: datos del usuario 
     * @param app: nombre de la aplicación
     * @param mod: nombre del módulo
     * @return 
     */

    public static GenericReturn<List<PrestamoAula_PA_MB>> getReportePrestamo(UsuarioMB user, String app, String mod) {
        final Logger logger = new Logger();
        GenericReturn retorna = new GenericReturn();
        Connection con = Conexion.getConnection(user.getNomUsuario(), user.getPassUsuario(), app, mod);

        CallableStatement cs;

        if (con != null) {
            try {

                cs = con.prepareCall("{call DEPTOS.PQ_GET_CATALOG_APP_GRUPOS_C.GET_REPORTE_AULAS_COOR_SP(?,?,?,?,?,?)}");

                cs.setInt("paCarreraId", user.getCarreraUS());
                cs.setInt("paOpcPeriodo", user.getTipoPeriodoUs());
                cs.setInt("paOpcReporte", 1);

                cs.registerOutParameter("paCurRetorno", OracleTypes.CURSOR);
                cs.registerOutParameter("paDescError", OracleTypes.VARCHAR);
                cs.registerOutParameter("paCodError", OracleTypes.NUMBER);

                cs.execute();

                int paCodError = cs.getInt("paCodError");
                retorna.setPaCodError(paCodError);
                retorna.setPaDesError(cs.getString("paDescError"));

                if (paCodError == 0) {
                    ResultSet rs = (ResultSet) cs.getObject("paCurRetorno");
                    List<PrestamoAula_PA_MB> prestamo = new ArrayList<>();

                            while (rs.next()) {
                                prestamo.add(new PrestamoAula_PA_MB(
                                        rs.getString("EDIFICIO"),
                                        rs.getString("AULA"),
                                        rs.getString("CARRERA_PROPIETARIO"),
                                        rs.getInt("ID_CARRERA_PROP"),
                                        rs.getString("CARRERA_PRESTAMO"),
                                        rs.getString("HORARIO_AULA"),
                                        rs.getString("DIA"),
                                        rs.getInt("NUM_DIA"),
                                        rs.getInt("ID_CARRERA_PRESTAMO")
                                        
                                ));
                                
                            }
                            retorna.setRetorno(prestamo);
                }

            } catch (SQLException ex) {
                logger.registrarErrorSQL(ex, app, mod, user.getNomUsuario());
                retorna.setPaCodError(ex.getErrorCode());
                retorna.setPaDesError(logger.getMensajeError());
            }finally{
                Conexion.cerrarConexion(con);
            }

        } else {
            retorna.setPaDesError(Conexion.getConnectionErrorMessage());

        }

        return retorna;
    }
    
    
    /**
     * Descripción: método que despliega las aulas que le prestan a la carrera
     * @param user: datos del usuario para la sesión
     * @param app: nombre de la aplicación
     * @param mod: nombre del módulo
     * @return 
     */
     public static GenericReturn<List<OtrasAulas_PA_MB>> getReporteOAulas(UsuarioMB user, String app, String mod) {
        final Logger logger = new Logger();
        GenericReturn retorna = new GenericReturn();
        Connection con = Conexion.getConnection(user.getNomUsuario(), user.getPassUsuario(), app, mod);

        CallableStatement cs;

        if (con != null) {
            try {

                cs = con.prepareCall("{call DEPTOS.PQ_GET_CATALOG_APP_GRUPOS_C.GET_REPORTE_AULAS_COOR_SP(?,?,?,?,?,?)}");

                cs.setInt("paCarreraId", user.getCarreraUS());
                cs.setInt("paOpcPeriodo", user.getTipoPeriodoUs());
                cs.setInt("paOpcReporte", 2);

                cs.registerOutParameter("paCurRetorno", OracleTypes.CURSOR);
                cs.registerOutParameter("paDescError", OracleTypes.VARCHAR);
                cs.registerOutParameter("paCodError", OracleTypes.NUMBER);

                cs.execute();

                int paCodError = cs.getInt("paCodError");
                retorna.setPaCodError(paCodError);
                retorna.setPaDesError(cs.getString("paDescError"));

                if (paCodError == 0) {
                    ResultSet rs = (ResultSet) cs.getObject("paCurRetorno");
                    List<OtrasAulas_PA_MB> oaulas = new ArrayList<>();
                        
                            while (rs.next()) {
                                oaulas.add(new OtrasAulas_PA_MB(
                                        rs.getString("EDIFICIO"),
                                        rs.getString("AULA"),
                                        rs.getString("CARRERA_PROPIETARIO"),
                                        rs.getString("CARRERA_PRESTAMO"),
                                        rs.getString("HORARIO_AULA"),
                                        rs.getString("DIA")
                                       
                                ));
                                
                            }
                            retorna.setRetorno(oaulas);
                }

            } catch (SQLException ex) {
                logger.registrarErrorSQL(ex, app, mod, user.getNomUsuario());
                retorna.setPaCodError(ex.getErrorCode());
                retorna.setPaDesError(logger.getMensajeError());
            }finally{
                Conexion.cerrarConexion(con);
            }

        } else {
            retorna.setPaDesError(Conexion.getConnectionErrorMessage());

        }

        return retorna;
    }
}
