/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao.grupos;

import bean.general.GenericReturn;
import bean.grupos.Grupo_Grupos_MB;
import bean.session.UsuarioMB;
import itt.web.conexion.Conexion;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import mx.edu.ittoluca.logutils.Logger;
import oracle.jdbc.OracleTypes;

/**
 *
 * @author Edgar
 */
public class DML_Grupos_DAO {
        
    

    /**
     * Descripción: Registra un nuevo grupo en la base de datos ya sea grupo
     * normal o grupo compartido
     *
     * @param grupo : Contiene los datos del grupo que sera dado de alta
     * @param paPlanId : Identifica el plan con el cual el grupo será dado de
     * alta
     * @param paEspId : Identifica la especialidad con la cual el grupo será
     * dado de alta
     * @param paTipoGrupo: Identifica si es grupo Compartido o Normal (1): Grupo
     * Normal (2): Grupo Compartido
     * @param user : Contiene los datos del usuario en sesión
     * @param app : Nombre de la aplicación
     * @param mod : Nombre del módulo
     * @return gr : Devuelve un elemento GenericReturn el cual almacena el grupoId, paCodError y paDesError
     */
    public static GenericReturn<Integer> insertGrupos(Grupo_Grupos_MB grupo, int paPlanId, int paEspId, int paTipoGrupo, UsuarioMB user, String app, String mod) {
        final Logger logger = new Logger();   
        GenericReturn<Integer> gr = new GenericReturn<>();        
        Connection con = Conexion.getConnection(user.getNomUsuario(), user.getPassUsuario(), app,mod);
        
        CallableStatement cs;
        if (con != null) {
            try {
                cs = con.prepareCall("{call DEPTOS.PQ_DML_APP_GRUPOS_COOR.INSERT_GPOS_GENERAL_SP(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}");
                cs.setInt("paMateriaId", grupo.getGrupo_Id());
                cs.setInt("paGpoReal", grupo.getGrupoId_Real());
                cs.setInt("paDeptoAcad", Integer.parseInt(grupo.getAcad_Imparte_Mat()));
                cs.setInt("paPlanId", paPlanId);
                cs.setInt("paEspId", paEspId);
                cs.setInt("paBloque", grupo.getBloque());
                cs.setInt("paCapacidad", grupo.getCapacidad());
                cs.setString("paHlun", grupo.getH_LUN());
                cs.setString("paHmar", grupo.getH_MAR());
                cs.setString("paHmie", grupo.getH_MIE());
                cs.setString("paHjue", grupo.getH_JUE());
                cs.setString("paHvie", grupo.getH_VIE());
                cs.setString("paHsab", grupo.getH_SAB()); //SABADO
                cs.setInt("paCarreraId", user.getCarreraUS());//Revisar
                cs.setInt("paTopeAlumno", grupo.getTope_Alumno());
                cs.setInt("paOpcionPeriodo", user.getTipoPeriodoUs());
                cs.setInt("paTipoGrupo", paTipoGrupo);
                
                cs.registerOutParameter("paGrupoId", OracleTypes.NUMBER);
                cs.registerOutParameter("paCodError", OracleTypes.NUMBER);
                cs.registerOutParameter("paDescError", OracleTypes.VARCHAR);

                cs.execute();

                int paCodError = cs.getInt("paCodError");
                gr.setPaCodError(paCodError);

                if (paCodError == 0) {                    
                    gr.setRetorno(cs.getInt("paGrupoId"));         
                } else {                    
                    gr.setPaDesError(cs.getString("paDescError"));                    
                }

            } catch (SQLException ex) {
                logger.registrarErrorSQL(ex, app, mod, user.getNomUsuario());
                gr.setPaCodError(ex.getErrorCode());
                gr.setPaDesError(logger.getMensajeError()+"\n"+ex.getMessage());
            } finally {
                Conexion.cerrarConexion(con);
            }
        } else {            
            gr.setPaDesError(Conexion.getConnectionErrorMessage());
        }

        return gr;
    }
    
    /**
     * Descripción: Actualiza los datos de los grupos preparación y preparacion
     * compartidos segun se indique en paOpcion
     *
     * @param grupo : Contiene los datos del grupo a ser actualizado
     * @param paTipoGrupo : Especifica si es un grupo normal o un grupo compartido
     * (1): Grupo Normal (2): Grupo Compartido
     * @param app : Nombre de la aplicación
     * @param mod : Nombre del módulo
     * @param user : Contiene los datos del usuario en sesión
     * @return retorno : Devuelve un elemento GenericReturn el cual almacena el paCodError y el paDesError
     */
    public static GenericReturn updateGrupos(Grupo_Grupos_MB grupo, int paTipoGrupo, UsuarioMB user, String app, String mod) {        
        final Logger logger = new Logger();   
        GenericReturn retorno = new GenericReturn();
        Connection con = Conexion.getConnection(user.getNomUsuario(), user.getPassUsuario(),app, mod);
        CallableStatement cs;

        if (con != null) {
            try {
                
                cs = con.prepareCall("{call DEPTOS.PQ_DML_APP_GRUPOS_COOR.UPDATE_GPOS_GENERAL_SP(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}");
          
                cs.setInt("paGrupoId", grupo.getGrupo_Id());  
                cs.setInt("paGpoReal", grupo.getGrupoId_Real());
                cs.setInt("paPeriodo", grupo.getPeriodo());
                cs.setInt("paAño", grupo.getAño());
                cs.setInt("paCapacidad", grupo.getCapacidad());
                cs.setInt("paBloque", grupo.getBloque());
                cs.setString("paHlun", grupo.getH_LUN());
                cs.setString("paHmar", grupo.getH_MAR());
                cs.setString("paHmie", grupo.getH_MIE());
                cs.setString("paHjue", grupo.getH_JUE());
                cs.setString("paHvie", grupo.getH_VIE());
                cs.setString("paHsab", grupo.getH_SAB()); //SABADO
                cs.setInt("paCarreraId", user.getCarreraUS());
                cs.setInt("paTopeAlumno", grupo.getTope_Alumno());
                cs.setInt("paTipoGrupo", paTipoGrupo);                
                
                cs.registerOutParameter("paCodError", OracleTypes.NUMBER);
                cs.registerOutParameter("paDescError", OracleTypes.VARCHAR);
                
                cs.execute();

                int paCodError = cs.getInt("paCodError");
                retorno.setPaCodError(paCodError);
                retorno.setPaDesError(cs.getString("paDescError"));               

            } catch (SQLException ex) {
                logger.registrarErrorSQL(ex, app, mod, user.getNomUsuario());
                retorno.setPaCodError(ex.getErrorCode());
                retorno.setPaDesError(logger.getMensajeError()+"\n"+ex.getMessage());                
            } finally {
                Conexion.cerrarConexion(con);
            }
        } else {
            retorno.setPaDesError(logger.getMensajeError());            
        }
        return retorno;
    }

    /**
     *
     * @param paGrupoId : Identifica al grupo que se desea eliminar
     * @param paPeriodo : Identifica el periodo en el cual se desea eliminar el
     * grupo
     * @param paAño : Identifica el año en el cual se desea eliminar el grupo
     * @param user : Contiene los datos del usuario en sesión
     * @param app : Nombre de la aplicación
     * @param mod : Nombre del módulo
     * @return retorno : Devuelve un elemento GenericReturn el cual almacena el paCodError y el paDesError
     */
    public static GenericReturn delGrupos(int paGrupoId, int paPeriodo, int paAño, UsuarioMB user, String app, String mod) {
        final Logger logger = new Logger();   
        GenericReturn retorno = new GenericReturn();
        Connection con = Conexion.getConnection(user.getNomUsuario(), user.getPassUsuario(),app,mod);
        CallableStatement cs;

        if (con != null) {
            try {
                cs = con.prepareCall("{call DEPTOS.PQ_DML_APP_GRUPOS_COOR.DELETE_GRUPOS_GENERAL_SP(?,?,?,?,?)}");
                cs.setInt("paGrupoId", paGrupoId);
                cs.setInt("paPeriodo", paPeriodo);
                cs.setInt("paAño", paAño);

                cs.registerOutParameter("paCodError", OracleTypes.NUMBER);
                cs.registerOutParameter("paDescError", OracleTypes.VARCHAR);
                cs.execute();

                int paCodError = cs.getInt("paCodError");
                retorno.setPaCodError(paCodError);
                retorno.setPaDesError(cs.getString("paDescError"));
                                
            } catch (SQLException ex) {
                logger.registrarErrorSQL(ex, app, mod, user.getNomUsuario());
                retorno.setPaCodError(ex.getErrorCode());                
                retorno.setPaDesError(logger.getMensajeError()+"\n"+ex.getMessage());
            } finally {
                Conexion.cerrarConexion(con);
            }
        } else {           
            retorno.setPaDesError(Conexion.getConnectionErrorMessage());
        }
        return retorno;
    }
          
}
