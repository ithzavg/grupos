/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao.grupos;

import bean.general.GenericReturn;
import bean.grupos.Grupo_Grupos_MB;
import bean.session.UsuarioMB;
import itt.web.conexion.Conexion;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import mx.edu.ittoluca.logutils.Logger;
import oracle.jdbc.OracleTypes;

/**
 *
 * @author Edgar
 */
public class CheckCapacidad_Grupos_DAO {

    /**
     * @param grupo    
     * @param paAula
     * @param user
     * @param app : Nombre de la aplicación
     * @param mod : Nombre del módulo
     * @return paCodError : Devuelve -2222 si el aula ya no cuenta con la
     * capacidad suficiente, 0 si la capacidad es menor a la del grupo.
     */
    public static GenericReturn validaCapacidad(Grupo_Grupos_MB grupo, String paAula, UsuarioMB user, String app, String mod) {
        final Logger logger = new Logger();
        Connection con = Conexion.getConnection(user.getNomUsuario(), user.getPassUsuario(), app, mod);
        GenericReturn bean = new GenericReturn();
        
        if (con != null) {
            try (CallableStatement cs = con.prepareCall("{call DEPTOS.PQ_CHECK_AULA_GPO_COOR_2.CHECK_VALIDA_CAPACIDAD_SP(?,?,?,?,?,?,?,?)}")){                
                cs.setInt("paGrupoId", grupo.getGrupo_Id());
                cs.setInt("paGrupoReal", grupo.getGrupoId_Real());
                cs.setInt("paCapacidadGpo", grupo.getCapacidad());
                cs.setInt("paPeriodo", user.getPeriodo());
                cs.setInt("paAño", user.getAño());
                cs.setString("paAula", paAula);
                cs.registerOutParameter("paCodError", OracleTypes.NUMBER);
                cs.registerOutParameter("paDescError", OracleTypes.VARCHAR);
                cs.execute();
                
                bean.setPaCodError(cs.getInt("paCodError"));
                bean.setPaDesError(cs.getString("paDescError"));
                                
            } catch (SQLException ex) {
                logger.registrarErrorSQL(ex, app, mod, user.getNomUsuario());
                bean.setPaCodError(ex.getErrorCode());
                bean.setPaDesError(logger.getMensajeError());

            } finally {
                Conexion.cerrarConexion(con);
            }
        } else {
            bean.setPaDesError(Conexion.getConnectionErrorMessage());
        }
        return bean;
    }
}
