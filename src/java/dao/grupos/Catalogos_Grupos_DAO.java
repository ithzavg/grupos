/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao.grupos;

import bean.general.Carreras_Profesores_MB;
import bean.general.GenericReturn;
import bean.grupos.Aula_Grupos_MB;
import bean.grupos.Grupo_Grupos_MB;
import bean.grupos.Materia_Grupos_MB;
import bean.session.UsuarioMB;
import itt.web.conexion.Conexion;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import mx.edu.ittoluca.logutils.Logger;
import oracle.jdbc.OracleTypes;

/**
 *
 * @author Edgar Cirilo González
 */
public class Catalogos_Grupos_DAO {

    /**
     * Descripción: Obtiene el catalogo de las materias segun el plan Id y
     * Especialidad Id que reciba como parametros.
     *
     * @param paPlanId: Identificador de plan de las materias
     * @param paEspeciaId: Identificador de Especialidad
     * @param user : Contiene los datos del usuario en sesión
     * @param app : Nombre de la aplicación
     * @param mod : Nombre del módulo
     * @return retorno Devuelve un objeto GenericReturn con la lista de materias
     * y los parametros de salida del procedimiento
     */
    public static GenericReturn<List<Materia_Grupos_MB>> getMaterias(int paPlanId, int paEspeciaId, UsuarioMB user, String app, String mod) {
        final Logger logger = new Logger();
        GenericReturn<List<Materia_Grupos_MB>> retorno = new GenericReturn<>();
        Connection con = Conexion.getConnection(user.getNomUsuario(), user.getPassUsuario(), app, mod);
        

        if (con != null) {
            try (CallableStatement cs = con.prepareCall("{call DEPTOS.PQ_GET_CATALOG_APP_GRUPOS_A.GET_CATALOG_GENERICOS_SP(?,?,?,?,?,?)}")){
                
                cs.setInt("paPlanId", paPlanId);
                cs.setInt("paEspeciaId", paEspeciaId);
                cs.setInt("paOpcionCatalogo", 1);
                cs.registerOutParameter("paCurRetorno", OracleTypes.CURSOR);
                cs.registerOutParameter("paCodError", OracleTypes.NUMBER);
                cs.registerOutParameter("paDescError", OracleTypes.VARCHAR);
                cs.execute();

                int paCodError = cs.getInt("paCodError");

                retorno.setPaCodError(paCodError);
                retorno.setPaDesError(cs.getString("paDescError"));

                if (paCodError == 0) {
                    List<Materia_Grupos_MB> lMaterias = new ArrayList<>();
                    ResultSet rs = (ResultSet) cs.getObject("paCurRetorno");

                    while (rs.next()) {
                        lMaterias.add(new Materia_Grupos_MB(
                                rs.getInt("MATERIA_ID"),
                                rs.getString("NOMBRE_MATERIA"),
                                rs.getInt("CREDITOS_MATERIA"),
                                rs.getString("NOMBRE_CORTO"),
                                rs.getString("ACADEMIA_ID")
                        ));
                    }

                    retorno.setRetorno(lMaterias);

                }
                
            } catch (SQLException ex) {
                logger.registrarErrorSQL(ex, app, mod, user.getNomUsuario());
                retorno.setPaCodError(ex.getErrorCode());
                retorno.setPaDesError(logger.getMensajeError());
            } finally {
                Conexion.cerrarConexion(con);
            }
        } else {
            retorno.setPaDesError(Conexion.getConnectionErrorMessage());
        }
        return retorno;
    }

    /**
     * Descripción: Obtiene el catalogo de los grupos Actuales o Preparación
     * dados de alta según el plan id y la especialidad id
     *
     * @param paPlanId : Identificador del plan de los grupos
     * @param paEspeciaId : Identificador de la especialidad de los grupos
     * @param paOpcionCatalogo : Especifica el periodo deseado de los grupos a
     * obtener (2) = Actuales (3) = Preparación
     * @param user : Contiene los datos del usuario en sesión
     * @param app : Nombre de la aplicación
     * @param mod : Nombre del módulo
     * @return retorno Devuelve un objeto GenericReturn con la lista de Grupos y
     * los parametros de salida del procedimiento
     */
    public static GenericReturn<List<Grupo_Grupos_MB>> getGrupos(int paPlanId, int paEspeciaId, int paOpcionCatalogo, UsuarioMB user, String app, String mod) {
        final Logger logger = new Logger();
        GenericReturn<List<Grupo_Grupos_MB>> retorno = new GenericReturn<>();

        Connection conn = Conexion.getConnection(user.getNomUsuario(), user.getPassUsuario(), app, mod);
        

        if (conn != null) {
            try(CallableStatement cs = conn.prepareCall("{call DEPTOS.PQ_GET_CATALOG_APP_GRUPOS_A.GET_CATALOG_GENERICOS_SP(?,?,?,?,?,?)}")) {
                
                cs.setInt("paPlanId", paPlanId);
                cs.setInt("paEspeciaId", paEspeciaId);
                cs.setInt("paOpcionCatalogo", paOpcionCatalogo);
                cs.registerOutParameter("paCurRetorno", OracleTypes.CURSOR);
                cs.registerOutParameter("paCodError", OracleTypes.NUMBER);
                cs.registerOutParameter("paDescError", OracleTypes.VARCHAR);
                cs.execute();

                retorno.setPaCodError(cs.getInt("paCodError"));
                retorno.setPaDesError(cs.getString("paDescError"));

                if (retorno.getPaCodError() == 0) {
                    List<Grupo_Grupos_MB> lGrupos = new ArrayList<>();
                    ResultSet rs = (ResultSet) cs.getObject("paCurRetorno");

                    switch (paOpcionCatalogo) {
                        case 2://Actuales
                            while (rs.next()) {
                                lGrupos.add(new Grupo_Grupos_MB(
                                        rs.getInt("BLOQUE"),
                                        rs.getInt("GRUPO_ID"),
                                        rs.getString("NOMBRE_MATERIA"),
                                        rs.getString("NOM_MAT_CORTO"),
                                        rs.getInt("CREDITOS"),
                                        rs.getInt("CAPACIDAD"),
                                        rs.getInt("PERIODO"),
                                        rs.getInt("AÑO"),
                                        String.valueOf(rs.getInt("ACAD_IMPARTE_MAT")),
                                        rs.getString("H_LUN"),
                                        rs.getString("H_MAR"),
                                        rs.getString("H_MIE"),
                                        rs.getString("H_JUE"),
                                        rs.getString("H_VIE"),
                                        rs.getString("H_SAB"),//SABADO
                                        rs.getInt("INSCRITOS"),
                                        rs.getInt("INSCRITOS_TOT"),
                                        rs.getInt("TOPE_ALUMNO"),
                                        rs.getString("NOMBRE_PROF")
                                ));
                            }
                            break;
                        case 3:
                            while (rs.next()) {//Preparacion
                                lGrupos.add(new Grupo_Grupos_MB(
                                        rs.getInt("BLOQUE"),
                                        rs.getInt("GRUPO_ID"),
                                        rs.getString("NOMBRE_MATERIA"),
                                        rs.getString("NOM_MAT_CORTO"),
                                        rs.getInt("CREDITOS"),
                                        rs.getInt("CAPACIDAD"),
                                        rs.getInt("PERIODO"),
                                        rs.getInt("AÑO"),
                                        String.valueOf(rs.getInt("ACAD_IMPARTE_MAT")),
                                        rs.getString("H_LUN"),
                                        rs.getString("H_MAR"),
                                        rs.getString("H_MIE"),
                                        rs.getString("H_JUE"),
                                        rs.getString("H_VIE"),
                                        rs.getString("H_SAB")//SABADO
                                ));
                            }
                            break;
                        case 4: //intersemestral
                            while (rs.next()) {
                                lGrupos.add(new Grupo_Grupos_MB(
                                        rs.getInt("BLOQUE"),
                                        rs.getInt("GRUPO_ID"),
                                        rs.getString("NOMBRE_MATERIA"),
                                        rs.getString("NOM_MAT_CORTO"),
                                        rs.getInt("CREDITOS"),
                                        rs.getInt("CAPACIDAD"),
                                        rs.getInt("PERIODO"),
                                        rs.getInt("AÑO"),
                                        String.valueOf(rs.getInt("ACAD_IMPARTE_MAT")),
                                        rs.getString("H_LUN"),
                                        rs.getString("H_MAR"),
                                        rs.getString("H_MIE"),
                                        rs.getString("H_JUE"),
                                        rs.getString("H_VIE"),
                                        rs.getString("H_SAB"),//SABADO
                                        rs.getInt("INSCRITOS"),
                                        rs.getInt("INSCRITOS_TOT"),
                                        rs.getInt("TOPE_ALUMNO"),
                                        rs.getString("NOMBRE_PROF"),
                                        paPlanId,
                                        paEspeciaId
                                ));
                            }
                            break;

                    }
                    retorno.setRetorno(lGrupos);
                }
                
            } catch (SQLException ex) {
                logger.registrarErrorSQL(ex, app, mod, user.getNomUsuario());
                retorno.setPaCodError(ex.getErrorCode());
                retorno.setPaDesError(logger.getMensajeError());
            } finally {
                Conexion.cerrarConexion(conn);
            }
        } else {
            retorno.setPaDesError(Conexion.getConnectionErrorMessage());
        }
        return retorno;
    }

    /**
     * Descripción: Obtiene el catalogo de los grupos compartidos dados de alta
     * en el periodo especificado
     *
     * @param paPlanId : Identicador del plan de los grupos a obtener
     * @param paEspeciaId : Identificador de la especialidad de los grupos a
     * obtener
     * @param paOpcionCatalogo : Opcion de periodo (1): Actuales (2):
     * Preparación
     * @param user : Contiene los datos del usuario en sesión
     * @param app : Nombre de la aplicación
     * @param mod : Nombre del módulo
     * @return retorno Devuelve un objeto GenericReturn con la lista de Grupos
     * Compartidos y los parametros de salida del procedimiento
     */
    public static GenericReturn<List<Grupo_Grupos_MB>> getGruposComp(int paPlanId, int paEspeciaId, int paOpcionCatalogo, UsuarioMB user, String app, String mod) {
        final Logger logger = new Logger();
        GenericReturn<List<Grupo_Grupos_MB>> retorno = new GenericReturn<>();
        Connection con = Conexion.getConnection(user.getNomUsuario(), user.getPassUsuario(), app, mod);
         

        if (con != null) {
            try(CallableStatement cs = con.prepareCall("{call DEPTOS.PQ_GET_CATALOG_APP_GRUPOS_C.GET_CATALOG_GENERICOS_SP(?,?,?,?,?,?)}")) {
                
                cs.setInt("paPlanId", paPlanId);
                cs.setInt("paEspeciaId", paEspeciaId);
                cs.setInt("paOpcionCatalogo", paOpcionCatalogo);
                cs.registerOutParameter("paCurRetorno", OracleTypes.CURSOR);
                cs.registerOutParameter("paCodError", OracleTypes.NUMBER);
                cs.registerOutParameter("paDescError", OracleTypes.VARCHAR);

                cs.execute();

                retorno.setPaCodError(cs.getInt("paCodError"));
                retorno.setPaDesError(cs.getString("paDescError"));
                
                if (retorno.getPaCodError() == 0) {
                    List<Grupo_Grupos_MB> lGrupos = new ArrayList<>();
                    ResultSet rs = (ResultSet) cs.getObject("paCurRetorno");
                    while (rs.next()) {
                        lGrupos.add(new Grupo_Grupos_MB(
                                rs.getInt("BLOQUE"),
                                rs.getInt("GRUPO_ID"),
                                rs.getString("NOMBRE_MATERIA"),
                                rs.getString("NOM_MAT_CORTO"),
                                rs.getInt("CREDITOS"),
                                rs.getInt("CAPACIDAD"),
                                rs.getInt("PERIODO"),
                                rs.getInt("AÑO"),
                                String.valueOf(rs.getInt("ACAD_IMPARTE_MAT")),
                                rs.getString("H_LUN"),
                                rs.getString("H_MAR"),
                                rs.getString("H_MIE"),
                                rs.getString("H_JUE"),
                                rs.getString("H_VIE"),
                                rs.getString("H_SAB"),//SABADO
                                rs.getInt("INSCRITOS"),
                                rs.getInt("TOPE_ALUMNO"),
                                rs.getString("NOMBRE_PROF"),
                                rs.getInt("REAL_ID"),
                                rs.getString("COMPARTIDO")));
                    }
                    retorno.setRetorno(lGrupos);
                }
                
            } catch (SQLException ex) {

                logger.registrarErrorSQL(ex, app, mod, user.getNomUsuario());
                retorno.setPaCodError(ex.getErrorCode());
                retorno.setPaDesError(logger.getMensajeError());

            } finally {
                Conexion.cerrarConexion(con);
            }
        } else {
            retorno.setPaDesError(Conexion.getConnectionErrorMessage());
        }
        return retorno;
    }

    /**
     * Descripción: Obtiene el catalogo de las carreras actuales activas en la
     * base de datos.
     *
     * @param user : Contiene los datos del usuario en sesión
     * @param app : Nombre de la aplicación
     * @param mod : Nombre del módulo
     * @return retorno Devuelve un objeto GenericReturn con la lista de Carreras
     * y los parametros de salida del procedimiento
     */
    public static GenericReturn<List<Carreras_Profesores_MB>> getCarreras(UsuarioMB user, String app, String mod) {
        final Logger logger = new Logger();
        GenericReturn<List<Carreras_Profesores_MB>> retorno = new GenericReturn<>();
        Connection con = Conexion.getConnection(user.getNomUsuario(), user.getPassUsuario(), app, mod);
        

        if (con != null) {
            try(CallableStatement cs = con.prepareCall("{call DEPTOS.PQ_GET_CATALOG_APP_GRUPOS_A.GET_CATALOG_GENERICOS_SP(?,?,?,?,?,?)}")) {
                
                cs.setInt("paPlanId", 0);
                cs.setInt("paEspeciaId", 0);
                cs.setInt("paOpcionCatalogo", 5);

                cs.registerOutParameter("paCurRetorno", OracleTypes.CURSOR);
                cs.registerOutParameter("paCodError", OracleTypes.NUMBER);
                cs.registerOutParameter("paDescError", OracleTypes.VARCHAR);
                cs.execute();

                retorno.setPaCodError(cs.getInt("paCodError"));
                retorno.setPaDesError(cs.getString("paDescError"));

                if (retorno.getPaCodError() == 0) {
                    List<Carreras_Profesores_MB> lCarreras = new ArrayList<>();
                    ResultSet rs = (ResultSet) cs.getObject("paCurRetorno");
                    while (rs.next()) {
                        lCarreras.add(new Carreras_Profesores_MB(
                                rs.getInt("ID"), rs.getString("NOMBRE"), rs.getString("NOM_CTO")));
                    }
                    retorno.setRetorno(lCarreras);
                } 
                
            } catch (SQLException ex) {
                logger.registrarErrorSQL(ex, app, mod, user.getNomUsuario());
                retorno.setPaCodError(ex.getErrorCode());
                retorno.setPaDesError(logger.getMensajeError());
            } finally {
                Conexion.cerrarConexion(con);
            }
        } else {

            retorno.setPaDesError(Conexion.getConnectionErrorMessage());
        }
        return retorno;
    }

    /**
     *
     * Descripción: Obtiene un catalogo de grupos dados de alta según la carrera
     * y el periodo
     *
     * @param paCarreraId : Identificador de la carrera de la cual se requieren
     * los grupos
     * @param paOpcPeriodo : Identificador del Periodo (1): Actuales (2):
     * Preparación
     * @param user : Contiene los datos del usuario en sesión
     * @param app : Nombre de la aplicación
     * @param mod : Nombre del módulo
     * @return retorno Devuelve un objeto GenericReturn con la lista de Grupos
     * por Carrera y los parametros de salida del procedimiento
     */
    public static GenericReturn<List<Grupo_Grupos_MB>> getGruposByCarrera(int paCarreraId, int paOpcPeriodo, UsuarioMB user, String app, String mod) {
        final Logger logger = new Logger();
        GenericReturn<List<Grupo_Grupos_MB>> retorno = new GenericReturn<>();
        Connection con = Conexion.getConnection(user.getNomUsuario(), user.getPassUsuario(), app, mod);
        
        if (con != null) {
            try(CallableStatement cs = con.prepareCall("{call DEPTOS.PQ_GET_CATALOG_APP_GRUPOS_C.GET_GRUPOS_X_CARRERA_SP(?,?,?,?,?)}")) {
                
                cs.setInt("paCarreraId", paCarreraId);
                cs.setInt("paOpcPeriodo", paOpcPeriodo);

                cs.registerOutParameter("paCurRetorno", OracleTypes.CURSOR);
                cs.registerOutParameter("paCodError", OracleTypes.NUMBER);
                cs.registerOutParameter("paDescError", OracleTypes.VARCHAR);

                cs.execute();

                retorno.setPaCodError(cs.getInt("paCodError"));
                retorno.setPaDesError(cs.getString("paDescError"));

                if (retorno.getPaCodError() == 0) {
                    List<Grupo_Grupos_MB> lGrupos = new ArrayList<>();
                    ResultSet rs = (ResultSet) cs.getObject("paCurRetorno");
                    while (rs.next()) {
                        lGrupos.add(new Grupo_Grupos_MB(
                                rs.getInt("BLOQUE"),
                                rs.getInt("GRUPO_ID"),
                                rs.getString("NOMBRE_MATERIA"),
                                rs.getString("NOM_MAT_CORTO"),
                                rs.getInt("CREDITOS"),
                                rs.getInt("CAPACIDAD"),
                                rs.getInt("PERIODO"),
                                rs.getInt("AÑO"),
                                String.valueOf(rs.getInt("ACAD_IMPARTE_MAT")),
                                rs.getString("H_LUN"),
                                rs.getString("H_MAR"),
                                rs.getString("H_MIE"),
                                rs.getString("H_JUE"),
                                rs.getString("H_VIE"),
                                rs.getString("H_SAB"),//SABADO
                                rs.getInt("INSCRITOS"),
                                0,
                                0,
                                rs.getString("NOMBRE_PROF")
                        ));
                    }
                    retorno.setRetorno(lGrupos);
                }
                
            } catch (SQLException ex) {
                logger.registrarErrorSQL(ex, app, mod, user.getNomUsuario());
                retorno.setPaCodError(ex.getErrorCode());
                retorno.setPaDesError(logger.getMensajeError());
            } finally {
                Conexion.cerrarConexion(con);
            }
        } else {
            retorno.setPaDesError(Conexion.getConnectionErrorMessage());
        }
        return retorno;
    }

    /**
     * Descripción: Obtiene el catalogo de aulas disponible para la carrera
     *
     * @param paGrupoId Identificador del grupo que solicita el catalogo
     * @param paCarreraId Identificador de la carrera del grupo
     * @param paDia Identificador del dia
     * @param paCapacidad Capacidad del grupo
     * @param paHor_Ini Hora inicio del grupo en el dia indicado
     * @param paHor_Fin Hora final del grupo en el dia indicado
     * @param user Contiene los datos del usuario en sesión
     * @param app : Nombre de la aplicación
     * @param mod : Nombre del módulo
     * @return retorno : Devuelve un elemento GenericReturn con el catalogo de
     * aulas y los parametro de salida del procedimiento
     */
    public static GenericReturn<List<Aula_Grupos_MB>> getAulas(int paGrupoId, int paCarreraId, int paDia, int paCapacidad, int paHor_Ini, int paHor_Fin, UsuarioMB user, String app, String mod) {
        final Logger logger = new Logger();
        GenericReturn<List<Aula_Grupos_MB>> retorno = new GenericReturn<>();
        
        Connection con = Conexion.getConnection(user.getNomUsuario(), user.getPassUsuario(), app, mod);
        if (con != null) {
            try(CallableStatement cs = con.prepareCall("{call DEPTOS.PQ_CHECK_AULA_GPO_COOR.CHECK_AULA_GRUPO_SP(?,?,?,?,?,?,?,?,?,?)}")) {
                
                cs.setInt("paGrupoId", paGrupoId);
                cs.setInt("paCarreraId", paCarreraId);
                cs.setInt("paDia", paDia);
                cs.setInt("paCapacidad", paCapacidad);
                cs.setInt("paHor_Ini", paHor_Ini);
                cs.setInt("paHor_Fin", paHor_Fin);
                cs.setInt("paOpcionPeriodo", user.getTipoPeriodoUs());
                cs.registerOutParameter("paCurRetorno", OracleTypes.CURSOR);
                cs.registerOutParameter("paCodError", OracleTypes.NUMBER);
                cs.registerOutParameter("paDescError", OracleTypes.VARCHAR);

                cs.execute();

                retorno.setPaCodError(cs.getInt("paCodError"));
                retorno.setPaDesError(cs.getString("paDescError"));
                
                if (retorno.getPaCodError() == 0) {
                    List<Aula_Grupos_MB> lAulas = new ArrayList();
                    ResultSet rs = (ResultSet) cs.getObject("paCurRetorno");
                    while (rs.next()) {
                        lAulas.add(new Aula_Grupos_MB(
                                rs.getString("AULA"), rs.getString("EDIFICIO"), rs.getInt("CAPACIDAD_AULA"), rs.getString("OBS")
                        ));
                    }
                    retorno.setRetorno(lAulas);
                }
                
            } catch (SQLException ex) {
                logger.registrarErrorSQL(ex, app, mod, user.getNomUsuario());
                retorno.setPaCodError(ex.getErrorCode());
                retorno.setPaDesError(logger.getMensajeError());
            } finally {
                Conexion.cerrarConexion(con);
            }
        } else {
            retorno.setPaDesError(Conexion.getConnectionErrorMessage());
        }
        return retorno;
    }
}
