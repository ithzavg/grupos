/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao.asignarprofesor;

import bean.asignarprofesor.Profesor_Profesores_MB;
import bean.general.GenericReturn;
import bean.grupos.Grupo_Grupos_MB;
import bean.session.UsuarioMB;
import itt.web.conexion.Conexion;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import mx.edu.ittoluca.logutils.Logger;
import oracle.jdbc.OracleTypes;
import oracle.sql.ARRAY;
import oracle.sql.ArrayDescriptor;

/**
 *
 * @author Edgar
 */
public class Profesor_Profesores_DAO {

    /**
     * Descripción: Obtiene el catalogo de profesores activos en la base de
     * datos y muestra si el profesor puede tomar el grupo, en caso de no ser
     * posible, indica el porqué no puede ser asignado
     *
     * @param paAcademiaId : Identificador de la academia del profesor
     * @param paGrupoId : Identificador del grupo a asignar el profesor
     * @param paPeriodo : Periodo del grupo
     * @param paAño : Año del grupo
     * @param user : Contiene los datos del usuario en sesión
     * @param app : Nombre de la aplicación
     * @param mod : Nombre del módulo
     * @return retorno : Devuelve la lista de Profesor_Profesores_MB dentro de
     * un objeto GenericReturn con el codigo de error y la descripcion del
     * mismo, indicando si pueden o no tomar el grupo, y especificando el
     * porqué.
     */
    public static GenericReturn<List<Profesor_Profesores_MB>> getProfesores(int paAcademiaId, int paGrupoId, int paPeriodo, int paAño, UsuarioMB user, String app, String mod) {
        final Logger logger = new Logger();
        GenericReturn<List<Profesor_Profesores_MB>> retorno = new GenericReturn<>();
        
        Connection con = Conexion.getConnection(user.getNomUsuario(), user.getPassUsuario(), app, mod);

        if (con != null) {
            try(CallableStatement cs = con.prepareCall("{call DEPTOS.PQ_CHECK_PROF_GPO_JDPTO.CHECK_PROF_GRUPO_SP(?,?,?,?,?,?,?)}")) {
                
                cs.setInt("paAcademiaId", paAcademiaId);
                cs.setInt("paGrupoId", paGrupoId);
                cs.setInt("paPeriodo", paPeriodo);
                cs.setInt("paAño", paAño);

                cs.registerOutParameter("paCurRetorno", OracleTypes.CURSOR);
                cs.registerOutParameter("paCodError", OracleTypes.NUMBER);
                cs.registerOutParameter("paDescError", OracleTypes.VARCHAR);

                cs.execute();

                retorno.setPaCodError(cs.getInt("paCodError"));
                retorno.setPaDesError(cs.getString("paDescError"));
                
                if (retorno.getPaCodError() == 0) {
                    List<Profesor_Profesores_MB> lProfes = new ArrayList<>();
                    ResultSet rs = (ResultSet) cs.getObject("paCurRetorno");
                    while (rs.next()) {
                        lProfes.add(new Profesor_Profesores_MB(
                                rs.getInt("PRO_ID"),
                                rs.getString("NOMBRE_PROF"),                                
                                rs.getInt("ACADEMIA_ID"),
                                rs.getString("HOR_OCUP"),
                                rs.getString("OBS"),0));
                    }
                    retorno.setRetorno(lProfes);
                }
                
            } catch (SQLException ex) {
                logger.registrarErrorSQL(ex, app, mod, user.getNomUsuario());
                retorno.setPaCodError(ex.getErrorCode());
                retorno.setPaDesError(logger.getMensajeError());
            } finally {
                Conexion.cerrarConexion(con);
            }
        } else {
            retorno.setPaDesError(Conexion.getConnectionErrorMessage());
        }
        return retorno;
    }

    /**
     * Descripción : Obtiene catálogo de profesores respecto a la academia
     * indicando las horas asignadas en el periodo y el año indicados.
     *
     * @param paAcademiaId : Indicador de la academia de los profesores
     * @param paPeriodo : Periodo en el cual se desea obtener información del
     * profesor
     * @param paAño : Año en el cual se desea obtener información del profesor
     * @param user : Contiene los datos del usuario en sesión
     * @param app : Nombre de la aplicación
     * @param mod : Nombre del módulo
     * @return lProfesores : Devuelve una lista de Profesor_Profesores_MB
     * indicando horas asignadas del profesor, dentro de un objeto
     * GenericReturn.
     */
    public static GenericReturn<List<Profesor_Profesores_MB>> getProfesores(int paAcademiaId, int paPeriodo, int paAño, UsuarioMB user, String app, String mod) {
        final Logger logger = new Logger();
        GenericReturn<List<Profesor_Profesores_MB>> retorno = new GenericReturn<>();
        
        Connection con = Conexion.getConnection(user.getNomUsuario(), user.getPassUsuario(), app, mod);

        if (con != null) {
            try(CallableStatement cs = con.prepareCall("{call DEPTOS.PQ_GET_CATALOG_APP_GRUPOS_B.GET_CATALOG_GENERICOS_B_SP(?,?,?,?,?,?,?,?,?)}")) {
                
                cs.setInt("paAcademiaId", paAcademiaId);
                cs.setInt("paPeriodo", paPeriodo);
                cs.setInt("paAño", paAño);
                cs.setInt("paCarreraId", 0);
                cs.setInt("paPlanId", 0);
                cs.setInt("paOpcionReq", 1);

                cs.registerOutParameter("paCurRetorno", OracleTypes.CURSOR);
                cs.registerOutParameter("paCodError", OracleTypes.NUMBER);
                cs.registerOutParameter("paDescError", OracleTypes.VARCHAR);
                cs.execute();

                retorno.setPaCodError(cs.getInt("paCodError"));
                retorno.setPaDesError(cs.getString("paDescError"));

                
                if (retorno.getPaCodError() == 0) {
                    List<Profesor_Profesores_MB> lProfes = new ArrayList<>();
                    ResultSet rs = (ResultSet) cs.getObject("paCurRetorno");
                    while (rs.next()) {
                        Profesor_Profesores_MB profesor = new Profesor_Profesores_MB(
                                rs.getInt("PRO_ID"),
                                rs.getString("NOMBRE_PROF"),
                                rs.getInt("ACADEMIA_ID"),
                                rs.getString("HORAS_ASIGNADAS"),
                                rs.getString("HORAS_OCUPADAS"));
                        lProfes.add(profesor);
                    }
                    retorno.setRetorno(lProfes);
                }
                
            } catch (SQLException ex) {
                logger.registrarErrorSQL(ex, app, mod, user.getNomUsuario());
                retorno.setPaCodError(ex.getErrorCode());
                retorno.setPaDesError(logger.getMensajeError());
            } finally {
                Conexion.cerrarConexion(con);
            }
        } else {
            retorno.setPaDesError(Conexion.getConnectionErrorMessage());
        }

        return retorno;
    }

    /**
     *
     * Descripción: Asigna un profesor a un grupo en el periodo y año
     * especificado
     *
     * @param paGrupoId : Identificador del grupo al cual será asignado el
     * profesor
     * @param paProfId : Identificador del profesor que será asignado
     * @param paPeriodo : Periodo del grupo
     * @param paAño : Año del grupo
     * @param user : Contiene los datos del usuario en sesión
     * @param app : Nombre de la aplicación
     * @param mod : Nombre del módulo
     * @return flag : Devuelve una bandera verdadera que indica que todo fue
     * exitoso, bandera falsa, ocurrio un error.
     */
    public static GenericReturn guardarProfesor(int paGrupoId, int paProfId, int paPeriodo, int paAño, UsuarioMB user, String app, String mod) {
        final Logger logger = new Logger();
        GenericReturn retorno = new GenericReturn();
        Connection conn = Conexion.getConnection(user.getNomUsuario(), user.getPassUsuario(), app, mod);
        

        if (conn != null) {
            try(CallableStatement cs = conn.prepareCall("{call DEPTOS.PQ_DML_APP_GRUPOS_JDPTO.UPDATE_GPOS_JF_DEPTO_SP(?,?,?,?,?,?)}")) {
                
                cs.setInt("paGrupoId", paGrupoId);
                cs.setInt("paProfId", paProfId);
                cs.setInt("paPeriodo", paPeriodo);
                cs.setInt("paAño", paAño);

                cs.registerOutParameter("paCodError", OracleTypes.NUMBER);
                cs.registerOutParameter("paDescError", OracleTypes.VARCHAR);

                cs.execute();

                retorno.setPaCodError(cs.getInt("paCodError"));
                retorno.setPaDesError(cs.getString("paDescError"));

                
            } catch (SQLException ex) {
                logger.registrarErrorSQL(ex, app, mod, user.getNomUsuario());
                retorno.setPaCodError(ex.getErrorCode());
                retorno.setPaDesError(logger.getMensajeError());
            } finally {
                Conexion.cerrarConexion(conn);
            }
        } else {
            retorno.setPaDesError(Conexion.getConnectionErrorMessage());
        }
        return retorno;
    }

    /**
     *
     * @param paProfId
     * @param paPeriodo
     * @param paAño
     * @param paHorasAsign
     * @param paArrayLunes
     * @param paArrayMartes
     * @param paArrayMiercoles
     * @param paArrayJueves
     * @param paArrayViernes
     * @param paArraySabado
     * @param user
     * @param app
     * @param mod
     * @return
     */
    public static GenericReturn guardarHorarios(int paProfId, int paPeriodo, int paAño, int paHorasAsign, String[] paArrayLunes, String[] paArrayMartes, String[] paArrayMiercoles, String[] paArrayJueves, String[] paArrayViernes, String[] paArraySabado, UsuarioMB user, String app, String mod) {
        final Logger logger = new Logger();
        GenericReturn retorno = new GenericReturn();
        Connection conn = Conexion.getConnection(user.getNomUsuario(), user.getPassUsuario(), app, mod);
        CallableStatement cs;

        if (conn != null) {
            try {
                ArrayDescriptor arrayd = ArrayDescriptor.createDescriptor("DEPTOS.ARRAY_APP_GRUPOS", conn);

                ARRAY paArrayLun = new ARRAY(arrayd, conn, paArrayLunes);
                ARRAY paArrayMar = new ARRAY(arrayd, conn, paArrayMartes);
                ARRAY paArrayMie = new ARRAY(arrayd, conn, paArrayMiercoles);
                ARRAY paArrayJue = new ARRAY(arrayd, conn, paArrayJueves);
                ARRAY paArrayVie = new ARRAY(arrayd, conn, paArrayViernes);
                ARRAY paArraySab = new ARRAY(arrayd, conn, paArraySabado);
                cs = conn.prepareCall("{call DEPTOS.PQ_DML_APP_GRUPOS_JDPTO.INSERT_HORARIO_PROF_SP(?,?,?,?,?,?,?,?,?,?,?,?)}");

                cs.setInt(1, paProfId);
                cs.setInt(2, paPeriodo);
                cs.setInt(3, paAño);
                cs.setInt(4, paHorasAsign);
                cs.setArray(5, paArrayLun);
                cs.setArray(6, paArrayMar);
                cs.setArray(7, paArrayMie);
                cs.setArray(8, paArrayJue);
                cs.setArray(9, paArrayVie);
                cs.setArray(10, paArraySab); //SABADO

                cs.registerOutParameter(11, OracleTypes.NUMBER);
                cs.registerOutParameter(12, OracleTypes.VARCHAR);

                cs.execute();

                retorno.setPaCodError(cs.getInt(11));
                retorno.setPaDesError(cs.getString(12));

                cs.close();
            } catch (SQLException ex) {
                logger.registrarErrorSQL(ex, app, mod, user.getNomUsuario());
                retorno.setPaCodError(ex.getErrorCode());
                retorno.setPaDesError(logger.getMensajeError());
            } finally {
                Conexion.cerrarConexion(conn);
            }
        } else {
            retorno.setPaDesError(Conexion.getConnectionErrorMessage());
        }
        return retorno;
    }

    /**
     *
     * @param paProfId
     * @param paPeriodo
     * @param paAño
     * @param user
     * @param app
     * @param mod
     * @return
     */
    public static GenericReturn<List<List<String>>> getHorarioProfesor(int paProfId, int paPeriodo, int paAño, UsuarioMB user, String app, String mod) {
        List<List<String>> lhorarios;
        final Logger logger = new Logger();
        GenericReturn<List<List<String>>> retorno = new GenericReturn<>();
        
        Connection con = Conexion.getConnection(user.getNomUsuario(), user.getPassUsuario(), app, mod);

        if (con != null) {
            try(CallableStatement cs = con.prepareCall("{call DEPTOS.PQ_GET_CATALOG_APP_GRUPOS_B.GET_HORARIO_PROFESORES_SP(?,?,?,?,?,?,?,?,?,?,?)}")) {
               
                cs.setInt(1, paProfId);
                cs.setInt(2, paPeriodo);
                cs.setInt(3, paAño);

                cs.registerOutParameter(4, OracleTypes.CURSOR);
                cs.registerOutParameter(5, OracleTypes.CURSOR);
                cs.registerOutParameter(6, OracleTypes.CURSOR);
                cs.registerOutParameter(7, OracleTypes.CURSOR);
                cs.registerOutParameter(8, OracleTypes.CURSOR);
                cs.registerOutParameter(9, OracleTypes.CURSOR);
                cs.registerOutParameter(10, OracleTypes.NUMBER);
                cs.registerOutParameter(11, OracleTypes.VARCHAR);
                cs.execute();
                retorno.setPaCodError(cs.getInt(10));

                if (retorno.getPaCodError() == 0) {

                    lhorarios = new ArrayList<>();
                    for (int i = 4; i < 10; i++) {
                        lhorarios.add(getHorario((ResultSet) cs.getObject(i)));
                    }

                    retorno.setRetorno(lhorarios);
                } else {
                    retorno.setPaDesError(cs.getString(10));
                }                
            } catch (SQLException ex) {
                logger.registrarErrorSQL(ex, app, mod, user.getNomUsuario());
                retorno.setPaCodError(ex.getErrorCode());
                retorno.setPaDesError(logger.getMensajeError());
            } finally {
                Conexion.cerrarConexion(con);
            }
        } else {
            retorno.setPaDesError(Conexion.getConnectionErrorMessage());
        }
        return retorno;
    }

    /**
     *
     * @param paPlanId
     * @param paEspeciaId
     * @param paAcademiaId
     * @param paOpcionCatalogo
     * @param user
     * @param app
     * @param mod
     * @return
     */
    public static GenericReturn<List<Grupo_Grupos_MB>> getGruposJefDepto(int paPlanId, int paEspeciaId, int paAcademiaId, int paOpcionCatalogo, UsuarioMB user, String app, String mod) {
        final Logger logger = new Logger();
        List<Grupo_Grupos_MB> lGrupos;
        GenericReturn<List<Grupo_Grupos_MB>> retorno = new GenericReturn<>();
        Connection con = Conexion.getConnection(user.getNomUsuario(), user.getPassUsuario(), app, mod);
        

        if (con != null) {
            try(CallableStatement cs = con.prepareCall("{call DEPTOS.PQ_GET_CATALOG_APP_GRUPOS_B.GET_GRUPOS_JEFE_DPTO_SP(?,?,?,?,?,?,?)}")) {
                
                cs.setInt("paPlanId", paPlanId);
                cs.setInt("paEspeciaId", paEspeciaId);
                cs.setInt("paAcademiaId", paAcademiaId);
                cs.setInt("paOpcionCatalogo", paOpcionCatalogo);

                cs.registerOutParameter("paCurRetorno", OracleTypes.CURSOR);
                cs.registerOutParameter("paCodError", OracleTypes.NUMBER);
                cs.registerOutParameter("paDescError", OracleTypes.VARCHAR);

                cs.execute();

                retorno.setPaCodError(cs.getInt("paCodError"));
                retorno.setPaDesError(cs.getString("paDescError"));

                if (retorno.getPaCodError() == 0) {
                    lGrupos = new ArrayList<>();
                    ResultSet rs = (ResultSet) cs.getObject("paCurRetorno");
                    while (rs.next()) {
                        lGrupos.add(new Grupo_Grupos_MB(
                                rs.getInt("BLOQUE"),
                                rs.getInt("GRUPO_ID"),
                                rs.getString("NOMBRE_MATERIA"),
                                rs.getString("NOM_MAT_CORTO"),
                                rs.getInt("CREDITOS"),
                                rs.getInt("CAPACIDAD"),
                                rs.getInt("PERIODO"),
                                rs.getInt("AÑO"),
                                String.valueOf(rs.getInt("ACAD_IMPARTE_MAT")),
                                rs.getString("H_LUN"),
                                rs.getString("H_MAR"),
                                rs.getString("H_MIE"),
                                rs.getString("H_JUE"),
                                rs.getString("H_VIE"),
                                rs.getString("H_SAB"),//SABADO
                                rs.getInt("INSCRITOS"),
                                0,
                                rs.getInt("TOPE_ALUMNO"),
                                rs.getString("NOMBRE_PROF")
                        ));
                    }
                    retorno.setRetorno(lGrupos);
                }
                
            } catch (SQLException ex) {
                logger.registrarErrorSQL(ex, app, mod, user.getNomUsuario());
                retorno.setPaCodError(ex.getErrorCode());
                retorno.setPaDesError(logger.getMensajeError());
            } finally {
                Conexion.cerrarConexion(con);
            }
        } else {
            retorno.setPaDesError(Conexion.getConnectionErrorMessage());
        }
        return retorno;
    }

    /**
     *
     * @param paHorAsig
     * @param paProfId
     * @param paPeriodo
     * @param paAño
     * @param user
     * @param app
     * @param mod
     * @return
     */
    public static GenericReturn updateHorAsigProfesor(int paHorAsig, int paProfId, int paPeriodo, int paAño, UsuarioMB user, String app, String mod) {
        final Logger logger = new Logger();
        GenericReturn retorno = new GenericReturn();
        
        Connection con = Conexion.getConnection(user.getNomUsuario(), user.getPassUsuario(), app, mod);

        if (con != null) {
            try(CallableStatement cs = con.prepareCall("{call DEPTOS.PQ_DML_APP_GRUPOS_JDPTO.UPDATE_HOR_ASIG_PROF_SP(?,?,?,?,?,?)}")) {
                
                cs.setInt("paHorAsig", paHorAsig);
                cs.setInt("paProfId", paProfId);
                cs.setInt("paPeriodo", paPeriodo);
                cs.setInt("paAño", paAño);

                cs.registerOutParameter("paCodError", OracleTypes.NUMBER);
                cs.registerOutParameter("paDescError", OracleTypes.VARCHAR);

                cs.execute();

                retorno.setPaCodError(cs.getInt("paCodError"));
                retorno.setPaDesError(cs.getString("paDescError"));

                
            } catch (SQLException ex) {
                logger.registrarErrorSQL(ex, app, mod, user.getNomUsuario());
                retorno.setPaCodError(ex.getErrorCode());
                retorno.setPaDesError(logger.getMensajeError());
            } finally {
                Conexion.cerrarConexion(con);
            }

        } else {
            retorno.setPaDesError(Conexion.getConnectionErrorMessage());
        }
        return retorno;
    }

    private static List<String> getHorario(ResultSet rs) throws SQLException {
        List<String> l = new ArrayList();
        while (rs.next()) {
            l.add(rs.getString(1));
        }
        return l;
    }

}
