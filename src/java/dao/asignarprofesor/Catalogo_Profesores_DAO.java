/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao.asignarprofesor;

import bean.asignarprofesor.Academia_Profesores_MB;
import bean.general.Carreras_Profesores_MB;
import bean.general.Especialidad_Grupos_MB;
import bean.general.GenericReturn;
import bean.general.Plan_Grupos_MB;
import bean.session.UsuarioMB;
import itt.web.conexion.Conexion;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import mx.edu.ittoluca.logutils.Logger;
import oracle.jdbc.OracleTypes;

/**
 *
 * @author Edgar Cirilo González
 */
public class Catalogo_Profesores_DAO {

    /**
     * Descripción: Obtiene el catalogo de academias que estan activas en la
     * base de datos
     *
     * @param user : Contiene los datos del usuario en sesion
     * @param app : Nombre de la aplicación
     * @param mod : Nombre del módulo
     * @return retorno : Devuelve la lista de las academias dentro de un objeto
     * GenericReturn
     */
    public static GenericReturn<List<Academia_Profesores_MB>> getAcademias(UsuarioMB user, String app, String mod) {

        final Logger logger = new Logger();
        GenericReturn<List<Academia_Profesores_MB>> retorno = new GenericReturn<>();
        Connection conn = Conexion.getConnection(user.getNomUsuario(), user.getPassUsuario(), app, mod);

        if (conn != null) {
            try (CallableStatement cs = conn.prepareCall("{call DEPTOS.PQ_GET_CATALOG_APP_GRUPOS_B.GET_ACADEMIAS_SP(?,?,?)}")) {

                cs.registerOutParameter("paCurRetorno", OracleTypes.CURSOR);
                cs.registerOutParameter("paCodError", OracleTypes.NUMBER);
                cs.registerOutParameter("paDescError", OracleTypes.VARCHAR);

                cs.execute();

                retorno.setPaCodError(cs.getInt("paCodError"));
                retorno.setPaDesError(cs.getString("paDescError"));
                if (retorno.getPaCodError() == 0) {
                    List<Academia_Profesores_MB> lAcademia = new ArrayList<>();
                    ResultSet rs = (ResultSet) cs.getObject("paCurRetorno");
                    while (rs.next()) {
                        lAcademia.add(new Academia_Profesores_MB(
                                rs.getInt("ID"),
                                rs.getString("NIVEL"),
                                rs.getString("NOM_CTO"),
                                rs.getString("NOMBRE")
                        ));
                    }
                    retorno.setRetorno(lAcademia);
                } 

            } catch (SQLException ex) {
                logger.registrarErrorSQL(ex, app, mod, user.getNomUsuario());
                retorno.setPaCodError(ex.getErrorCode());
                retorno.setPaDesError(logger.getMensajeError());
            }finally{
                Conexion.cerrarConexion(conn);
            }
        } else {
            retorno.setPaDesError(Conexion.getConnectionErrorMessage());
        }

        return retorno;
    }

    /**
     *
     * Descripción : Obtiene el catalogo de las carreras indicando si falta
     * atender alguna asignación de profesor a algun grupo en el periodo y año
     * indicado segun la academia del Jefe de Departamento.
     *
     * @param paAcademiaId : Identificador de la academia
     * @param paPeriodo : Periodo donde se requiere obtener información de la
     * carrera
     * @param paAño : Año donde se requiere obtener información de la carrera
     * @param user : Contiene los datos del usuario en sesion
     * @param app : Nombre de la aplicación
     * @param mod : Nombre del módulo
     * @return retorno : Devuelve la lista de Carreras_Profesores_MB con un
     * indicador, dentro de un objeto GenericReturn
     */
    public static GenericReturn<List<Carreras_Profesores_MB>> getCarreras(int paAcademiaId, int paPeriodo, int paAño, UsuarioMB user, String app, String mod) {
        GenericReturn<List<Carreras_Profesores_MB>> retorno = new GenericReturn<>();
        final Logger logger = new Logger();
        Connection conn = Conexion.getConnection(user.getNomUsuario(), user.getPassUsuario(), app, mod);
        

        if (conn != null) {
            try(CallableStatement cs = conn.prepareCall("{call DEPTOS.PQ_GET_CATALOG_APP_GRUPOS_B.GET_CATALOG_GENERICOS_B_SP(?,?,?,?,?,?,?,?,?)}")) {
                
                cs.setInt("paAcademiaId", paAcademiaId);
                cs.setInt("paPeriodo", paPeriodo);
                cs.setInt("paAño", paAño);
                cs.setInt("paCarreraId", 0);
                cs.setInt("paPlanId", 0);
                cs.setInt("paOpcionReq", 2);

                cs.registerOutParameter("paCurRetorno", OracleTypes.CURSOR);
                cs.registerOutParameter("paCodError", OracleTypes.NUMBER);
                cs.registerOutParameter("paDescError", OracleTypes.VARCHAR);
                cs.execute();

                retorno.setPaCodError(cs.getInt("paCodError"));
                retorno.setPaDesError(cs.getString("paDescError"));

                if (retorno.getPaCodError() == 0) {
                    List<Carreras_Profesores_MB> lCarreras = new ArrayList<>();
                    ResultSet rs = (ResultSet) cs.getObject("paCurRetorno");
                    while (rs.next()) {
                        lCarreras.add(new Carreras_Profesores_MB(
                                rs.getInt("ID")
                               ,rs.getString("NOMBRE")
                               ,rs.getString("NOM_CTO")
                               ,rs.getInt("PENDIENTES")));
                    }
                    retorno.setRetorno(lCarreras);
                }
                
            } catch (SQLException ex) {
                logger.registrarErrorSQL(ex, app, mod, user.getNomUsuario());
                retorno.setPaCodError(ex.getErrorCode());
                retorno.setPaDesError(logger.getMensajeError());
            }finally{
                Conexion.cerrarConexion(conn);
            }
        } else {
            retorno.setPaDesError(Conexion.getConnectionErrorMessage());
        }
        return retorno;
    }

    /**
     * Descripción: Obtiene el catalogo de planes de la carrera indicada, donde
     * se especifica que planes hacen falta por atender una asignación de
     * profesor en el periodo y año indicado.
     *
     * @param paAcademiaId : Identificador de la academia
     * @param paPeriodo : Periodo donde se requiere obtener información de los
     * planes
     * @param paAño : Año donde se requiere obtener información de los planes
     * @param paCarreraId : Identificador de la carrera
     * @param user : Contiene los datos del usuario en sesion
     * @param app : Nombre de la aplicación
     * @param mod : Nombre del módulo
     * @return retorno : Devuelve una lista de Plan_Grupos_MB con un indicador,
     * dentro de un objeto GenericReturn
     */
    public static GenericReturn<List<Plan_Grupos_MB>> getPlanes(int paAcademiaId, int paPeriodo, int paAño, int paCarreraId, UsuarioMB user, String app, String mod) {
        final Logger logger = new Logger();
        GenericReturn<List<Plan_Grupos_MB>> retorno = new GenericReturn<>();
        Connection conn = Conexion.getConnection(user.getNomUsuario(), user.getPassUsuario(), app, mod);
        

        if (conn != null) {
            try(CallableStatement cs = conn.prepareCall("{call DEPTOS.PQ_GET_CATALOG_APP_GRUPOS_B.GET_CATALOG_GENERICOS_B_SP(?,?,?,?,?,?,?,?,?)}")) {
                
                cs.setInt("paAcademiaId", paAcademiaId);
                cs.setInt("paPeriodo", paPeriodo);
                cs.setInt("paAño", paAño);
                cs.setInt("paCarreraId", paCarreraId);
                cs.setInt("paPlanId", 0);
                cs.setInt("paOpcionReq", 3);

                cs.registerOutParameter("paCurRetorno", OracleTypes.CURSOR);
                cs.registerOutParameter("paCodError", OracleTypes.NUMBER);
                cs.registerOutParameter("paDescError", OracleTypes.VARCHAR);
                cs.execute();

                retorno.setPaCodError(cs.getInt("paCodError"));
                retorno.setPaDesError(cs.getString("paDescError"));

                if (retorno.getPaCodError() == 0) {
                    List<Plan_Grupos_MB> lPlanes = new ArrayList<>();
                    ResultSet rs = (ResultSet) cs.getObject("paCurRetorno");
                    while (rs.next()) {
                        lPlanes.add(new Plan_Grupos_MB(
                                rs.getInt("PLAN_ID")
                               ,rs.getString("CLAVE")
                               ,rs.getString("COMPETENCIAS").charAt(0)
                               ,rs.getInt("PENDIENTES")));
                    }
                    retorno.setRetorno(lPlanes);
                } 
                
            } catch (SQLException ex) {
                logger.registrarErrorSQL(ex, app, mod, user.getNomUsuario());
                retorno.setPaCodError(ex.getErrorCode());
                retorno.setPaDesError(logger.getMensajeError());
            }finally{
                Conexion.cerrarConexion(conn);
            }
        } else {
            retorno.setPaDesError(Conexion.getConnectionErrorMessage());
        }
        return retorno;
    }

    /**
     * Descripción: Obtiene la lista de especialidades que pertenecen al plan
     * que se le envia, se indica en cada especialidad si hacen falta grupos por
     * asignar profesor.
     *
     * @param paPeriodo : Periodo donde se requiere obtener información de los
     * planes
     * @param paAño : Año donde se requiere obtener información de los planes
     * @param paPlanId : Plan al que pertenecen las materias
     * @param user : Contiene los datos del usuario en sesion
     * @param app : Nombre de la aplicación
     * @param mod : Nombre del módulo
     * @return retorno : Devuelve un objeto GenericReturn con la lista de
     * especialidades y los datos de error del procedimiento
     */
    public static GenericReturn<List<Especialidad_Grupos_MB>> getEspecialidades(int paPeriodo, int paAño, int paPlanId, UsuarioMB user, String app, String mod) {
        final Logger logger = new Logger();
        GenericReturn<List<Especialidad_Grupos_MB>> retorno = new GenericReturn<>();
        Connection conn = Conexion.getConnection(user.getNomUsuario(), user.getPassUsuario(), app, mod);
        

        if (conn != null) {
            try(CallableStatement cs = conn.prepareCall("{call DEPTOS.PQ_GET_CATALOG_APP_GRUPOS_B.GET_CATALOG_GENERICOS_B_SP(?,?,?,?,?,?,?,?,?)}")) {
                
                cs.setInt("paAcademiaId", user.getAcademiaUs());
                cs.setInt("paPeriodo", paPeriodo);
                cs.setInt("paAño", paAño);
                cs.setInt("paCarreraId", user.getCarreraUS());
                cs.setInt("paPlanId", paPlanId);
                cs.setInt("paOpcionReq", 4);

                cs.registerOutParameter("paCurRetorno", OracleTypes.CURSOR);
                cs.registerOutParameter("paCodError", OracleTypes.NUMBER);
                cs.registerOutParameter("paDescError", OracleTypes.VARCHAR);
                cs.execute();

                retorno.setPaCodError(cs.getInt("paCodError"));
                retorno.setPaDesError(cs.getString("paDescError"));

                if (retorno.getPaCodError() == 0) {
                    List<Especialidad_Grupos_MB> lEspecialidad = new ArrayList<>();
                    ResultSet rs = (ResultSet) cs.getObject("paCurRetorno");
                    while (rs.next()) {
                        lEspecialidad.add(
                                new Especialidad_Grupos_MB(rs.getInt("ESP_ID")
                                                          ,rs.getString("NOMBRE_ESPEC")
                                                          ,rs.getInt("PENDIENTES")));
                                                
                    }
                    retorno.setRetorno(lEspecialidad);
                }
                
            } catch (SQLException ex) {
                logger.registrarErrorSQL(ex, app, mod, user.getNomUsuario());
                retorno.setPaCodError(ex.getErrorCode());
                retorno.setPaDesError(logger.getMensajeError());
            }finally{            
                Conexion.cerrarConexion(conn);
            }
        } else {

            retorno.setPaDesError(Conexion.getConnectionErrorMessage());
        }
        return retorno;
    }
}
