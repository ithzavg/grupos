/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao.session;

import bean.general.GenericReturn;
import bean.session.UsuarioMB;
import itt.web.conexion.Conexion;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import mx.edu.ittoluca.logutils.Logger;
import oracle.jdbc.OracleTypes;

/**
 *
 * @author Edgar
 */
public class UserDAO {

    public static GenericReturn<UsuarioMB> getUsuarioGpos(String usuario, String password, String app, String mod) {
        final Logger logger = new Logger();
        GenericReturn<UsuarioMB> retorno = new GenericReturn<>();
        UsuarioMB user = new UsuarioMB();
        Connection con = Conexion.getConnection(usuario, password, app, mod);
        CallableStatement cs;

        if (con != null) {
            try {

                cs = con.prepareCall("{call SIA.INFO_USUARIOS_ORACLE.GET_GRUPOS_INFO_USER_SP(?,?,?,?,?,?,?)}");//adaptar con distancia

                cs.registerOutParameter("paAcademia", OracleTypes.INTEGER);
                cs.registerOutParameter("paCarrera", OracleTypes.INTEGER);
                cs.registerOutParameter("paNombreCarrera", OracleTypes.VARCHAR);
                cs.registerOutParameter("paTipoUsuario", OracleTypes.INTEGER);
                cs.registerOutParameter("paModalidad", OracleTypes.VARCHAR);
                cs.registerOutParameter("paCodError", OracleTypes.INTEGER);
                cs.registerOutParameter("paDescError", OracleTypes.VARCHAR);
                cs.execute();

                retorno.setPaCodError(cs.getInt("paCodError"));

                if (retorno.getPaCodError() == 0) {

                    user.setAcademiaUs(cs.getInt("paAcademia"));
                    user.setCarreraUS(cs.getInt("paCarrera"));
                    user.setNombreCarrera(cs.getString("paNombreCarrera"));
                    user.setTipoUs(cs.getInt("paTipoUsuario"));
                    user.setNomUsuario(usuario);
                    user.setPassUsuario(password);
                    user.setModalidad(cs.getString("paModalidad").equals("D") ? 1 : 0);//Quitar cuando se obtenga del SP
                    retorno.setRetorno(user);
                } else {
                    if (retorno.getPaCodError() == 100) {
                        retorno.setPaDesError("El usuario no se encuentra registrado.");
                    } else {
                        retorno.setPaDesError(cs.getString("paDescError"));
                    }
                }
                cs.close();
            } catch (SQLException ex) {
                logger.registrarErrorSQL(ex, app, mod, usuario);
                retorno.setPaCodError(ex.getErrorCode());
                retorno.setPaDesError(logger.getMensajeError());
            } finally {
                Conexion.cerrarConexion(con);
            }
        } else {
            retorno.setPaDesError(Conexion.getConnectionErrorMessage());
        }

        return retorno;
    }

    public static GenericReturn<String> getAcademiaJefe(UsuarioMB usuario, String app, String mod) {
        final Logger logger = new Logger();
        GenericReturn<String> retorno = new GenericReturn<>();
        Connection con = Conexion.getConnection(usuario.getNomUsuario(), usuario.getPassUsuario(), app, mod);
        CallableStatement cs;

        if (con != null) {
            try {

                cs = con.prepareCall("{call SIA.INFO_USUARIOS_ORACLE.GET_INFO_USUARIOS_ORCL_SP(?,?,?,?,?,?,?,?,?,?)}");

                cs.registerOutParameter("paNombreUsuOcl", OracleTypes.VARCHAR);
                cs.registerOutParameter("paDueñoUsuOcl", OracleTypes.VARCHAR);
                cs.registerOutParameter("paAcadId", OracleTypes.NUMBER);
                cs.registerOutParameter("paNombreAcademia", OracleTypes.VARCHAR);
                cs.registerOutParameter("paPuestoId", OracleTypes.INTEGER);
                cs.registerOutParameter("paNombrePuesto", OracleTypes.VARCHAR);
                cs.registerOutParameter("paIdCarrera", OracleTypes.INTEGER);
                cs.registerOutParameter("paNombreCarrera", OracleTypes.VARCHAR);
                cs.registerOutParameter("paCodError", OracleTypes.INTEGER);
                cs.registerOutParameter("paMsjError", OracleTypes.VARCHAR);

                cs.execute();
                retorno.setPaCodError(cs.getInt("paCodError"));

                if (retorno.getPaCodError() == 0) {
                    retorno.setRetorno(cs.getString("paNombreAcademia"));
                } else {
                    retorno.setPaDesError(cs.getString("paMsjError"));
                }
                cs.close();
            } catch (SQLException ex) {
                logger.registrarErrorSQL(ex, app, mod, usuario.getNomUsuario());
                retorno.setPaCodError(ex.getErrorCode());
                retorno.setPaDesError(logger.getMensajeError());

            } finally {
                Conexion.cerrarConexion(con);
            }
        } else {
            retorno.setPaDesError(Conexion.getConnectionErrorMessage());
        }
        return retorno;
    }
}
