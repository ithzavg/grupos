/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao.general;

import bean.general.Especialidad_Grupos_MB;
import bean.general.GenericReturn;
import bean.general.Plan_Grupos_MB;
import bean.session.UsuarioMB;
import itt.web.conexion.Conexion;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import mx.edu.ittoluca.logutils.Logger;
import oracle.jdbc.OracleTypes;

/**
 *
 * @author Edgar
 */
public class General_Grupos_DAO {

    /**
     *
     * @param paCarreraId : Se refiere a la carrera para la cual pertenecen los
     * planes
     * @param user : Contiene los datos del usuario en sesión
     * @param app : Nombre de la aplicación
     * @param mod : Nombre del módulo
     * @return retorno: Devuelve un objeto tipo GenericReturn con la lista de
     * planes, el codigo de error y la descripcion del mismo
     */
    public static GenericReturn<List<Plan_Grupos_MB>> getPlanes(int paCarreraId, UsuarioMB user, String app, String mod) {
        final Logger logger = new Logger();
        GenericReturn<List<Plan_Grupos_MB>> retorno = new GenericReturn<>();
        
        Connection con = Conexion.getConnection(user.getNomUsuario(), user.getPassUsuario(), app, mod);
        if (con != null) {
            try (CallableStatement cs = con.prepareCall("{call DEPTOS.PQ_GET_CATALOG_APP_GRUPOS_A.GET_PLANES_X_CARRERA_SP(?,?,?,?)}")){
                
                cs.setInt("paCarreraId", paCarreraId);
                cs.registerOutParameter("paCurRetorno", OracleTypes.CURSOR);
                cs.registerOutParameter("paCodError", OracleTypes.NUMBER);
                cs.registerOutParameter("paDescError", OracleTypes.VARCHAR);
                cs.execute();

                retorno.setPaCodError(cs.getInt("paCodError"));
                retorno.setPaDesError(cs.getString("paDescError"));
                
                if (retorno.getPaCodError() == 0) {
                    List<Plan_Grupos_MB> lPlanes = new ArrayList<>();
                    ResultSet rs = (ResultSet) cs.getObject("paCurRetorno");
                    while (rs.next()) {
                        lPlanes.add(new Plan_Grupos_MB(
                                rs.getInt("PLAN_ID"),
                                rs.getString("CLAVE"),
                                rs.getString("COMPETENCIAS").charAt(0)));
                    }
                    retorno.setRetorno(lPlanes);
                }
                
            } catch (SQLException ex) {
                logger.registrarErrorSQL(ex, app, mod, user.getNomUsuario());
                retorno.setPaCodError(ex.getErrorCode());
                retorno.setPaDesError(logger.getMensajeError());
            } finally {
                Conexion.cerrarConexion(con);
            }
        } else {
            retorno.setPaDesError(Conexion.getConnectionErrorMessage());
        }
        return retorno;
    }

    /**
     *
     * @param paPlanId : Se refiere al plan del cual se requieren las
     * especialidades
     * @param user : Contiene los datos del usuario en sesión
     * @param app : Nombre de la aplicación
     * @param mod : Nombre del módulo
     * @return retorno: Devuelve un objeto tipo GenericReturn que contiene la
     * lista de las especialidades, el codigo de error y la descripcion del
     * mismo
     */
    public static GenericReturn<List<Especialidad_Grupos_MB>> getEspecialidad(int paPlanId, UsuarioMB user, String app, String mod) {
        final Logger logger = new Logger();
        GenericReturn<List<Especialidad_Grupos_MB>> retorno = new GenericReturn<>();

        Connection con = Conexion.getConnection(user.getNomUsuario(), user.getPassUsuario(), app, mod);
        if (con != null) {
            try (CallableStatement cs =con.prepareCall("{call DEPTOS.PQ_GET_CATALOG_APP_GRUPOS_A.GET_ESPEC_X_PLAN_SP(?,?,?,?)}")){
                
                cs.setInt("paPlanId", paPlanId);
                cs.registerOutParameter("paCurRetorno", OracleTypes.CURSOR);
                cs.registerOutParameter("paCodError", OracleTypes.NUMBER);
                cs.registerOutParameter("paDescError", OracleTypes.VARCHAR);
                cs.execute();

                retorno.setPaCodError(cs.getInt("paCodError"));
                retorno.setPaDesError(cs.getString("paDescError"));
                if (retorno.getPaCodError() == 0) {
                    List<Especialidad_Grupos_MB> lEspecialidad = new ArrayList<>();
                    ResultSet rs = (ResultSet) cs.getObject("paCurRetorno");
                    while (rs.next()) {
                        lEspecialidad.add(new Especialidad_Grupos_MB(
                                rs.getInt("ESP_ID"),
                                rs.getString("NOMBRE_ESPEC")));
                    }
                    retorno.setRetorno(lEspecialidad);
                } 
                
            } catch (SQLException ex) {
                logger.registrarErrorSQL(ex, app, mod, user.getNomUsuario());
                retorno.setPaCodError(ex.getErrorCode());
                retorno.setPaDesError(logger.getMensajeError());
            }
        } else {
            retorno.setPaDesError(Conexion.getConnectionErrorMessage());
        }
        return retorno;
    }

    /**
     * Descripción: Obtiene el periodo y el año (Agosto-Diciembre 20##||
     * Enero-Junio 20##) con base a la opción requerida
     *
     * @param paOpcion : Especifica que periodo es requerido (1): Actuales (2):
     * Preparación
     * @param user
     * @param app : Nombre de la aplicación
     * @param mod : Nombre del módulo
     * @return retorno : Devuelve objeto de tipo GenericReturn con el periodo y
     * el año
     */
    public static GenericReturn getPeriodoAño(int paOpcion, UsuarioMB user, String app, String mod) {
        final Logger logger = new Logger();
        GenericReturn retorno = new GenericReturn();
        Connection con = Conexion.getConnection(user.getNomUsuario(), user.getPassUsuario(), app, mod);
        
        if (con != null) {
            try(CallableStatement cs = con.prepareCall("{call DEPTOS.PQ_GET_CATALOG_APP_GRUPOS_A.GET_PER_A\u00d1O_REINS_COOR_SP(?,?,?,?,?)}")) {
                
                cs.setInt("paOpcion", paOpcion);
                cs.registerOutParameter("paPeriodo", OracleTypes.NUMBER);
                cs.registerOutParameter("paA\u00f1o", OracleTypes.NUMBER);
                cs.registerOutParameter("paCodigoError", OracleTypes.NUMBER);
                cs.registerOutParameter("paMjeDescError", OracleTypes.VARCHAR);
                cs.execute();
                retorno.setPaCodError(cs.getInt("paCodigoError"));
                retorno.setPaDesError(cs.getString("paMjeDescError"));
                if (retorno.getPaCodError() == 0) {
                    retorno.getExtraParams().put("paPeriodo", cs.getInt("paPeriodo"));
                    retorno.getExtraParams().put("paA\u00f1o", cs.getInt("paA\u00f1o"));
                }
                
            } catch (SQLException ex) {
                logger.registrarErrorSQL(ex, app, mod, user.getNomUsuario());
                retorno.setPaCodError(ex.getErrorCode());
                retorno.setPaDesError(logger.getMensajeError());
            } finally {
                Conexion.cerrarConexion(con);
            }
        } else {
            retorno.setPaDesError(Conexion.getConnectionErrorMessage());
        }
        return retorno;
    }

    public static GenericReturn getLeyendaPeriodo(int paOpcion, UsuarioMB user, String app, String mod) {
        final Logger logger = new Logger();
        GenericReturn<String> retorno = new GenericReturn<>();
        Connection con = Conexion.getConnection(user.getNomUsuario(), user.getPassUsuario(), app, mod);
        if (con != null) {

            try (CallableStatement cs = con.prepareCall("{call DEPTOS.PQ_GET_CATALOG_APP_GRUPOS_A.GET_LEY_PER_A\u00d1O_COOR_SP(?,?,?,?,?,?)}")) {
                cs.setInt("paOpcion", paOpcion);

                cs.registerOutParameter("paPeriodo", OracleTypes.NUMBER);
                cs.registerOutParameter("paAño", OracleTypes.NUMBER);
                cs.registerOutParameter("paLeyenda", OracleTypes.VARCHAR);
                cs.registerOutParameter("paCodError", OracleTypes.NUMBER);
                cs.registerOutParameter("paDesError", OracleTypes.VARCHAR);

                cs.execute();

                retorno.setPaCodError(cs.getInt("paCodError"));
                retorno.setPaDesError(cs.getString("paDesError"));

                if (retorno.getPaCodError() == 0) {
                    user.setPeriodo(cs.getInt("paPeriodo"));
                    user.setAño(cs.getInt("paAño"));
                    retorno.getExtraParams().put("paPeriodo", user.getPeriodo());
                    retorno.getExtraParams().put("paAño", user.getAño());
                    retorno.getExtraParams().put("paLeyenda", cs.getString("paLeyenda"));                                        
                }

            } catch (SQLException ex) {
                logger.registrarErrorSQL(ex, app, mod, user.getNomUsuario());
                retorno.setPaCodError(ex.getErrorCode());
                retorno.setPaDesError(logger.getMensajeError());
            } finally {
                Conexion.cerrarConexion(con);
            }
        } else {
            retorno.setPaDesError(Conexion.getConnectionErrorMessage());
        }

        return retorno;

    }
}
