/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package interfaces.strings;

/**
 *
 * @author Edgar
 */
public interface AppInfo {

    //Nombres de Aplicacion 
    String APP_NAME_1 = "GRUPOS";
    String APP_NAME_2 = "ASIGNAR_PROFESOR";

    //Nombres de Modulos de APP1
    String APP_MOD_C = "INSERTAR_GRUPOS";
    String APP_MOD_R = "CATALOGO_GRUPOS";
    String APP_MOD_U = "ACTUALIZAR_GRUPOS";
    String APP_MOD_D = "BORRAR_GRUPOS";
    String APP_MOD_CHK = "CHECK_CAPACIDAD";
    String APP_MOD_SES = "SESIÓN_GRUPOS";
    String APP_MOD_PA = "PRESTAMO_AULAS";

    //Nombres de Modulos APP1 para Reportes 
    String APP_MOD_REP_H = "REPORTE_MAPEO_AULAS";
    String APP_MOD_REP_I = "REPORTE_INSCRITOS";

    // Nombres de Modulos de APP2
    String APP_2_MOD_C = "INSERTAR_PROFESOR";
    String APP_2_MOD_R = "CATALOGOS_PROFESOR";
    String APP_2_MOD_R_1 = "CATALOGOS_GRUPOS_PROFESOR";

    String APP_2_MOD_HORARIOS_C = "GUARDAR HORARIOS";
    String APP_2_MOD_HORARIOS_R = "OBTENER HORARIOS";
    String APP_2_MOD_HORARIOS_U = "ACTUALIZAR_HORARIOS";

    //Nombres de Modulos APP2 para Reportes 
    String APP_2_MOD_REP_H = "REPORTE_MAPEO_PROFESORES";

}
